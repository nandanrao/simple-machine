$(document).ready(function () {
    $("button.submit, a.submit, .submit-spinner").on("click", function(){
        $(this).hide();
        $(this).after('<span class="form-submit-spinner"><div class="dot1"></div><div class="dot2"></div></span>');
    })
});