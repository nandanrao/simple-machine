$(window).ready(function(){

    if (sessionStorage) {
        if (sessionStorage.getItem('show_filter_mark') == 1) {
            $("span.filter-mark").show();
            $("button.reset").show();
        }

        min.value = sessionStorage.getItem('min_value');
        max.value = sessionStorage.getItem('max_value');
    }
    checkValue("input#min", 1);
    checkValue("input#max", 180);

    $.each($("nav a"), function(index,el){
        if ($(el).attr("href") == window.location.pathname) {
            $(el).addClass("active");
        }
    });

    $("#pg--sidebar").mmenu({
        zposition: 'front'
    });

    $("button#reset-filters-button").on('click', function(e){
        e.preventDefault();
        $("input#min").val(1);
        $("input#max").val(180);
        $("input#minYear").val(maxYear.min);
        $("input#maxYear").val(maxYear.max);
        $("span.countries a.country").removeClass('active');
        $("span.countries a.country[data-id='all']").addClass('active');
        $("a.active-country").html($("span.countries a.country[data-id='all']").html());
        setCurrentFilterState();
        $(window).trigger('filterValues.changed');
    })

    $("a.sidebar-menu").on('click', function(){
        $("#pg--sidebar").css('display', '').trigger( "open.mm" );
        loadCurrentFilterState();
    });

     $("a.active-country").on("click", function(){
        $("span.countries-placeholder").show();
        $("a.active-country").hide();
     })

    // Click on country
    $("span.countries").on("click", "a.country", function(ev){
        ev.preventDefault();
        $("span.countries a.country").removeClass('active');
        $(ev.currentTarget).addClass('active');
        $("a.active-country").html($(ev.currentTarget).html());
        $("span.countries-placeholder").hide();
        $("a.active-country").show();
        setCurrentFilterState();
        $(window).trigger('filterValues.changed');
    });

});

function checkValue(element_selector, value){
    var element_value = $(element_selector).val();
    if (element_value < 1) {
        $(element_selector).val(value);
        setCurrentFilterState();
    }
    $(window).trigger('filterValues.changed');
}

function changeMin(){
    checkValue("input#min", 1);
};

function changeMax(){
    checkValue("input#max", 180);
};

function loadCurrentFilterState(){
    $.getJSON(load_filter_data_url, function(data){
        $("#minYear").attr('min', data.min_year);
        $("#minYear").attr('max', data.max_year);
        var min_year = (sessionStorage && sessionStorage.getItem('min_year')) ? sessionStorage.getItem('min_year') : data.min_year;
        var max_year = (sessionStorage && sessionStorage.getItem('max_year')) ? sessionStorage.getItem('max_year') : data.max_year;
        $("#minYear").attr('value', min_year);
        $("#maxYear").attr('min', data.min_year);
        $("#maxYear").attr('max', data.max_year);
        $("#maxYear").attr('value', max_year);

        $("span.countries-placeholder").html('<p><a class="country active" data-id="all" href="#">ALL COUNTRIES</a></p>' + data.countries);

        if (sessionStorage && sessionStorage.getItem('country_id') && sessionStorage.getItem('country_id') != 'all') {
            $("span.countries a.country").removeClass('active');
            $("span.countries a.country[data-id='" + sessionStorage.getItem('country_id') + "']").addClass('active');
            $("a.active-country").html($("span.countries a.country[data-id='" + sessionStorage.getItem('country_id') + "']").html());
        }
        setCurrentFilterState();
    });
}

function changeMinYear(){
    checkValue("input#minYear", maxYear.min);
};

function changeMaxYear(){
    checkValue("input#maxYear", maxYear.max);
};

function setNewValueToSession(){
    setCurrentFilterState();
}

function setCurrentFilterState(){
    var min_value = min.value;
    var max_value = max.value;
    var min_year = minYear.value;
    var max_year = maxYear.value;
    var show_filter_mark = 0;
    var country_id = $("span.countries a.country.active").attr("data-id");
    if (country_id == undefined){
        country_id = 'all';
    }
    if (min_value != min.min || max_value != min.max || min_year != minYear.min || max_year != minYear.max || country_id != 'all') {
        show_filter_mark = 1;
        $("span.filter-mark").show();
        $("button.reset").show();
    }
    else {
        $("span.filter-mark").hide();
        $("button.reset").hide();
    }

    if (sessionStorage) {
        sessionStorage.setItem('min_value', min_value);
        sessionStorage.setItem('max_value', max_value);
        sessionStorage.setItem('min_year', min_year);
        sessionStorage.setItem('max_year', max_year);
        sessionStorage.setItem('country_id', country_id);
        sessionStorage.setItem('show_filter_mark', show_filter_mark);
    };
}