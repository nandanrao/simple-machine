var attachFileUploader = function() {
    $('#fileupload').fileupload({
        dataType: 'json',
        done: function(e, data){
            if (data.result.success == true) {
                $("div#progress").fadeOut(200);
                $("a#upload-button span").html("+UPLOAD OTHER IMAGE");
                $("div.fileinput-button").css("border", "none");
                $("span.fileinput-button").fadeIn(200);
                $("div#uploader-info").css("border", "");

                // Uploaded image and croppper outline on it
                $("div#uploaded-image").css("height", "");
                $("div#uploaded-image").html("<img id='profile_image' src='"+data.result.url+"' />");
                $("div#uploaded-image").append("<input type='hidden' name='coords' />");
                $("input#profile_image_field").val(data.result.filename);
                $('#profile_image').Jcrop({
                    aspectRatio: 3.5,
                    setSelect: [0, 0, 3500, 3500],
                    trueSize: [data.result.width, data.result.height],
                    bgOpacity: .6,
                    bgColor: 'gray',
                    onSelect: function(c) {
                        $("form input#coords").val([c.x, c.y, c.x2, c.y2].join(','));
                    }
                });
            } else {
                $("div#progress").hide();
                $("a#upload-button span").html("UPLOAD PROFILE IMAGE");
                $("span.fileinput-button").fadeIn(50);
                alert(data.result.reason);
            }
        },
        progress: function(e, data){
            // Upload progress
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .bar').css(
                'width',
                progress + '%'
            );
        },
        start: function(e){
            $("div#progress").fadeIn(200);
            $("div#progress div.bar").css('width', '0px');
            $("span.fileinput-button").fadeOut(200);
        }
    });

    $("#fileupload").fileupload('option', {
        maxFileSize: 30000000,
        acceptFileTypes: /(\.|\/)(gif|jpeg|jpg|png|bmp)$/i
    });

    $("#fileupload").bind("fileuploadprocessfail", function(e, data){
        if (data.files[0].type == "image/tiff") {
            alert("Sorry! TIF files cannot be shown in most major browsers, so for now we are not accepting them! Please upload a JPEG, PNG, or GIF in RGB color space.");
        }
        else {
            alert("Invalid file. Please upload gif, jpg, png or bmp file that is smaller than 30MB.");
        }
    });
};

var fileUploadWithCrop = function(coordinatesField) {
    if (coordinatesField.val() != "") {
        var coords = coordinatesField.val();
        $('#profile_image').Jcrop({
            aspectRatio: 3.5,
            setSelect: coords.split(','),
            bgOpacity: .6,
            bgColor: 'gray',
            onSelect: function(c) {
                $("form input#coords").val([c.x, c.y, c.x2, c.y2].join(','));
            }
        });
        $("button#upload-button span").html("+UPLOAD DIFFERENT IMAGE");
        $("div.fileinput-button").css("border", "none");
        attachFileUploader();
    }
    else {
        // No image uploaded
        attachFileUploader();
    }
};
