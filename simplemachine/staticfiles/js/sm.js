var SM = {
    /*
     * Attaches Select2 single choice dropdown to jQuery element el
     * Pulls select options from url
     * If autocomplete flag is true, options are pulled while typing,
     * else options are preloaded
     */
    singleChoiceDropdown: function(el, url, autocomplete, hideSelected) {
        var _showSelectedText = function(elem, selectedItemTitle) {
            elem.after("<span class='selected-choice'>" + selectedItemTitle + " <a href='#' class='remove-selection'> -REMOVE/CHANGE</a></span>");
            elem.siblings("div").hide();

            $("a.remove-selection").on("click", function(ev){
                ev.preventDefault();
                elem.select2("val", "");
                elem.siblings("div").show();
                elem.siblings("span").remove();
            });
        };

        el.select2({
            minimumInputLength: 2,
            multiple: true,
            maximumSelectionSize: 1,
            formatSelectionTooBig: function(){
                return '';
            },
            ajax: {
                url: url,
                dataType: 'json',
                quietMillis: 100,
                data: function(term, page){
                    return { q: term }
                },
                results: function(data, page){
                    return { results: data }
                }
            },
            formatInputTooShort: function(){
                return 'Please enter at least 2 characters...';
            },
            initSelection: function(element, callback){
                var id = element.attr("data-id");
                var title = element.attr("data-text");
                if (id && title) {
                    _showSelectedText($(element), title);
                }
            }
        });

        if (hideSelected != true){
            el.on("change", function(e) {
                if (e.val && e.val.length > 0) {
                    _showSelectedText($(e.currentTarget), e.added.text);
                }
            });
        }
    },

    singleChoicePick: function(el, createText, createUrl) {
        if (createText != "") {
            el.append("<option value='-1'>" + createText + "</option>");
        }

        el.select2({
            minimumResultsForSearch: '1000' // Don't show search bar
        });

        el.on("change", function(e){
            if (e.val && e.val=="-1") {
                window.location = createUrl;
            }
        });
    },

    showMessage: function(type, text) {
        if (type == "success" || type == "error") {
            type = "alert-" + type;
            $("div.message-container").prepend(
                "<div class='alert alert-block " + type + "'><a class='close' data-dismiss='alert' href='#' style='margin-top: 2px'>×</a>"+text+"</div>"
            )
        }
    },

    /*
     * Returns data from users sessionStorage
     */
    getFiltersValues: function(){
        return {
            min: sessionStorage.getItem('min_value'),
            max: sessionStorage.getItem('max_value'),
            minYear: sessionStorage.getItem('min_year'),
            maxYear: sessionStorage.getItem('max_year'),
            country_id: sessionStorage.getItem('country_id')
        }
    },

    /*
     * Adds loading spinner to element
     */
    addSpinner: function(elementSelector){
        $(elementSelector).html('<div class="spinner"><div class="dot1"></div><div class="dot2"></div></div>');
     },

    /*
     * Adds ID as hidden field to form, and shows it's
     * title in taggedFilmsContainer along with REMOVE button
     */
    tagFilm: function(id, title, form, taggedFilmsContainer) {
        form.append("<input type='hidden' name='films' value="+id+" data-title="+title+" />")
        taggedFilmsContainer.append("<p>"+title+" <a href='#' style='font-weight: 700' data-id="+id+">-REMOVE</a></p>");
    },

    /*
     * Attaches Select2 autocomplete picker to jQuery element el
     * Films are autocompleted from url passed as parameter
     * When film is selected, tagFilm() is called
     * When REMOVE is clicked, film is removed from both form and taggedFilmsContainer
     */
    filmTaggerAutocomplete: function(el, url, form, taggedFilmsContainer) {
        el.select2({
            minimumInputLength: 2,
            multiple: true,
            maximumSelectionSize: 1,
            formatSelectionTooBig: function(){
                return '';
            },
            ajax: {
                url: url,
                dataType: 'json',
                quietMillis: 100,
                data: function(term, page){
                    return { q: term }
                },
                results: function(data, page){
                    return { results: data }
                }
            },
            formatInputTooShort: function(){
                return 'Please enter at least 2 characters...';
            },
            initSelection: function(element, callback){
                var id = element.attr("data-id");
                var title = element.attr("data-text");
                if (id && title) {
                    _showSelectedText($(element), title);
                }
            }
        });

        el.on("change", function(e) {
            if (e.val && e.val.length > 0) {
                SM.tagFilm(e.added.id, e.added.text, form, taggedFilmsContainer);
                el.select2("val", "");
            }
        });
    },

    /*
     * Shows already tagged films.
     * Called after form submission if form has errros and
     * when editing
     */
    showTaggedFilms: function(form, taggedFilmsContainer) {
        var fields = form.find("input.tagged-film");
        _.each(fields, function(element, index, list) {
            SM.tagFilm($(element).attr('data-id'), $(element).attr('data-title'), form, taggedFilmsContainer);
        });
    }
}
