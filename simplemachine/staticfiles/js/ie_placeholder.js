function addPlaceHolder(input) {
    if (!Modernizr.input.placeholder) {
        input.focus(function () {
            if ($(this).val() == $(this).attr('placeholder')) {
                $(this).val('');
                $(this).removeClass('placeholder');
                $(this).css("color", "#666");
                $(this).css("font-weight", "300");
            }
        }).blur(function () {
            if ($(this).val() == '' || $(this).val() == $(this).attr('placeholder')) {
                $(this).addClass('placeholder');
                $(this).val($(this).attr('placeholder'));
                $(this).css("color", "#AAA");
                $(this).css("font-weight", "300");
            }
        }).blur();
        $(input).parents('form').submit(function () {
            $(this).find(input).each(function () {
                if ($(this).val() == $(this).attr('placeholder')) {
                    $(this).val('');
                }
            })
        });
     }
 }

addPlaceHolder($(':text'));
addPlaceHolder($('textarea'));
addPlaceHolder($(':password'));
addPlaceHolder($('input[type="url"]'));
addPlaceHolder($('input[type="tel"]'));
addPlaceHolder($('input[type="email"]'));
