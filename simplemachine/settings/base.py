# Django settings for simplemachine project.

import django.conf.global_settings as DEFAULT_SETTINGS
import os.path
import djcelery
djcelery.setup_loader()

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
PROJECT_NAME = os.path.basename(ROOT_DIR)

def ABS_PATH(*args):
    return os.path.join(ROOT_DIR, *args)

ADMINS = (
    ('Dev Team', 'dev@smplmchn.com'),
)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
# MEDIA_ROOT = ABS_PATH('media')
MEDIA_ROOT = '/opt/sm/media/'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ABS_PATH('static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ABS_PATH('staticfiles'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
)

COMPRESS_PRECOMPILERS = (
    # ('text/coffeescript', 'coffee --compile --stdio'),
    # ('text/less', 'lessc {infile} {outfile}'),
    # ('text/x-sass', 'sass {infile} {outfile}'),
    ('text/x-scss', 'sass --scss {infile} {outfile}'),
    # ('text/stylus', 'stylus < {infile} > {outfile}'),
    # ('text/foobar', 'path.to.MyPrecompilerFilter'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '4cnus_qz)56vf=%f-)3blzd^@6i^t4%i+$k6928it@crcpzm#z'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request',
    'common.context_processors.mixpanel_token',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'pagination.middleware.PaginationMiddleware'
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'simplemachine.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'simplemachine.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    ABS_PATH('templates')
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'filebrowser',
    'grappelli',
    'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
    'pagination',
    'django_select2',
    'djcelery',
    'kombu.transport.django',
    'south',
    'compressor',
    'taggit',

    'common',
    'film',
    'screening',
    'notifications',
    'online_screening'
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(asctime)s %(levelname)s %(pathname)s %(lineno)s %(message)s'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'dj_logfile': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'logs/app.log',
            'formatter': 'verbose',
        },
        'dj_message': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'logs/messages.log',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'simplemachine': {
            'handlers': ['dj_logfile'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'sm_messages': {
            'handlers': ['dj_message'],
            'level': 'INFO',
            'propagate': True,
        },
    }
}


LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'

PROFILE_IMAGE_MAX_WIDTH = 460

AUTH_PROFILE_MODULE = 'common.UserProfile'

BASE_URL = "http://simplemachine.dobarkod.hr"

DEFAULT_FROM_EMAIL = "simple machine <donotreply@smplmchn.com>"
DEFAULT_FROM_EMAIL_ACCOUNTS = "accounts@smplmchn.com"
BADCOP_EMAIL = "badcop@smplmchn.com"

# Celery
BROKER_URL = 'django://'

# Dropbox
DROPBOX_APP_KEY = 'mhnwzurq8xh5agc'
DROPBOX_APP_SECRET = 'a8uatgpbcrforha'
DROPBOX_ACCESS_TYPE = 'app_folder'

FILE_UPLOAD_MAX_MEMORY_SIZE = 15728640
VIDEO_FILES_DIR = MEDIA_ROOT + '/films/exhibition_files'

SM_USER = 'machine@smplmchn.com'
WELCOME_MESSAGE_TEXT = 'Welcome to Simple Machine - feel free to ask me any questions or voice any concerns you might have. This is new, and we really want your feedback!'
INBOUND_EMAIL_DOMAIN = "inbound.smplmchn.com"

# Eventbrite
EVENTBRITE_API_KEY = 'WD5G2FNXYULQAGOWER'
EVENTBRITE_CLIENT_SECRET = 'R62J2THIEA4JHYV776LI35KJSMC2SGLBRHBVSOVFP7P5O6CTZ4'
EVENTBRITE_USER_AUTH_URL = 'https://www.eventbrite.com/oauth/authorize?response_type=code&client_id=%s&ref=%d'
EVENTBRITE_ACCESS_TOKEN_URL = 'https://www.eventbrite.com/oauth/token'

# Vimeo
VIMEO_CLIENT_ID = '3c57cb3ebc6580c6b824831952d8e7b4f590b7d8'
VIMEO_CLIENT_SECRET = '3b16930369cf2a67acbaaf9eba2c12ea0647fe66'
VIMEO_REQUEST_TOKEN_URL = 'https://vimeo.com/oauth/request_token'
VIMEO_USER_AUTH_URL = 'https://vimeo.com/oauth/authorize'
VIMEO_ACCESS_TOKEN_URL = 'https://vimeo.com/oauth/access_token'
VIMEO_PERMISSION = 'write'  # read, write or delete
