from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': '',
        'USER': 'postgres',
        'PASSWORD': '',
        'NAME': 'smplmchn',
        'OPTIONS': {
            'autocommit': True
        }
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = 'postmaster@inbound-test.smplmchn.com'
EMAIL_HOST_PASSWORD = '5n2xq9l7s026'
INBOUND_EMAIL_DOMAIN = "inbound-test.smplmchn.com"

ALLOWED_HOSTS = ['88.198.48.100',]

BASE_URL = "http://88.198.48.100:8080"

PAYPAL_SVCS_ENDPOINT = "https://svcs.sandbox.paypal.com"
PAYPAL_POSTBACK_ENDPOINT = "https://www.sandbox.paypal.com/webscr"

PAYPAL_API_USERNAME = "smpay_1355828977_biz_api1.gmail.com"
PAYPAL_API_PASSWORD = "1355829039"
PAYPAL_API_SIGNATURE = "A2sM8A02dN-3UVDF3wdW1jwjHJHVAsLnnc9OYUHXsq-2OCHRKaLJZZr7"
PAYPAL_APPLICATION_ID = "APP-80W284485P519543T"

SIMPLEMACHINE_PAYPAL_EMAIL = 'smpay_1355828977_biz@gmail.com'

PROFTPD_AUTH_FILE = '/var/lib/proftpd/auth.sqlite'
XFERLOG_FILE_PATH = '/var/log/proftpd/xferlog'
FTP_UPLOAD_DIR = '/srv/ftp/incoming'

# Mixpanel
MIXPANEL_TOKEN = '65dae2a4c5d4dc56c74144af7fa09a05'

#Campaign Monitor
CM_CLIENT_ID = 'f70360a4edb1215845c0843b0905224b'
CM_API_KEY = 'c3792736c5914e67c01c2cda6d10a46d'
