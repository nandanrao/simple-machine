from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'dev.db'
    }
}

MIDDLEWARE_CLASSES += (
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
)


INSTALLED_APPS += (
    'django_nose',
    # 'debug_toolbar'
)

# NOSE_ARGS = ['--rednose']
# NOSE_ARGS = ['--processes=4']
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

BASE_URL = "http://localhost:8000"

PAYPAL_SVCS_ENDPOINT = "https://svcs.sandbox.paypal.com"
PAYPAL_POSTBACK_ENDPOINT = "https://www.sandbox.paypal.com/webscr"

PAYPAL_API_USERNAME = "smpay_1355828977_biz_api1.gmail.com"
PAYPAL_API_PASSWORD = "1355829039"
PAYPAL_API_SIGNATURE = "A2sM8A02dN-3UVDF3wdW1jwjHJHVAsLnnc9OYUHXsq-2OCHRKaLJZZr7"
PAYPAL_APPLICATION_ID = "APP-80W284485P519543T"

SIMPLEMACHINE_PAYPAL_EMAIL = 'smpay_1355828977_biz@gmail.com'

SOUTH_TESTS_MIGRATE = False

PROFTPD_AUTH_FILE = 'sqlite.auth'
XFERLOG_FILE_PATH = '/Users/imustac/Develop/work/ftp/log/xferlog'
FTP_UPLOAD_DIR = '/srv/ftp/incoming'

# Mixpanel
# MIXPANEL_TOKEN = 'dummy_token'
MIXPANEL_TOKEN = '65dae2a4c5d4dc56c74144af7fa09a05'

# Eventbrite
EVENTBRITE_API_KEY = 'E4ZRLYPKQAAEQRXP5V'
EVENTBRITE_CLIENT_SECRET = '4BB3PX62ZP56OAM5TN7DMXZOZU6QYUGQ7PITAKEUGT2X2CBKUN'

#Vimeo
VIMEO_CLIENT_ID = 'f567b11f7f45933a313d71d802a0e3776910db60'
VIMEO_CLIENT_SECRET = '723e9c3583788834cb04eee50777133ffabe2336'

#Campaign Monitor
# CM_CLIENT_ID = '100150'
# CM_CLIENT_SECRET = 'URnFVB85SV55at35tg5kmQY2iBqhM53N255524LLK3NK5c5WGOD5Kb55o5TPOT572J7L1WaErkZrzAg5'
CM_CLIENT_ID = 'f70360a4edb1215845c0843b0905224b'
CM_API_KEY = 'c3792736c5914e67c01c2cda6d10a46d'
