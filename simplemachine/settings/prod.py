from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': '',
        'USER': 'postgres',
        'PASSWORD': '',
        'NAME': 'smplmchn',
        'OPTIONS': {
            'autocommit': True
        }
    }
}

ALLOWED_HOSTS = [
    'beta.smplmchn.com',
    '5.9.121.245',
]

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = 'postmaster@inbound.smplmchn.com'
EMAIL_HOST_PASSWORD = '7043z-w-pvw3'

SM_PAYPAL = 'smpay_1355828977_biz@gmail.com'
BASE_URL = "http://beta.smplmchn.com"

PAYPAL_SVCS_ENDPOINT = "https://svcs.paypal.com"
PAYPAL_POSTBACK_ENDPOINT = "https://www.paypal.com/webscr"

PAYPAL_API_USERNAME = "sm_api1.smplmchn.com"
PAYPAL_API_PASSWORD = "9DVM8AVQ6KGLCKFG"
PAYPAL_API_SIGNATURE = "A2bdQcl-twiD9nRjpE6c.TTA8uCYA0dExz8ZnZnnXur6FNMmZjLoM84q"
PAYPAL_APPLICATION_ID = "APP-3R158282DY352842M"

SIMPLEMACHINE_PAYPAL_EMAIL = "sm@smplmchn.com"

# Dropbox
DROPBOX_APP_KEY = 'mhnwzurq8xh5agc'
DROPBOX_APP_SECRET = 'a8uatgpbcrforha'
DROPBOX_ACCESS_TYPE = 'app_folder'

PROFTPD_AUTH_FILE = '/var/lib/proftpd/auth.sqlite'
XFERLOG_FILE_PATH = '/var/log/proftpd/xferlog'
FTP_UPLOAD_DIR = '/srv/ftp/incoming'

# Mixpanel
MIXPANEL_TOKEN = '6141bc16199fb389162cddbc0fd39274'

#Campaign Monitor
CM_CLIENT_ID = 'f70360a4edb1215845c0843b0905224b'
CM_API_KEY = 'c3792736c5914e67c01c2cda6d10a46d'
