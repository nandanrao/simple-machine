from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'HOST': '',
        'USER': 'postgres',
        'PASSWORD': '',
        'NAME': 'smplmchn',
        'OPTIONS': {
            'autocommit': True
        }
    }
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INTERNAL_IPS = ('127.0.0.1',)

BASE_URL = "http://88.198.48.100:9000"

PAYPAL_SVCS_ENDPOINT = "https://svcs.sandbox.paypal.com"
PAYPAL_POSTBACK_ENDPOINT = "https://www.sandbox.paypal.com/webscr"

PAYPAL_API_USERNAME = "smpay_1355828977_biz_api1.gmail.com"
PAYPAL_API_PASSWORD = "1355829039"
PAYPAL_API_SIGNATURE = "A2sM8A02dN-3UVDF3wdW1jwjHJHVAsLnnc9OYUHXsq-2OCHRKaLJZZr7"
PAYPAL_APPLICATION_ID = "APP-80W284485P519543T"

SIMPLEMACHINE_PAYPAL_EMAIL = 'smpay_1355828977_biz@gmail.com'

SOUTH_TESTS_MIGRATE = False

