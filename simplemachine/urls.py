from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import film.urls
import film.critic_urls
import film.publication_urls
import film.curation_urls
import film.festival_urls
import screening.urls
import screening.venue_urls
import notifications.urls
import online_screening.urls
import common.account_urls
import common.browse_urls

from filebrowser.sites import site


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'simplemachine.views.home', name='home'),
    # url(r'^simplemachine/', include('simplemachine.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin/', include(admin.site.urls)),

    # Styling admin panel
    url(r'^grappelli/', include('grappelli.urls')),

    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page':'/'}, name='auth_logout'),

    url(r'^register/$', 'common.views.register', name='register'),
    url(r'^activate/(?P<key>.*)/$', 'common.views.activate', name='activate'),
    url(r'^login/', 'common.views.login', name='login'),
    url(r'^signup_login/', 'common.views.signup_login', name='signup_login'),
    url(r'^dropbox/link/(?P<current_film>.*)/$', 'common.views.link_dropbox_account_url', name='link-with-dropbox-url'),
    url(r'^dropbox/store_token/', 'common.views.store_dropbox_token', name='store-dropbox-token'),
    url(r'^file/tmp_upload/', 'common.views.upload_tmp_image', name='upload-tmp-file'),

    url(r'^films/', include(film.urls)),
    url(r'^screenings/', include(screening.urls)),
    url(r'^venues/', include(screening.venue_urls)),
    url(r'^festivals/', include(film.festival_urls)),
    url(r'^notifications/', include(notifications.urls)),
    url(r'^critics/', include(film.critic_urls)),
    url(r'^publications/', include(film.publication_urls)),
    url(r'^curations/', include(film.curation_urls)),
    url(r'^browse/', include(common.browse_urls)),
    url(r'^online_screenings/', include(online_screening.urls)),

    url(r'^what_it_is/', 'common.views.what_it_is', name='what_it_is'),
    url(r'^contact/', 'common.views.contact', name='contact'),
    url(r'^developer/', 'common.views.developer', name='developer'),
    url(r'^grants/', 'common.views.grants', name='grants'),
    url(r'^search/', 'common.views.search', name='search'),

    # url(r'^load_countries/', 'common.views.load_countries', name='load-countries'),
    # url(r'^load_min_max_year/', 'common.views.load_min_max_year', name='load-min-max-year'),
    url(r'^load_filter_data/', 'common.views.load_filter_data', name='load-filter-data'),
    url(r'^load_discover_data/', 'common.views.load_discover_data', name='load-discover-data'),


    url(r'^events/', 'screening.views.see_events', name='see-events'),
    url(r'^(?P<pk>\d+)/send_message/', 'common.views.send_message', name='send-message'),
    url(r'^users/(?P<pk>\d+)/', 'common.views.user_page', name='user-page'),

    url(r'^account/', include(common.account_urls)),
    url(r'^$', 'common.views.home', name='home'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
