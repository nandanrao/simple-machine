from django.shortcuts import redirect, render, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import Http404

from common.vimeo_api import VimeoAPI
from .models import OnlineScreening, OnlineFilm
from .forms import OnlineScreeningForm, OnlineFilmForm

@login_required
def create_online_screening(request, pk=None):
    if pk:
        online_screening = get_object_or_404(OnlineScreening, pk=pk)
        form = OnlineScreeningForm(request.POST or None, instance=online_screening)
    else:
        online_screening = None
        form = OnlineScreeningForm(request.POST or None)

    if form.is_valid():
        form.instance.user = request.user
        form.instance.invite_hash = OnlineScreening._generate_invite_hash(request.user)
        new_online_screening = form.save()
        if online_screening:
            messages.success(request, 'Online screening updated!')
        else:
            messages.success(request, 'Online screening created!')
        return redirect('online-screening-details', new_online_screening.pk)

    edit = True if online_screening else False

    return render(request, 'online_screening_form.html', {
        'form': form,
        'edit': edit
    })


def view_online_screening(request, pk):
    online_screening = get_object_or_404(OnlineScreening, pk=pk)
    if online_screening.cancelled:
        return redirect('home')
    online_screening_url = request.build_absolute_uri(reverse('create-online-film', args=(online_screening.invite_hash,)))
    return render(request, 'online_screening_details.html', {
        'online_screening': online_screening,
        'online_screening_url': online_screening_url
    })


@login_required
def create_online_film(request, invite_hash):
    if request.user.get_profile().is_linked_with_vimeo():
        vimeo_videos = VimeoAPI.get_user_videos(request.user.get_profile().vimeo_access_token)
        online_screening = get_object_or_404(OnlineScreening, invite_hash=invite_hash)
        if online_screening.is_started:
            return redirect('online-screening-details', pk=online_screening.pk)
        is_pro = vimeo_videos[0].get('owner').get('is_pro') if vimeo_videos else None

        form = OnlineFilmForm(request.POST or None)

        if form.is_valid():
            form.instance.user = request.user
            form.instance.online_screening = online_screening
            new_online_film = form.save()
            messages.success(request, 'Film added to online screening!')

        available_videos = []
        unavailable_videos = []
        already_added = []
        for video in vimeo_videos:
            video['thumbnail'] = video.get('thumbnails').get('thumbnail')[2].get('_content')
            if online_screening.check_if_film_already_exists(video.get('id')):
                already_added.append(video)
            elif video.get('embed_privacy') == 'anywhere' or is_pro == '1':
                available_videos.append(video)
            else:
                unavailable_videos.append(video)

        return render(request, 'online_film_form.html', {
            'online_screening': online_screening,
            'available_videos': available_videos,
            'unavailable_videos': unavailable_videos,
            'already_added_videos': already_added,
            'form': form,
            'is_pro': is_pro
        })
    else:
        return redirect("%s?next=%s" % (reverse('account-vimeo'), request.path))


@login_required
def cancel_online_screening(request, pk):
    online_screening = get_object_or_404(OnlineScreening, pk=pk)
    if not online_screening.user == request.user:
        return redirect('home')

    if online_screening.user == request.user:
        online_screening.cancel()
        online_screening.save()

    return render(request, 'online_screening_details.html', {
        'online_screening': online_screening
    })


@login_required
def delete_online_film(request, online_screening_id, vimeo_video_id):
    online_screening = get_object_or_404(OnlineScreening, pk=online_screening_id)
    try:
        online_film = OnlineFilm.objects.filter(vimeo_id=vimeo_video_id, online_screening__id=online_screening_id)[:1].get()
    except ObjectDoesNotExist:
        raise Http404

    if online_film and not online_film.user == request.user:
        return redirect('home')

    if online_film and online_film.user == request.user:
        online_film.delete()
    return redirect(reverse('create-online-film', args=(online_screening.invite_hash,)))
