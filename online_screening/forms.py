import datetime
import pytz

from dateutil import parser as iso8601_parser
from django import forms
from pytz import timezone
from django_select2.widgets import Select2Widget
from django_select2.fields import Select2ChoiceField

from .models import OnlineScreening, OnlineFilm


class OnlineScreeningForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'TITLE *'}), required=True)
    start_time = forms.CharField(widget=forms.HiddenInput(), required=True)
    end_time = forms.CharField(widget=forms.HiddenInput(), required=True)
    timezone = Select2ChoiceField(
        required=True,
        choices=[('', '')] + OnlineScreening.TIMEZONES,
        widget=Select2Widget(select2_options={'placeholder': 'TIMEZONE *'})
    )

    def clean(self):
        self.cleaned_data['start_time'] = self._clean_time(self.cleaned_data['start_time'])
        self.cleaned_data['end_time'] = self._clean_time(self.cleaned_data['end_time'])
        return self.cleaned_data

    def _clean_time(self, field):
        if field:
            try:
                dt = iso8601_parser.parse(field)
                now = datetime.datetime.now(timezone(self.cleaned_data['timezone']))
                if dt < now:
                    raise forms.ValidationError('Invalid online screening date or time')
                field = dt
            except:
                raise forms.ValidationError('Invalid online screening date or time')
        return field

    class Meta:
        model = OnlineScreening
        exclude = ('user', 'invite_hash', 'cancelled', 'created_on', 'is_ready')



class OnlineFilmForm(forms.ModelForm):
    class Meta:
        model = OnlineFilm
        exclude = ('user', 'online_screening', 'is_ready')
        widgets = {
            'vimeo_id': forms.HiddenInput(),
            'vimeo_password': forms.TextInput(attrs={'placeholder': 'VIMEO VIDEO PASSWORD *'}),
            'vimeo_privacy': forms.HiddenInput(),
            'vimeo_embed_privacy': forms.HiddenInput()
        }
