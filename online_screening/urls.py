from django.conf.urls.defaults import patterns, url

from .views import *

urlpatterns = patterns(
    '',
    url(r'^add/$', create_online_screening, name='add-online-screening'),
    url(r'^edit/(?P<pk>\d+)/$', create_online_screening, name='edit-online-screening'),
    url(r'^cancel/(?P<pk>\d+)/$', cancel_online_screening, name='cancel-online-screening'),
    url(r'^(?P<pk>\d+)/$', view_online_screening, name='online-screening-details'),
    url(r'^(?P<invite_hash>.+)/invitation/$', create_online_film, name='create-online-film'),
    url(r'^(?P<online_screening_id>\d+)/remove_film/(?P<vimeo_video_id>\d+)/$', delete_online_film, name='delete-online-film'),
)
