import hashlib
import datetime
import pytz

from pytz import timezone
from django.db import models
from django.contrib.auth.models import User


class OnlineScreening(models.Model):
    TIMEZONES = map(lambda tz: (tz, tz.replace('_', ' ')), pytz.common_timezones)

    name = models.CharField(max_length=200)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    timezone = models.CharField(max_length=100, choices=TIMEZONES)
    user = models.ForeignKey(User, related_name='online_screenings')
    invite_hash = models.URLField(max_length=250, unique=True)
    cancelled = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    is_ready = models.BooleanField(default=False)

    def __unicode__(self):
        return self.invite_hash

    @staticmethod
    def _generate_invite_hash(user):
        m = hashlib.md5()
        m.update("%s%s" % (user.email, datetime.datetime.now()))
        return m.hexdigest()

    @staticmethod
    def get_by_hash(invite_hash):
        return OnlineScreening.objects.filter(
            OnlineScreening.cancelled==False
        ).filter(OnlineScreening.invite_hash==invite_hash).first()

    @property
    def is_started(self):
        now = datetime.datetime.now(pytz.utc)
        return True if now > self.start_time else False

    def check_if_film_already_exists(self, vimeo_id):
        if vimeo_id in [film.vimeo_id for film in self.online_films.all()]:
            return True
        else:
            return False

    def cancel(self):
        self.cancelled = True


class OnlineFilm(models.Model):
    user = models.ForeignKey(User, related_name='online_films')
    online_screening = models.ForeignKey(OnlineScreening, related_name='online_films')
    vimeo_id = models.CharField(max_length=20, blank=True)
    vimeo_password = models.CharField(max_length=50, blank=True)
    vimeo_privacy = models.CharField(max_length=10, blank=True)
    vimeo_embed_privacy = models.CharField(max_length=10, blank=True)
    is_ready = models.BooleanField(default=False)
