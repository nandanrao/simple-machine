import json
import datetime
import requests
from django.core.management.base import BaseCommand
from common.vimeo_api import VimeoAPI
from online_screening.models import OnlineScreening, OnlineFilm


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Revert video from online screenings that past 10 minutes ago to old state
        print 'Revert privacy on online screenings videos: %s' % str(datetime.datetime.now())
        end_time = datetime.datetime.now() - datetime.timedelta(minutes=10)
        online_screenings_ended = OnlineScreening.objects.exclude(cancelled=True).filter(
            end_time__lt=end_time,
            is_ready=True
        )
        for screening in online_screenings_ended:
            for film in screening.online_films.all():
                vimeo_access_token = film.user.get_profile().vimeo_access_token
                film_info = VimeoAPI.get_video_info(vimeo_access_token, film.vimeo_id)

                if film_info[0].get('privacy') != film.vimeo_privacy:
                    print "Revert vimeo_id: %s, privacy: %s" % (film.vimeo_id, film.vimeo_privacy)
                    VimeoAPI.set_privacy(vimeo_access_token, film.vimeo_id, film.vimeo_privacy, film.vimeo_password)

                if (film_info[0].get('embed_privacy') != film.vimeo_embed_privacy and
                    film_info[0].get('owner').get('is_pro') != '0'):
                    print "Revert vimeo_id: %s, privacy: %s" % (film.vimeo_id, film.vimeo_privacy)
                    VimeoAPI.set_embed_privacy(vimeo_access_token, film.vimeo_id, film.vimeo_embed_privacy)

                film.is_ready = False
                film.save()
            screening.is_ready = False
            screening.save()
