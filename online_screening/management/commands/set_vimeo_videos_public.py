import json
import datetime
import requests
from django.core.management.base import BaseCommand
from common.vimeo_api import VimeoAPI
from online_screening.models import OnlineScreening, OnlineFilm


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Set privacy for videos on online screening that starts within 2 minutes
        print 'Set privacy on online screenings videos: %s' % str(datetime.datetime.now())
        start_time = datetime.datetime.now()
        start_time_end = datetime.datetime.now() + datetime.timedelta(minutes=2)
        online_screenings = OnlineScreening.objects.exclude(cancelled=True).filter(
            start_time__range=(start_time, start_time_end),
            is_ready=False
        )
        for screening in online_screenings:
            for film in screening.online_films.all():
                vimeo_access_token = film.user.get_profile().vimeo_access_token
                film_info = VimeoAPI.get_video_info(vimeo_access_token, film.vimeo_id)
                film.vimeo_privacy = film_info[0].get('privacy')
                film.vimeo_embed_privacy = film_info[0].get('embed_privacy')

                if film_info[0].get('privacy') != 'anybody':
                    print "Set vimeo_id: %s, old privacy: %s" % (film.vimeo_id, film.vimeo_privacy)
                    r = VimeoAPI.set_privacy(vimeo_access_token, film.vimeo_id, 'anybody')

                if film_info[0].get('embed_privacy') != 'anywhere':
                    if film_info[0].get('owner').get('is_pro') != '0':
                        print "Set vimeo_id: %s, old privacy: %s" % (film.vimeo_id, film.vimeo_privacy)
                        r = VimeoAPI.set_embed_privacy(vimeo_access_token, film.vimeo_id, 'anywhere')
                        if r.get('stat') == 'ok':
                            film.is_ready = True
                        else:
                            film.is_ready = False
                    else:
                        film.is_ready = False
                else:
                    film.is_ready = True
                film.save()
            screening.is_ready = True
            screening.save()
