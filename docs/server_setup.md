Deamons that must be running
----------------------------

* simplemachine-web - Upstart script that starts gunicorn workers
* simplemachine-celery - Upstart script that starts Celery workers
* nginx - (in /etc/init.d) for serving static files
* postgres - (in /etc/init.d) database
* ftp-upload-monitor - Upstart script for monitoring FTP uploads
