# Dev environment requirements

    - nose
    - django_nose
    - rednose
    - webtest
    - django-webtest 
    - factory-boy

Run tests with:
    ./manage.py test


# Make sure following is set up
1. crontab task for sending screening reminders (wathc out for path in send_reminders.sh)
2. decoders (install those first, and then install pillow)
    - png and jpeg


#############
# Steps to set up production data locally (production is postgres, local is sqlite)
#############

1. Export all data on production DB except common.userprofile (because that will be
    created by signal when creating User model)

    python manage.py dumpdata --all -e common.userprofile > db.json

2. Initialize dev.db sqlite database (don't create admin)

    python manage.py syncdb --all

3. Empty django_content_type and auth_permission tables in newly initialized DB

    python manage.py dbshell
    delete from django_content_type
    delete from auth_permission

4. Load DB dump from production

    python manage.py loaddata db.json

5. Empty common_userprofile table

    python manage.py dbshell
    delete from common_userprofile

6. Export common_userprofile table on production

    python manage.py dumpdata --all common.userprofile > up.json

7. Load DB dump of for common.userprofile locally

    python manage.py loaddata up.json
