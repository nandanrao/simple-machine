from django.core.urlresolvers import reverse
from django_webtest import WebTest

from .factories import ReviewerFactory, UserFactory, FilmFactory


class ReviewsTestCase(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user = UserFactory.create(username='reviewtest')
        FilmFactory.create(user=self.user)

    def test_adding_reviewer_returns_its_id(self):
        resp = self.app.post(reverse('add-reviewer'), {'name':'some name'}, user=self.user)
        self.assertEquals(resp.body, '1')

    def test_adding_organization_returns_its_id(self):
        resp = self.app.post(reverse('add-organization'), {'name':'some org'}, user=self.user)
        self.assertEquals(resp.body, '1')

    def test_user_can_add_review_with_url_and_reviewer(self):
        reviewer = ReviewerFactory.create()
        resp = self.app.post(reverse('add-review', args=[1]), {
            'url': 'www.google.com',
            'reviewer': '1',
            'full_text': 'some full text with pull quote',
            'pull_quote': 'pull quote'
        }, user=self.user)
        self.assertTrue('Jonathan Holland' in resp.body)
