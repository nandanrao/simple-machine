import datetime

from django.test import TestCase
from .factories import UserFactory, FilmFactory, VenueFactory
from notifications.models import NewsItem, NewsItemType
from notifications.conversation_creation import ConversationCreation

from notifications.conversations import Conversations, Conversation


class ConversationsIntegrationTest(TestCase):
    def test_are_sorted_from_newest_to_oldest(self):
        u1 = UserFactory.create(username='filmowner')
        u2 = UserFactory.create(username='venueowner')
        u3 = UserFactory.create(username='randomuser')
        u4 = UserFactory.create(username='randomuser4')
        f1 = FilmFactory.create(user=u1, title='somefilm')
        v1 = VenueFactory.create(user=u2, name='somevenue')

        c1 = ConversationCreation.start_new(f1, u3)
        c2 = ConversationCreation.start_new(v1, u3)
        c3 = ConversationCreation.start_new(u3, u4)
        c1.save()
        c2.save()
        c3.save()

        n1 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.PREVIEW_REQUEST_SENT,
                                     conversation=c2,
                                     content_object=f1)
        n2 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.SCREENING_INVITATION,
                                     conversation=c1,
                                     content_object=v1)
        n3 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.SCREENING_ACCEPTED,
                                     conversation=c3,
                                     content_object=f1)

        conversations = list(Conversations.for_user(u3))
        self.assertEqual(conversations[0].last_updated, n3.created_on)
        self.assertEqual(conversations[1].last_updated, n2.created_on)
        self.assertEqual(conversations[2].last_updated, n1.created_on)


    def test_unread_conversations_are_on_top(self):
        u1 = UserFactory.create(username='filmowner')
        u2 = UserFactory.create(username='venueowner')
        u3 = UserFactory.create(username='randomuser')
        u4 = UserFactory.create(username='randomuser4')
        f1 = FilmFactory.create(user=u1, title='somefilm')
        v1 = VenueFactory.create(user=u2, name='somevenue')

        c1 = ConversationCreation.start_new(f1, u3)
        c2 = ConversationCreation.start_new(v1, u3)
        c3 = ConversationCreation.start_new(u3, u4)
        c1.save()
        c2.save()
        c3.save()

        n1 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.PREVIEW_REQUEST_SENT,
                                     conversation=c2,
                                     content_object=f1)
        n2 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.SCREENING_DECLINED,
                                     conversation=c1,
                                     read=True,
                                     content_object=v1)
        n3 = NewsItem.objects.create(for_user=u3,
                                     type=NewsItemType.SCREENING_ACCEPTED,
                                     conversation=c3,
                                     read=True,
                                     content_object=f1)

        conversations = list(Conversations.for_user(u3))
        self.assertEqual(conversations[0].last_updated, n1.created_on)
        self.assertEqual(conversations[1].last_updated, n3.created_on)
        self.assertEqual(conversations[2].last_updated, n2.created_on)
