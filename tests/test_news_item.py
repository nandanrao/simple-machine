from django.test import TestCase
from .factories import UserFactory, FilmFactory

from notifications.models import NewsItem, NewsItemType


# INTEGRATION (DB)
class NewsItemTest(TestCase):
    def setUp(self):
        self.user1 = UserFactory.create(username='user1')
        self.user2 = UserFactory.create(username='user2')
        self.film = FilmFactory.create(user=self.user1)
        self.ni1 = NewsItem.objects.create(
            for_user=self.user1,
            type=NewsItemType.PREVIEW_REQUEST_SENT,
            sent=True,
            content_object=self.film
        )
        self.ni2 = NewsItem.objects.create(
            for_user=self.user1,
            type=NewsItemType.SCREENING_INVITATION,
            sent=False,
            content_object=self.film
        )
        self.ni3 = NewsItem.objects.create(
            for_user=self.user2,
            type=NewsItemType.SCREENING_ACCEPTED,
            sent=True,
            content_object=self.film
        )

    def test_latest_for_user_returns_users_latest_news_items(self):
        news_items = NewsItem.latest_for_user(self.user1)
        self.assertListEqual(list(news_items), [self.ni2, self.ni1])
