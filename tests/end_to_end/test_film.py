from django_webtest import WebTest
from tests.factories import FilmFactory, UserFactory
from django.core.urlresolvers import reverse


class FilmDetailsTest(WebTest):
    def test_film_details(self):
        film = FilmFactory.create()
        resp = self.app.get(reverse('film-details', args=[1]), user=film.user).follow()
        self.assertTrue('terminator 2' in resp)
        self.assertTrue('SOME DIRECTOR' in resp)
        self.assertTrue('www.website.com' in resp)


class EditFilmTest(WebTest):
    csrf_checks = False

    def setUp(self):
        self.film = FilmFactory.create()
        self.other_user = UserFactory.create(username='other')

    def test_only_film_owner_can_edit(self):
        resp = self.app.post(reverse('edit-film', args=[1]), user=self.other_user).follow()
        self.assertTrue('CONVERSATIONS' in resp)
        self.assertFalse('REQUIRED INFORMATION' in resp)

    def test_when_user_edits_film_film_page_shows_new_info(self):
        new_data = {
            'title': 'terminator 4',
            'director': 'goran',
            'countries': 1,
            'synopsis': 'asd',
            'image': 'some_image.jpg',
            'coordinates': '1,1,200,200'
        }
        resp2 = self.app.post(reverse('edit-film', args=[1]), new_data, user=self.film.user)
        resp = self.app.get(reverse('film-details', args=[1]), user=self.other_user).follow()
        self.assertTrue('terminator 4' in resp)
        self.assertTrue('GORAN' in resp)
