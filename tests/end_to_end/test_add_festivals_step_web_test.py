from django_webtest import WebTest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from film.models import Film


class IfUserCreatesNewFestivalOnFestivalStep(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user_adding_festival = User.objects.create(username='festival_adder')
        self.other_user = User.objects.create(username='other_user')
        self.app.post(reverse('add-festival'), {'name': 'Some festival'}, user=self.user_adding_festival)

    def test_that_festival_is_visible_to_him(self):
        visible_festivals = self.app.get(reverse('list-festivals'), {'q': 'Some'}, user=self.user_adding_festival)
        self.assertListEqual([{'id': 1, 'text': 'Some festival'}], visible_festivals.json)

    def test_that_festival_is_not_visible_to_other_users(self):
        visible_festivals = self.app.get(reverse('list-festivals'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([], visible_festivals.json)


class IfUserSavesFestivalStep(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user_adding_festival = User.objects.create(username='festival_adder')
        self.other_user = User.objects.create(username='other_user')
        FILM_DATA = {
            'title': 'Film Title',
            'director': 'Some Director',
            'synopsis': 'Some Description',
            'profile_image': 'Some image',
            'user': self.user_adding_festival
        }

        self.film = Film.objects.create(**FILM_DATA)
        self.app.post(reverse('add-festival'), {'name': 'Some festival'}, user=self.user_adding_festival)
        self.app.post(reverse('add-film-on-festival'), {
            'id-festival[]': 1,
            'section': 'some section',
            'awards': 'some awards',
            'year': 2012,
            'film_pk': 1
        }, user=self.user_adding_festival)
        self.app.post(reverse('add-film-festivals', args=[self.film.id]), {'step_name': 'festivals'})

    def test_added_festival_is_visible_to_everybody(self):
        visible_festivals = self.app.get(reverse('list-festivals'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([{'id': 1, 'text': 'Some festival'}], visible_festivals.json)

    def test_added_film_on_festival_is_saved(self):
        self.assertEqual(1, len(self.film.played_on.all()))
