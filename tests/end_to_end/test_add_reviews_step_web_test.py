from django_webtest import WebTest
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from film.models import Film, Reviewer


class IfUserCreatesNewReviewerWhenAddingReview(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user_adding_review = User.objects.create(username='review_adder')
        self.other_user = User.objects.create(username='other_user')
        self.app.post(reverse('add-reviewer'), {'name': 'Some reviewer'}, user=self.user_adding_review)

    def test_that_reviewer_is_visible_to_him(self):
        visible_reviewers = self.app.get(reverse('list-reviewers'), {'q': 'Some'}, user=self.user_adding_review)
        self.assertListEqual([{'id': 1, 'text': 'Some reviewer'}], visible_reviewers.json)

    def test_that_reviewer_is_not_visible_to_other_users(self):
        visible_reviewers = self.app.get(reverse('list-reviewers'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([], visible_reviewers.json)


class IfUserCreatesNewOrganizationWhenAddingReview(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user_adding_org = User.objects.create(username='org_adder')
        self.other_user = User.objects.create(username='other_user')
        self.app.post(reverse('add-organization'), {'name': 'Some org'}, user=self.user_adding_org)

    def test_that_org_is_visible_to_him(self):
        visible_orgs = self.app.get(reverse('list-organizations'), {'q': 'Some'}, user=self.user_adding_org)
        self.assertListEqual([{'id': 1, 'text': 'Some org'}], visible_orgs.json)

    def test_that_org_is_not_visible_to_other_users(self):
        visible_orgs = self.app.get(reverse('list-organizations'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([], visible_orgs.json)


class IfUserSavesReviewStep(WebTest):
    csrf_checks = False

    def setUp(self):
        self.user_adding_review = User.objects.create(username='review_adder')
        self.other_user = User.objects.create(username='other_user')
        FILM_DATA = {
            'title': 'Film Title',
            'director': 'Some Director',
            'synopsis': 'Some Description',
            'profile_image': 'Some image',
            'user': self.user_adding_review
        }

        self.film = Film.objects.create(**FILM_DATA)
        self.app.post(reverse('add-reviewer'), {'name': 'Some reviewer'}, user=self.user_adding_review)
        self.app.post(reverse('add-organization'), {'name': 'Some organization'}, user=self.user_adding_review)
        self.app.post(reverse('add-review', args=[1]), {
            'url': 'http://www.google.com',
            'organization': 1,
            'reviewer': 1,
            'full_text': 'asd',
            'pull_quote': 'asd'
        }, user=self.user_adding_review)
        self.app.post(reverse('add-film-reviews', args=[self.film.id]), {
            'step_name': 'reviews',
        })

    def test_added_reviewer_is_visible_to_everybody(self):
        visible_reviewers = self.app.get(reverse('list-reviewers'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([{'id': 1, 'text': 'Some reviewer'}], visible_reviewers.json)

    def test_added_org_is_visible_to_everybody(self):
        visible_orgs = self.app.get(reverse('list-organizations'), {'q': 'Some'}, user=self.other_user)
        self.assertListEqual([{'id': 1, 'text': 'Some organization'}], visible_orgs.json)

    def test_added_review_is_saved(self):
        self.assertEqual('http://www.google.com/', self.film.reviews.all()[0].url)
