from django.test import TestCase

from film.models import Film, VideoLinkConverter
from .factories import UserFactory, FilmFactory


class FilmTestCase(TestCase):
    def setUp(self):
        self.user1 = UserFactory.create(username='user1')

    def test_create_film(self):
        film = Film(title='Cool film',
                    director='Some director',
                    runtime=120,
                    user=self.user1)
        film.save()
        self.assertEquals(film.id, 1)

    def test_get_films_by_user_returns_only_users_films(self):
        u1 = UserFactory.create(username='u1')
        f1 = FilmFactory.create(title='some f1', user=u1)
        f2 = FilmFactory.create(title='some f2', user=u1)

        films = Film.by_user(u1)
        self.assertListEqual([f1, f2], list(films))


class VideoLinkConverterTest(TestCase):
    def test_youtube_link_has_param_v_in_querystring_returns_id(self):
        link = "youtube.com/watch?v=X61BVv6pLtw"
        self.assertEquals(VideoLinkConverter.parse_youtube_link_id(link), 'X61BVv6pLtw')
        link = "http://www.youtube.com/watch?v=X61BVv6pLtw"
        self.assertEquals(VideoLinkConverter.parse_youtube_link_id(link), 'X61BVv6pLtw')
        link = "http://www.youtube.com/watch?v=X61BVv6pLtw&playnext=1&list=P&feature=results_main"
        self.assertEquals(VideoLinkConverter.parse_youtube_link_id(link), 'X61BVv6pLtw')

    def test_youtube_link_has_no_param_v_in_querystring_returns_None(self):
        link = "http://www.youtube.com"
        self.assertEquals(VideoLinkConverter.parse_youtube_link_id(link), None)
        link = "http://www.youtube.com/watch?playnext=1&list=PLF050D73C&feature=results_main"
        self.assertEquals(VideoLinkConverter.parse_youtube_link_id(link), None)

    def test_correct_vimeo_link(self):
        link = "vimeo.com/123"
        self.assertEquals(VideoLinkConverter.parse_vimeo_link_id(link), '123')
        link = "vimeo.com/123?#extra"
        self.assertEquals(VideoLinkConverter.parse_vimeo_link_id(link), '123')

    def test_incorrect_vimeo_link(self):
        link = "vimeo.com/aaa"
        self.assertEquals(VideoLinkConverter.parse_vimeo_link_id(link), None)
