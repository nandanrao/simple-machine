import unittest

from nose.plugins.attrib import attr

from film.forms import FestivalsStep, BasicInfoStep, MediaStep, ReviewForm
from film.models import Reviewer, Organization


@attr('unit')
class AddFilmWizardTestCase(unittest.TestCase):
    def test_festivals_form_correctly_parses_empty_id_list(self):
        form = FestivalsStep({'festival_ids':''})
        form.is_valid()
        self.assertEquals(form.cleaned_data['festivals'], [])

    def test_festivals_form_correctly_extract_festival_ids(self):
        form = FestivalsStep({'festival_ids':'1,2,3'})
        form.is_valid()
        self.assertEquals(form.cleaned_data['festivals'], [1,2,3])


@attr('unit')
class FilmFormTest(unittest.TestCase):
    REQUIRED = {
        'title':'some film',
        'director':'some director',
        'countries':['1'],
        'synopsis':'some desc',
        'image':'some_image.jpg',
        'coordinates': '1,1,100,100'
    }

    def test_form_is_valid_with_required_fields(self):
        form = BasicInfoStep(self.REQUIRED)
        self.assertTrue(form.is_valid())

    def test_raises_error_if_trailer_url_is_not_youtube_or_vimeo(self):
        form = MediaStep({'trailer_url':'www.fakeurl.com'})
        self.assertEqual(form.errors['trailer_url'][0], 'Please provide Vimeo or Youtube URL')

    def test_accepts_https_link(self):
        form = MediaStep({'trailer_url':'https://vimeo.com'})
        self.assertTrue('trailer_url' not in form.errors)


@attr('unit')
class ReviewFormTest(unittest.TestCase):

    def setUp(self):
        self.reviewer = Reviewer.objects.create(name="reviewer")
        self.organization = Organization.objects.create(name="organization")
        self.form_data = {
            'url': 'http://www.a.com',
            'organization': None,
            'reviewer': None,
            'full_text': 'text with pull quote',
            'pull_quote': 'pull quote'
        }

    def test_reviewer_not_selected_and_organization_not_selected_not_valid(self):
        form = ReviewForm(self.form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)
        self.assertEqual(form.errors['__all__'], ['Please select reviewer and/or organization'])

    def test_only_reviewer_selected_valid(self):
        self.form_data['reviewer'] = self.reviewer.id
        form = ReviewForm(self.form_data)
        self.assertTrue(form.is_valid())

    def test_only_organization_selected_valid(self):
        self.form_data['organization'] = self.organization.id
        form = ReviewForm(self.form_data)
        self.assertTrue(form.is_valid())

    def test_pull_quote_not_in_full_text_not_valid(self):
        self.form_data['organization'] = self.organization.id
        self.form_data['pull_quote'] = 'pull quote not in full text'
        form = ReviewForm(self.form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)
        self.assertEqual(form.errors['pull_quote'], ['pull quote not contained in full text'])

    def test_pull_quote_in_full_text_valid(self):
        self.form_data['organization'] = self.organization.id
        form = ReviewForm(self.form_data)
        self.assertTrue(form.is_valid())

    def test_full_text_is_required(self):
        self.form_data['organization'] = self.organization.id
        data = self.form_data.copy()
        data['full_text'] = ''
        form = ReviewForm(data)
        self.assertEqual(form.errors['full_text'], ['This field is required.'])
