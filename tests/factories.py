import factory

from django.contrib.auth.models import User

from film.models import Film, Reviewer, Festival, Review, Organization
from screening.models import Venue, Screening, ProfitShare

class UserFactory(factory.Factory):
    FACTORY_FOR = User

    username = 'testuser'
    first_name = 'test'
    last_name = 'user'


class FilmFactory(factory.Factory):
    FACTORY_FOR = Film

    title = 'terminator 2'
    year = 2012
    director = 'some director'
    profile_image = 'some_image.jpg'
    profile_image_small = 'some_image.jpg'
    user = factory.SubFactory(UserFactory, username='film owner')
    website = 'www.website.com'


class VenueFactory(factory.Factory):
    FACTORY_FOR = Venue

    name = 'some venue'
    user = factory.SubFactory(UserFactory, username='venue owner')
    description = 'some desc'
    timezone = 'Europe/Zagreb'
    country_id = 1


class ReviewerFactory(factory.Factory):
    FACTORY_FOR = Reviewer

    name = 'Jonathan Holland'
    active = True


class OrganizationFactory(factory.Factory):
    FACTORY_FOR = Organization

    name = 'Rotten Tomatoes'
    active = True


class ReviewFactory(factory.Factory):
    FACTORY_FOR = Review

    url = 'www.someurl.com'
    reviewer = factory.SubFactory(ReviewerFactory)
    film = factory.SubFactory(FilmFactory)
    full_text = 'some full text with pull quote'
    pull_quote = 'pull quote'
    active = True


class ProfitShareFactory(factory.Factory):
    FACTORY_FOR = ProfitShare

    type = 'NOPROFIT'


class ScreeningFactory(factory.Factory):
    FACTORY_FOR = Screening

    description = 'some desc'
    venue = factory.SubFactory(VenueFactory)
    film = factory.SubFactory(FilmFactory)
    profit = factory.SubFactory(ProfitShareFactory)


class FestivalFactory(factory.Factory):
    FACTORY_FOR = Festival

    name = 'some fest'
    about = 'this is festival description'
    website = 'www.google.com'
    email = 'festival@example.com'
    logo = 'somelogo.jpg'
    phone = '1923-0523'
    user = None
    active = True
