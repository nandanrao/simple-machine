def upload_file(app, url, user, csrf):
    f = open('tests/sample.jpg', 'r')
    return app.post(url, user=user, headers={'X-CSRFToken':csrf}, upload_files=[('file', 'sample.jpg', f.read())])


class ConversationHelper(object):
    @staticmethod
    def user_has_no_conversations(response):
        return True if len(response.pyquery('#conversations p')) == 0 else False

    @staticmethod
    def user_has_conversation(response, title):
        if not response.pyquery('#my-conversations a').html() == title:
            return False
        return True

    @staticmethod
    def has_message_in_conversation(response, title, subtitle):
        if not title in response.pyquery('div.message p.message-generated').html():
            return False
        if not subtitle in response.pyquery('div.message').html():
            return False
        return True

    @staticmethod
    def has_action_link(response, link_title, link_address):
        for link in response.pyquery('div.message p.action-links').items('a'):
            if link_title in link.html() and link.attr['href'] == link_address:
                return True
        return False

    @staticmethod
    def has_no_action_links(response):
        return response.pyquery('div.message td.action-links a').html() == None
