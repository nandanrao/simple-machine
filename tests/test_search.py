from django.test import TestCase
from django.core.urlresolvers import reverse

from common.search import Search
from common.models import Country
from film.models import Festival, Review, FilmOnFestival
from .factories import *


class SearchTestCase(TestCase):

    def setUp(self):
        self.user1 = UserFactory.create(username='user1')

    def test_returns_empty_list_if_there_are_no_results(self):
        s = Search('Hobit')
        results = s.search()
        self.assertEqual(results, [])

    def test_returns_list_with_one_film_result(self):
        self.f1 = FilmFactory.create(title='Django', user=self.user1, profile_image_small='dummy_filename')
        self.f1 = FilmFactory.create(title='Hobit', user=self.user1, profile_image_small='dummy_filename2')

        s = Search('django')
        results = s.search()
        self.assertEqual(len(results), 1)

    def test_returns_proper_film_format(self):
        f1 = FilmFactory.create(director='some director', title='Django', user=self.user1, profile_image_small='dummy_filename')
        test_results = [{
            'name': f1.title,
            'type': 'Film',
            'link': reverse('film-details', args=(f1.pk,)),
            'image': '/media/dummy_filename',
            'director': f1.director
        }]

        s = Search('django')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_proper_venue(self):
        v1 = VenueFactory.create(name='SC Kino', user=self.user1)
        test_results = [{
            'name': v1.name,
            'type': 'Venue',
            'link': reverse('venue-details', args=(v1.pk,)),
            # 'image': '/media/dummy_filename'
        }]

        s = Search('sc')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_proper_festival(self):
        f1 = Festival.objects.create(name='MovieFest', user=self.user1)
        film = FilmFactory.create(title='Django', user=self.user1,
            synopsis='Great django movie!', profile_image_small='dummy_filename')
        fof = FilmOnFestival.objects.create(film=film, festival=f1, active=True)

        test_results = [{
            'name': f1.name,
            'type': 'Festival',
            'link': reverse('festival-details', args=(f1.pk,)),
            'films': [{'title': 'Django', 'url': reverse('film-details', args=(film.pk,))}]
        }, {
            'name': film.title,
            'type': 'Film',
            'link': reverse('film-details', args=(film.pk,)),
            'image': '/media/dummy_filename',
            'director': film.director
        }]
        s = Search('movie')
        results = s.search()
        self.assertListEqual(results, test_results)

    def test_returns_proper_film_synopsis(self):
        f1 = FilmFactory.create(title='Django', user=self.user1,
            synopsis='Great western movie!', profile_image_small='dummy_filename')
        test_results = [{
            'name': f1.title,
            'type': 'Film',
            'link': reverse('film-details', args=(f1.pk,)),
            'image': '/media/dummy_filename',
            'director': 'some director'
        }]

        s = Search('western')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_proper_venue_by_tag_city(self):
        v1 = VenueFactory.create(user=self.user1, city='New York')
        test_results = [{
            'name': v1.name,
            'type': 'Venue',
            'link': reverse('venue-details', args=(v1.pk,)),
            # 'image': '/media/dummy_filename'
        }]

        s = Search('New York')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_proper_venue_by_tag_state(self):
        v1 = VenueFactory.create(user=self.user1, state='New York')
        test_results = [{
            'name': v1.name,
            'type': 'Venue',
            'link': reverse('venue-details', args=(v1.pk,)),
            # 'image': '/media/dummy_filename'
        }]

        s = Search('New York')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_proper_venue_by_tag_country(self):
        country = Country.objects.create(code='123', name='USA')
        v1 = VenueFactory.create(user=self.user1, country_id=country.id)
        test_results = [{
            'name': v1.name,
            'type': 'Venue',
            'link': reverse('venue-details', args=(v1.pk,)),
            # 'image': '/media/dummy_filename'
        }]

        s = Search('USA')
        results = s.search()
        self.assertEqual(results, test_results)

    def test_returns_without_duplicate_film_results(self):
        # django is both mentioned in title and in synopsis
        FilmFactory.create(title='Django', user=self.user1,
            synopsis='Great django movie!', profile_image_small='dummy_filename')

        s = Search('django')
        results = s.search()
        self.assertEqual(len(results), 1)

    def test_returns_without_duplicate_venue_results(self):
        v1 = VenueFactory.create(user=self.user1, name='New York Cinema',
            state='New York', city='New York')

        s = Search('york')
        results = s.search()
        self.assertEqual(len(results), 1)

    def test_returns_without_duplicate_festival_results(self):
        f1 = Festival.objects.create(name='MovieFest', user=self.user1,
            about='Best movie fest ever!')
        film = FilmFactory.create(title='Django', user=self.user1,
            synopsis='Great django movie!', profile_image_small='dummy_filename')
        fof = FilmOnFestival.objects.create(film=film, festival=f1, active=True)

        s = Search('fest')
        results = s.search()
        self.assertEqual(len(results), 2)

    def test_returns_without_duplicate_results(self):
        # 1
        film = FilmFactory.create(title='Django', user=self.user1,
            synopsis='Great western django movie!', profile_image_small="dummy_filename")
        # 2
        venue = VenueFactory.create(name='Django Kino', user=self.user1)
        # 4
        festival = Festival.objects.create(name='DjangoFest', user=self.user1,
            about='Yearly summer django festival!')
        fof = FilmOnFestival.objects.create(film=film, festival=festival, active=True)

        s = Search('django')
        results = s.search()
        self.assertEqual(len(results), 3)

        # Count types
        types = ['Film', 'Venue', 'Festival']
        for t in types:
            self.assertEqual(len([d for d in results if d['type'] == t]), 1)

    def test_deleted_film_does_not_show_as_screened_on_festival(self):
        user = UserFactory.create(username='fo')
        film1 = FilmFactory.create(title='film1', user=user)
        film2 = FilmFactory.create(title='film2', user=user)
        fest = FestivalFactory.create()
        fof1 = FilmOnFestival(film=film1, festival=fest, year=2012, active=True)
        fof2 = FilmOnFestival(film=film2, festival=fest, year=2012, active=True)
        fof1.save()
        fof2.save()
        film1.deleted = True
        film1.save()

        s = Search('fest')
        results = s.search()
        self.assertTrue(len(results) == 2)
        self.assertEqual(results[0]['name'], 'film2')
        self.assertEqual(results[0]['type'], 'Film')
        self.assertEqual(results[1]['name'], 'some fest')
        self.assertEqual(results[1]['type'], 'Festival')
        self.assertEqual(len(results[1]['films']), 1)

    def test_search_films_by_director(self):
        film1 = FilmFactory.create(title='film1', director='some cool director')

        s = Search('cool director')
        results = s.search()
        self.assertEqual(results[0]['name'], 'film1')

    def test_search_films_by_user(self):
        user = UserFactory.create(username='coolusername')
        user.get_profile().name = 'User Name'
        user.get_profile().save()
        film1 = FilmFactory.create(title='film1', user=user)

        s = Search('User Na')
        results = s.search()
        self.assertEqual(results[0]['name'], 'film1')

    def test_search_films_by_reviewer(self):
        review = ReviewFactory.create()

        s = Search('Jonathan Holland')
        results = s.search()
        self.assertEqual(results[0]['name'], 'terminator 2')

    def test_search_films_by_organization(self):
        org = OrganizationFactory.create()
        review = ReviewFactory.create(reviewer=None, organization=org)

        s = Search('Rotten')
        results = s.search()
        self.assertEqual(results[0]['name'], 'terminator 2')

    def test_search_films_by_review_text(self):
        review = ReviewFactory.create(full_text='this is review of this cool film')
        s = Search('review of this cool')
        results = s.search()
        self.assertEqual(results[0]['name'], 'terminator 2')

    def test_search_reviewers_by_name(self):
        reviewer = ReviewerFactory.create(name='Cool Reviewer')
        review = ReviewFactory.create(reviewer=reviewer)

        s = Search('Cool Revi')
        results = s.search()
        self.assertEqual(results[1]['name'], 'Cool Reviewer')

    def test_search_users_by_name(self):
        user = UserFactory.create(username='cooluser')
        user.get_profile().name = 'Cool User Name'
        user.get_profile().save()

        s = Search('Cool User')
        results = s.search()
        self.assertEqual(results[0]['name'], 'Cool User Name')
        self.assertEqual(results[0]['type'], 'User')

    def test_search_reviewers_by_film_name(self):
        film = FilmFactory.create(title="yeeha!")
        review = ReviewFactory.create(film=film)

        s = Search('yeeha')
        results = s.search()
        self.assertEqual(results[1]['name'], 'Jonathan Holland')
        self.assertEqual(results[1]['type'], 'Reviewer')
