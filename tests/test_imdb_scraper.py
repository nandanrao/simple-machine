import unittest
from collections import OrderedDict

from nose.plugins.attrib import attr
from film.imdb_parser import ImdbParser, ImdbCrewAndCastParser


@attr('integration')
class ImdbScraperTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        raw_html = open('tests/imdb_sample_1.txt').read()
        parser = ImdbParser(raw_html)
        self.parsed = parser.parse()

    def test_parses_title(self):
        self.assertEqual('The Men of Dodge City', self.parsed['title'])

    def test_parses_plot(self):
        plot = "Twentysomething J. and two of his close friends are in the process of realizing their utopian dream: turning an abandoned cathedral into a grant-powered, environmentally sustainable art space. Newcomers to the post-industrial frontier where they found this chapel, they have been carried here by progressive beliefs that have since left them in a muddled quandary to question the validity of their presence. But there is work to do every day. Work to realize the dream they might still have."
        self.assertEqual(plot, self.parsed['plot'])

    def test_parses_language(self):
        self.assertEqual(['English'], self.parsed['language'])

    def test_parses_year(self):
        self.assertEqual(2012, self.parsed['year'])

    def test_parses_countries(self):
        self.assertEqual(['US'], self.parsed['countries'])

    def test_parses_directors(self):
        self.assertEqual(['Nandan Rao'], self.parsed['directors'])

    def test_parses_runtime(self):
        self.assertEqual(94, self.parsed['runtime'])


class ImdbCrewAndCastScraperTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        raw_html  = open('tests/imdb_cast_sample_1.txt').read()
        parser = ImdbCrewAndCastParser(raw_html)
        self.parsed = parser.parse()

    def test_parses_cast(self):
        expected_cast = OrderedDict()
        expected_cast.update({'Ben Rickles': 'Ben'})
        expected_cast.update({'Sophia Takal': 'Sophia'})
        expected_cast.update({'Zach Weintraub': 'Zach'})
        self.assertEqual(expected_cast, self.parsed['cast'])

    def test_parses_crew(self):
        expected_crew = OrderedDict()
        expected_crew.update({'directed_by': ['Nandan Rao']})
        expected_crew.update({'produced_by': ['Samuel Baumel', 'Nandan Rao']})
        expected_crew.update({'music_by': ['Ernesto Carcamo']})
        expected_crew.update({'cinematography_by': ['Nandan Rao']})
        expected_crew.update({'editing_by': ['Michael R. Sweeny']})
        expected_crew.update({'sound': ['Nick Chirumbolo']})
        self.assertEqual(expected_crew, self.parsed['crew'])


class ImdbScraperWorksWhen(unittest.TestCase):
    def setUp(self):
        raw_html = open('tests/imdb_sample_2.txt').read()
        parser = ImdbParser(raw_html)
        self.parsed = parser.parse()

    def test_there_are_multiple_countries(self):
        self.assertListEqual(['AU', 'US'], self.parsed['countries'])

    def test_there_are_multiple_languages(self):
        self.assertListEqual(['English', 'French'], self.parsed['language'])

    def test_there_are_multiple_directors(self):
        self.assertEqual(['Andy Wachowski', 'Lana Wachowski'], self.parsed['directors'])

    def test_runtime_has_R_in_front(self):
        self.assertEqual(129, self.parsed['runtime'])
