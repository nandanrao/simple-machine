from django.test import TestCase
from django.contrib.auth.models import User
from common.models import UserProfile


class UserProfileTest(TestCase):

    def setUp(self):
        User.objects.all().delete()
        UserProfile.objects.all().delete()
        self.user = User.objects.create_user('some_user', password='pwd',
            email='a@a.com')

    def test_encode_decode_signature(self):
        code = UserProfile.encode_signature_with_email(self.user.email)
        result = UserProfile.decode_signature(code)
        self.assertEqual(result, {
            'user_id': self.user.id,
            'email': self.user.email
        })
