import unittest

from nose.plugins.attrib import attr

from common.forms import RegistrationForm


@attr('unit')
class UserRegistrationTest(unittest.TestCase):
    def test_user_can_register(self):
        form = RegistrationForm({'name':'Some User', 'email':'test@example.com', 'password1':'asd', 'password2':'asd'})
        self.assertTrue(form.is_valid())

    def test_throws_error_if_passwords_dont_match(self):
        form = RegistrationForm({'name':'Some User', 'email':'test@example.com', 'password1':'as1d', 'password2':'asd'})
        self.assertFalse(form.is_valid())

    def test_throws_error_if_invalid_email(self):
        form = RegistrationForm({'name':'Some User', 'email':'test@exampl@e.com', 'password1':'asd', 'password2':'asd'})
        self.assertFalse(form.is_valid())

    def test_user_cant_register_with_name_similar_to_simplemachine(self):
        forbidden_names = ('simple machine', 'Simple Machine', 'SIMPLE MACHINE', 'SMPLMCHN', 'smplmchn',
                           'smpl mchn', 'simplemachine')

        for name in forbidden_names:
            form = RegistrationForm({'name': name, 'email':'somemail@example.com', 'password1': 'asd', 'password2': 'asd'})
            self.assertFalse(form.is_valid())
