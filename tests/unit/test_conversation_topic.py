from unittest import TestCase
from mock import Mock

from notifications.conversations import Message, Conversation
from notifications.models import NewsItem
from film.models import Film
from django.contrib.auth.models import User
from screening.models import Screening, Venue


class ConversationTopicForFilmOwnerIsTest(TestCase):
    def test_None_if_only_items_in_conversation_are_pms(self):
        pm1 = Message(NewsItem(type='MESSAGE'))
        pm2 = Message(NewsItem(type='MESSAGE'))
        conv = Conversation(Mock(), Mock(), [pm1, pm2])
        self.assertTrue(conv.topic is None)

    def test_film_title_if_last_item_is_preview_request(self):
        i1 = Mock(content_object=Mock(film=Film(title='testfilm')), type='PREVIEW_REQUEST_SENT')
        i2 = NewsItem(type='MESSAGE')
        conv = Conversation(Mock(), Mock(), [i1, i2])
        self.assertEquals(conv.topic, 'testfilm')

    def test_venue_name_if_last_item_is_related_to_screening(self):
        user = User(id=1, username='asd')
        user2 = User(id=2, username='asd2')
        i1 = Mock(content_object=Screening(
            venue=Venue(name='venuename', user=user),
            film=Film(title='asd', user=user2)
        ), type='SCREENING_ACCEPTED')
        i2 = NewsItem(type='MESSAGE')
        conv = Conversation(Mock(), user2, [i1, i2])
        self.assertEquals(conv.topic, 'venuename')


class ConversationTopicForVenueOwnerIsTest(TestCase):
    def test_film_title_if_last_item_is_preview_request(self):
        i1 = Mock(content_object=Mock(film=Film(title='testfilm')), type='PREVIEW_REQUEST_SENT')
        i2 = NewsItem(type='MESSAGE')
        conv = Conversation(Mock(), Mock(), [i1, i2])
        self.assertEquals(conv.topic, 'testfilm')

    def test_film_title_if_last_item_is_related_to_screening(self):
        user = User(id=1, username='asd')
        user2 = User(id=2, username='asd2')
        i1 = Mock(content_object=Screening(
            venue=Venue(name='venuename', user=user2),
            film=Film(title='filmtitle', user=user)
        ), type='SCREENING_ACCEPTED')
        conv = Conversation(Mock(), user2, [i1])
        self.assertEquals(conv.topic, 'filmtitle')
