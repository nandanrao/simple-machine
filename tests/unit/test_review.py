from django.test import TestCase

from tests.factories import ReviewFactory


class ReviewTestCase(TestCase):
    def test_pull_quote_is_extracted_from_full_review_if_not_provided(self):
        r = ReviewFactory.create(full_text='''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam elementum tempus imperdiet. Duis commodo, felis vestibulum imperdiet molestie, elit tortor pellentesque orci, et ultricies augue quam et nunc. Fusce eget condimentum metus. Sed eu lectus nulla, a pharetra massa. Cras a turpis arcu. Ut et ligula sit amet urna aliquam sodales nec et lectus. Donec justo felis, pulvinar eget molestie ut, faucibus sit amet leo. In pharetra aliquam mi at bibendum. Nulla sagittis nibh quis tortor accumsan vestibulum. Morbi dapibus porttitor ipsum vel viverra. Duis ac risus nec quam faucibus scelerisque nec ac metus. Etiam pulvinar, arcu tempus mattis fringilla, lectus justo dictum arcu, id suscipit felis risus in mi. Donec ut orci nec odio accumsan bibendum. Curabitur placerat condimentum dui, et aliquam dui consectetur vitae. Morbi bibendum gravida odio a fringilla. Proin mattis, est eget volutpat porta, mi odio venenatis nulla, non euismod enim dolor ac est.''', pull_quote=None)
        self.assertEquals(r.get_pull_quote, ' '.join(r.full_text.split(' ')[:90]) + ' ...')

    def test_show_user_provided_pull_qoute_if_he_provided_one(self):
        r = ReviewFactory.create(full_text='this is full text with pull quote', pull_quote='pull quote')
        self.assertEquals(r.get_pull_quote, 'pull quote')
