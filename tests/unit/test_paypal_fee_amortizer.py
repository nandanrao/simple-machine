import unittest

from common.paypal_fee_amortizer import PaypalFeeAmortizer


class PaypalFeeAmortizerTest(unittest.TestCase):
    def test_adds_FOs_fees_amount_to_film_owners_amount(self):
        fo_amount, sm_amount = PaypalFeeAmortizer.calculate(fo_amount=50, sm_amount=10)
        self.assertEqual(fo_amount, 51.80)

    def test_substracts_FOs_fees_from_SM_amount(self):
        fo_amount, sm_amount = PaypalFeeAmortizer.calculate(fo_amount=50, sm_amount=10)
        self.assertEqual(sm_amount, 8.2)

    def test_amortizer_works_with_float_fees(self):
        fo_amount, sm_amount = PaypalFeeAmortizer.calculate(fo_amount=50.25, sm_amount=9.82)
        self.assertEqual(fo_amount, 52.06)
        self.assertEqual(sm_amount, 8.01)

    def test_fo_amount_and_sm_amount_must_add_up_to_starting_amount(self):
        fo_amount, sm_amount = PaypalFeeAmortizer.calculate(fo_amount=50.25, sm_amount=9.82)
        starting_amount = 50.25 + 9.82
        self.assertEqual(fo_amount + sm_amount, starting_amount)

    def test_throws_exception_if_sm_cut_is_to_small_to_pay_fo_fee(self):
        self.assertRaises(ValueError, PaypalFeeAmortizer.calculate, 4, 0.4)
