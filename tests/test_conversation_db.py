import datetime

from django.test import TestCase
from .factories import UserFactory, FilmFactory, VenueFactory
from pytz import timezone

from notifications.models import ConversationDb, NewsItem, NewsItemType


class ConversationDatabaseTest(TestCase):
    def test_conversation_is_for_user_if_he_is_one_of_the_participants(self):
        part1 = UserFactory.create(username='user1')
        part2 = UserFactory.create(username='user2')
        part3 = UserFactory.create(username='user3')
        conv1 = ConversationDb.objects.create(participant_1=part1, participant_2=part2, user1=part1, user2=part2)
        conv2 = ConversationDb.objects.create(participant_1=part2, participant_2=part3, user1=part2, user2=part3)

        self.assertListEqual(list(ConversationDb.for_user(part1)), [conv1])
        self.assertListEqual(list(ConversationDb.for_user(part2)), [conv1, conv2])

    def test_conversation_is_for_user_if_his_film_is_one_participant(self):
        user1 = UserFactory.create(username='user1')
        user2 = UserFactory.create(username='user2')
        user3 = UserFactory.create(username='user3')
        film1 = FilmFactory.create(user=user1)
        film2 = FilmFactory.create(user=user2)
        conv1 = ConversationDb.objects.create(participant_1=film1, participant_2=user2, user1=user1, user2=user2)
        conv2 = ConversationDb.objects.create(participant_1=film2, participant_2=user3, user1=user3, user2=user2)

        self.assertListEqual(list(ConversationDb.for_user(user1)), [conv1])

    def test_conversation_is_for_user_if_his_venue_is_one_participant(self):
        user1 = UserFactory.create(username='user1')
        user2 = UserFactory.create(username='user2')
        user3 = UserFactory.create(username='user3')
        venue1 = VenueFactory.create(user=user1)
        film1 = FilmFactory.create(user=user2)
        conv1 = ConversationDb.objects.create(participant_1=venue1, participant_2=film1, user1=user1, user2=user2)
        conv2 = ConversationDb.objects.create(participant_1=user1, participant_2=user3, user1=user1, user2=user3)

        self.assertListEqual(list(ConversationDb.for_user(user1)), [conv1, conv2])

    def test_conversations_last_update_time_is_datetime_of_last_added_news_item(self):
        u1 = UserFactory.create(username='filmowner')
        u2 = UserFactory.create(username='venueowner')
        conv1 = ConversationDb.objects.create(participant_1=u1, participant_2=u2, user1=u1, user2=u2)
        n1 = NewsItem.objects.create(for_user=u1,
                                     type=NewsItemType.PREVIEW_REQUEST_SENT,
                                     conversation=conv1,
                                     content_object=u1)
        n2 = NewsItem.objects.create(for_user=u1,
                                     type=NewsItemType.SCREENING_INVITATION,
                                     conversation=conv1,
                                     content_object=u1)
        n3 = NewsItem.objects.create(for_user=u1,
                                     type=NewsItemType.SCREENING_ACCEPTED,
                                     conversation=conv1,
                                     content_object=u1)

        self.assertEqual(conv1.last_updated, n3.created_on)

    def test_correctly_determines_if_conversation_exists_for_participants(self):
        u1 = UserFactory.create(username='filmowner')
        u2 = UserFactory.create(username='venueowner')
        u3 = UserFactory.create(username='randomuser')

        conv = ConversationDb.objects.create(participant_1=u1, participant_2=u2, user1=u1, user2=u2)
        conv2 = ConversationDb.objects.create(participant_1=u3, participant_2=u2, user1=u3, user2=u2)

        self.assertEqual(ConversationDb.for_participants(u1, u2), conv)
        self.assertEqual(ConversationDb.for_participants(u2, u1), conv)
