from django.test import TestCase
from django.contrib.auth.models import User

from film.models import Reviewer


class ReviewerTest(TestCase):
    def setUp(self):
        self.reviewer_creator = User.objects.create(username='review_creator')
        self.other_user = User.objects.create(username='other_user')
        self.reviewer1 = Reviewer.create(name='Reviewer1', user=self.reviewer_creator)
        self.reviewer1.make_active()
        self.reviewer1.save()
        self.reviewer2 = Reviewer.create(name='Reviewer2', user=self.reviewer_creator)

    def test_all_users_can_see_all_active_reviewers(self):
        self.assertTrue(self.reviewer1 in list(Reviewer.for_user(self.reviewer_creator)))
        self.assertTrue(self.reviewer1 in list(Reviewer.for_user(self.other_user)))

    def test_user_can_see_his_inactive_reviewers(self):
        self.assertTrue(self.reviewer2 in list(Reviewer.for_user(self.reviewer_creator)))

    def test_user_cant_see_other_users_inactive_reviewers(self):
        self.assertTrue(self.reviewer2 not in list(Reviewer.for_user(self.other_user)))
