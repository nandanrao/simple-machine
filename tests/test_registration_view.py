from django_webtest import WebTest
from django.contrib.auth.models import User

from common.models import UserProfile


class RegistrationTestCase(WebTest):
    def setUp(self):
        registration_form = self.app.get('/register/').form
        print registration_form
        registration_form['name'] = 'Some Name'
        registration_form['email'] = 'testemail@gmail.com'
        registration_form['password1'] = 'testuser'
        registration_form['password2'] = 'testuser'
        self.res = registration_form.submit()

    def test_user_can_register_with_name_password_and_email(self):
        self.assertTrue('Registration is complete' in self.res)

    def test_registered_user_can_activate_his_account(self):
        #create sm user
        User.objects.create(email='machine@smplmchn.com', username='sm')
        user = User.objects.get(email='testemail@gmail.com')
        reg_profile = UserProfile.objects.get(user=user)
        activation_link = '/activate/' + reg_profile.activation_key + '/'
        resp = self.app.get(activation_link).follow()
        user = User.objects.get(email='testemail@gmail.com')
        self.assertTrue(user.is_active)

    def test_user_is_instructed_to_provide_other_email_address_if_this_exists(self):
        registration_form = self.app.get('/register/').form
        registration_form['name'] = 'Some Name'
        registration_form['email'] = 'testemail@gmail.com'
        registration_form['password1'] = 'testuser'
        registration_form['password2'] = 'testuser'
        res = registration_form.submit()
        self.assertTrue('Please choose another email address' in res)
