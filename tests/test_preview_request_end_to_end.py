from django_webtest import WebTest
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from common.models import UserProfile
from .factories import FilmFactory
from .helpers import ConversationHelper


class WhenPreviewRequestIsSent(WebTest):
    csrf_checks = False

    def setUp(self):
        self.film_owner = User.objects.create(username='filmowner')
        self.film_owner.get_profile().name = 'film owner'
        self.film_owner.get_profile().save()
        self.venue_owner = User.objects.create(username='venueowner')
        self.venue_owner.get_profile().name = 'venue owner'
        self.venue_owner.get_profile().save()

        self.film = FilmFactory.create(user=self.film_owner, title='film name', profile_image='dummy_filename')

        self.app.post(reverse('send-preview-request', args=[1]), {
            'requester_info': 'some info',
            'request_message': 'please can i see this'
        }, user=self.venue_owner)

    def test_sender_has_new_conversation(self):
        home_page = self.app.get(reverse('home'), user=self.venue_owner)
        self.assertTrue(ConversationHelper.user_has_conversation(home_page, title='film owner <span>(film name)</span>'))

    def test_film_owner_has_new_conversation(self):
        home_page = self.app.get(reverse('home'), user=self.film_owner)
        self.assertTrue(ConversationHelper.user_has_conversation(
            home_page, title='venue owner <span>(film name)</span>'
        ))

    def test_film_owner_has_message_on_conversation_page(self):
        conversation_page = self.app.get(reverse('view-conversation', args=[1]), user=self.film_owner)
        self.assertTrue(ConversationHelper.has_message_in_conversation(conversation_page, title="venue owner wants to watch", subtitle="film name"))

    def test_film_owner_can_accept_or_decline_request_from_conversation(self):
        conversation_page = self.app.get(reverse('view-conversation', args=[1]), user=self.film_owner)
        self.assertTrue(ConversationHelper.has_action_link(conversation_page, link_title='YEA!', link_address=reverse('film-accept-preview-request', args=[1, 1])))
        self.assertTrue(ConversationHelper.has_action_link(conversation_page, link_title='NO!', link_address=reverse('film-decline-preview-request', args=[1, 1])))

    def test_venue_owner_has_no_links_in_conversation_message(self):
        conversation_page = self.app.get(reverse('view-conversation', args=[1]), user=self.venue_owner)
        self.assertTrue(ConversationHelper.has_no_action_links(conversation_page))
