from django.test import TestCase

from screening.forms import VenueForm


class VenueFormTest(TestCase):
    REQUIRED = {
        'name':'some venue',
        'country':'1',
        # 'email':'some@email.com',
        'description':'some desc',
        # 'image':'some_image.jpg',
        'timezone': 'Europe/Zagreb'
    }

    def test_from_is_valid_with_required_fields(self):
        form = VenueForm(self.REQUIRED)
        self.assertTrue(form.is_valid())

    # def test_google_or_yelp_url_must_be_google_or_yelp(self):
    #     form_data = self.REQUIRED.copy()
    #     form_data.update({'google_or_yelp':'www.wrongdomain.com'})
    #     form = VenueForm(form_data)
    #     self.assertTrue('google_or_yelp' in form.errors)
