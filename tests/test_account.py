from django.test import TestCase, Client
from django.contrib.auth.models import User
from common.models import UserProfile
from lxml.cssselect import CSSSelector
from lxml.etree import fromstring, HTMLParser
from django.core.urlresolvers import reverse
from django.core import mail


class PasswordRecoveryTest(TestCase):

    def setUp(self):
        User.objects.all().delete()
        UserProfile.objects.all().delete()
        self.user = User.objects.create_user('some_user', password='pwd',
            email='a@a.com')

    def test_user_can_change_email(self):
        with self.settings(BASE_URL=''):
            c = Client()
            c.login(username='some_user', password='pwd')

            # submit new email address
            c.post(reverse('account_change_email_form_handler'),
                data={
                    'email': 'b@b.com',
                    'password': 'pwd'
                })

            # get link from email
            sel = CSSSelector('a')
            parser = HTMLParser()
            doc = fromstring(mail.outbox[0].body, parser=parser)
            href = [e.get('href') for e in sel(doc)][1]

            # submit new password (using link from email)
            c.get(href)

            # refresh user and check password
            user = User.objects.get(pk=self.user.id)
            self.assertEqual(user.email, 'b@b.com')
