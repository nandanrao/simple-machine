import datetime
import pytz

from django.test import TestCase

from .factories import UserFactory, VenueFactory, ScreeningFactory, FilmFactory
from screening.models import ScreeningEvent
from notifications.management.commands.send_screening_reminders import ReminderGenerator
from notifications.conversation_creation import ConversationCreation
from notifications.models import NewsItemType


class ReminderGeneratorTest(TestCase):
    def setUp(self):
        self.venue_owner = UserFactory.create(username='venueowner')
        self.film_owner = UserFactory.create(username='filmowner')
        ConversationCreation.start_new(self.venue_owner, self.film_owner)
        venue = VenueFactory.create(user=self.venue_owner)
        film = FilmFactory.create(user=self.film_owner)
        self.screening = ScreeningFactory.create(venue=venue, film=film)
        today = datetime.datetime(2012, 10, 25, 21, 00, 00, tzinfo=pytz.utc)
        event = ScreeningEvent(screening=self.screening, time=today)
        event.save()

    def test_generates_first_report_reminder_for_venue_owner_after_last_event_passes(self):
        yesterday = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=1)
        event = ScreeningEvent.objects.create(screening=self.screening, time=yesterday)
        reminders = ReminderGenerator.generate_for(self.screening)
        self.assertEqual(reminders[0].type, NewsItemType.SCREENING_REPORT_VENUE)
        self.assertEqual(reminders[0].for_user, self.venue_owner)
        self.assertTrue(reminders[0].actionable)

    def test_generates_second_report_reminder_for_venue_owner_after_3_days(self):
        three_days_ago = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=3)
        event = ScreeningEvent.objects.create(screening=self.screening, time=three_days_ago)
        reminders = ReminderGenerator.generate_for(self.screening)
        self.assertEqual(reminders[0].type, NewsItemType.SCREENING_REPORT_VENUE)
        self.assertEqual(reminders[0].for_user, self.venue_owner)
        self.assertTrue(reminders[0].actionable)

        self.assertEqual(reminders[1].type, NewsItemType.SCREENING_REPORT_REMINDER_VENUE)
        self.assertEqual(reminders[1].for_user, self.venue_owner)

    def test_generates_first_report_message_for_film_owner_after_6_days(self):
        six_days_ago = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=6)
        event = ScreeningEvent.objects.create(screening=self.screening, time=six_days_ago)
        reminders = ReminderGenerator.generate_for(self.screening)
        self.assertEqual(reminders[0].type, NewsItemType.SCREENING_REPORT_VENUE)
        self.assertEqual(reminders[0].for_user, self.venue_owner)
        self.assertTrue(reminders[0].actionable)

        self.assertEqual(reminders[1].type, NewsItemType.SCREENING_REPORT_REMINDER_VENUE)
        self.assertEqual(reminders[1].for_user, self.venue_owner)

        self.assertEqual(reminders[2].type, NewsItemType.SCREENING_REPORT_FILM)
        self.assertEqual(reminders[2].for_user, self.film_owner)
