import unittest
import os

from nose.plugins.attrib import attr

from PIL import Image

from common.image_cropper import ImageCropper


@attr('integration')
class ImageCropperTestCase(unittest.TestCase):
    ORIGINAL_IMAGE = 'tests/sample.jpg' # 452px wide
    CROPPED_IMAGE = 'tests/cropped.jpg'

    def setUp(self):
        try:
            os.remove(CROPPED_IMAGE)
        except:
            pass

    def test_crops_with_respect_to_aspect_ratio(self):
        orig = open(self.ORIGINAL_IMAGE, 'rb')
        cropped = open(self.CROPPED_IMAGE, 'wb')
        coords = [100, 100, 300, 200]
        max_width = 400

        ImageCropper.crop(orig, cropped, coords, max_width)
        cropped.close()

        cropped = Image.open(self.CROPPED_IMAGE)
        width, height = cropped.size

        #scaling factor is 1.13
        self.assertEquals(width, 200)
        self.assertEquals(height, 100)

    def test_images_less_wide_than_max_width_have_real_size(self):
        orig = open(self.ORIGINAL_IMAGE, 'rb')
        cropped = open(self.CROPPED_IMAGE, 'wb')
        coords = [100, 100, 300, 200]
        max_width = 500

        ImageCropper.crop(orig, cropped, coords, max_width)
        cropped.close()

        cropped = Image.open(self.CROPPED_IMAGE)
        width, height = cropped.size

        # no scaling
        self.assertEquals(width, 200)
        self.assertEquals(height, 100)

    def test_cropper_converts_coordinates_to_integers_if_passed_as_strings(self):
        orig = open(self.ORIGINAL_IMAGE, 'rb')
        cropped = open(self.CROPPED_IMAGE, 'wb')
        coords = ['100', '100', '300', '200']
        max_width = 400

        ImageCropper.crop(orig, cropped, coords, max_width)
        cropped.close()

        cropped = Image.open(self.CROPPED_IMAGE)
        width, height = cropped.size

        self.assertEquals(width, 200)
        self.assertEquals(height, 100)

    def test_cropped_non_integer_coordinates(self):
        orig = open(self.ORIGINAL_IMAGE, 'rb')
        cropped = open(self.CROPPED_IMAGE, 'wb')
        coords = ['100', '100.1', '300.2', '200']
        max_width = 400

        ImageCropper.crop(orig, cropped, coords, max_width)
        cropped.close()

        cropped = Image.open(self.CROPPED_IMAGE)
        width, height = cropped.size

        self.assertEquals(width, 200)
        self.assertEquals(height, 100)
