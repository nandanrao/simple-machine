import os
import unittest

from film.proftpd_adapter import ProftpdAdapter
from django.conf import settings


class ProftpdAdapterTest(unittest.TestCase):

    def setUp(self):
        self.db = os.tmpnam()
        self.pc = ProftpdAdapter(db_path=self.db)

    def tearDown(self):
        self.pc.close()
        os.unlink(self.db)
        os.system('rm -rf %s/*' % ProftpdAdapter.get_incoming_directory())

    def test_create_user(self):
        ref, user, pwd, home = self.pc.create_user(42, 'user', 'pwd')
        self.assertEqual(ref, 42)
        self.assertEqual(user, 'user')
        self.assertEqual(pwd, 'pwd')
        self.assertEqual(home,
            os.path.join(self.pc.get_incoming_directory(), user))

    def test_create_random_user(self):
        ref1, user1, pwd1, home1 = self.pc.create_random_user(42)
        ref2, user2, pwd2, home2 = self.pc.create_random_user(31337)
        self.assertNotEqual(user1, user2)
        self.assertNotEqual(pwd1, pwd2)
        self.assertNotEqual(home1, home2)

    def test_unique_reference(self):
        self.pc.create_random_user(42)
        self.assertRaises(ValueError, self.pc.create_random_user, 42)

    def test_get_file_reference_failure(self):
        self.assertIsNone(self.pc.get_file_reference('/foo.txt'))
        self.assertIsNone(self.pc.get_file_reference(
            self.pc.get_incoming_directory()))

    def test_get_file_reference(self):
        ref, user, pwd, home = self.pc.create_user(42, 'user', 'pwd')
        fakefile = os.path.join(self.pc.get_incoming_directory(), user,
            'test.txt')

        self.assertEqual(ref, self.pc.get_file_reference(fakefile))

    def test_delete_user(self):
        ref, user, pwd, home = self.pc.create_user(42, 'user', 'pwd')
        self.pc.delete_user(ref)
        self.assertEqual(0, self.pc._select_single_field(
            'SELECT COUNT(*) FROM users WHERE ref = ?', ref))

    def test_delete_user_failure(self):
        self.assertRaises(ValueError, self.pc.delete_user, 42)
