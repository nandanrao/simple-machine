from mock import patch
from django import test
from nose.plugins.attrib import attr
from django.contrib.auth.models import User
from django.conf import settings

from common.user_creation import UserCreation, DuplicateEmail, ACTIVATION_EMAIL_TEXT


@attr('integration')
class UserCreationTest(test.TestCase):
    def setUp(self):
        User.objects.all().delete()
        UserCreation.create(email='test@example.com',
                            name='Some User',
                            password='somepass')

    def test_creates_user(self):
        self.assertTrue(User.objects.filter(email='test@example.com').exists())

    def test_correctly_stores_name(self):
        user = User.objects.get(email='test@example.com')
        self.assertEqual(user.get_profile().name, 'Some User')

    def test_stores_encrypted_password(self):
        user = User.objects.get(email='test@example.com')
        self.assertTrue('pbkdf2_sha256$10000' in user.password)
        self.assertTrue('somepass' not in user.password)

    def test_throws_exception_if_user_already_exists(self):
        with self.assertRaises(DuplicateEmail):
            UserCreation.create(email='test@example.com',
                                name='Some User',
                                password='somepass')

    def test_generates_activation_key(self):
        user = User.objects.get(email='test@example.com')
        # activation_key is SHA1 hash
        self.assertTrue(len(user.get_profile().activation_key) == 40)
