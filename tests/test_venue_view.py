import json

from  django_webtest import WebTest
from .factories import UserFactory
from .helpers import upload_file


class AddVenueTestCase(WebTest):
    def setUp(self):
        self.user = UserFactory.create(username='username10')

    def test_user_can_add_venue(self):
        venue_form = self.app.get('/venues/add/', user=self.user).forms['add-venue-form']
        venue_form['name'] = 'some title'
        # venue_form['street_address'] = 'some title'
        venue_form['description'] = 'some desc'
        venue_form['website'] = 'www.google.com'
        venue_form['city'] = 'www.google.com'
        venue_form['phone_number'] = 'www.google.com'
        venue_form['county'] = 'www.google.com'
        venue_form['state'] = 'www.google.com'
        # venue_form['email'] = 'goran.peretin@dobarkod.hr'
        venue_form['timezone'] = 'Europe/Zagreb'
        # venue_form['google_or_yelp'] = 'www.google.com'
        venue_form['country'] = '2'
        csrf = venue_form['csrfmiddlewaretoken'].value
        # resp = upload_file(self.app, '/file/tmp_upload/', self.user, str(csrf))
        # venue_form['image'] = json.loads(resp.content)['filename']
        # venue_form['coordinates'] = ['100,100,200,200']
        res = venue_form.submit().follow()
        self.assertTrue('Venue added' in res)
