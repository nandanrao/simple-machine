import unittest

from nose.plugins.attrib import attr

from screening.presenter import ScreeningPresenter
from screening.models import ProfitShare
from .factories import ScreeningFactory, UserFactory, FilmFactory, VenueFactory


@attr('unit')
class OnScreeningPageUserTest(unittest.TestCase):
    def test_cant_see_decision_menu_if_he_is_not_film_owner(self):
        user = UserFactory.build(id=1)
        user2 = UserFactory.build(id=2)
        film = FilmFactory.build(user=user2)
        screening = ScreeningFactory.build(film=film)
        sp = ScreeningPresenter(screening, user)
        self.assertFalse(sp['show_decision_menu'])

    def test_can_see_admin_menu_if_he_is_venue_owner(self):
        user = UserFactory.build(id=1)
        user2 = UserFactory.build(id=2)
        film = FilmFactory.build(user=user2)
        venue = VenueFactory.build(user=user)
        screening = ScreeningFactory.build(film=film, venue=venue)
        sp = ScreeningPresenter(screening, user)
        self.assertTrue(sp['show_admin_menu'])

    def test_can_see_profit_share(self):
        user = UserFactory.build(id=1)
        user2 = UserFactory.build(id=2)
        film = FilmFactory.build(user=user2)
        profit = ProfitShare(type='SHARE')
        screening = ScreeningFactory.build(film=film, profit=profit)
        sp = ScreeningPresenter(screening, user)
        self.assertTrue(sp['profit_share'])

    def test_can_see_upfront_amount(self):
        user = UserFactory.build(id=1)
        user2 = UserFactory.build(id=2)
        film = FilmFactory.build(user=user2)
        profit = ProfitShare(type='UPFRONT')
        screening = ScreeningFactory.build(film=film, profit=profit)
        sp = ScreeningPresenter(screening, user)
        self.assertTrue(sp['upfront'])

    def test_can_see_nonprofit(self):
        user = UserFactory.build(id=1)
        user2 = UserFactory.build(id=2)
        film = FilmFactory.build(user=user2)
        profit = ProfitShare(type='NOPROFIT')
        screening = ScreeningFactory.build(film=film, profit=profit)
        sp = ScreeningPresenter(screening, user)
        self.assertTrue(sp['noprofit'])
