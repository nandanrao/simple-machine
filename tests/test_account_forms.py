from django.test import TestCase
from common.forms import ChangePasswordForm, ChangeEmailForm
from django.contrib.auth.models import User


class ChangePasswordFormTest(TestCase):

    def setUp(self):
        self.user = User()
        self.user.set_password('a')

    def test_required_fields(self):
        form = ChangePasswordForm(self.user, {})
        self.assertFalse(form.is_valid())
        self.assertTrue(
            'This field is required.' in form.errors['current_password'])
        self.assertTrue(
            'This field is required.' in form.errors['new_password'])
        self.assertTrue(
            'This field is required.' in form.errors['new_password_rpt'])
        self.assertEqual(len(form.errors), 3)

    def test_wrong_repeat_field(self):
        form = ChangePasswordForm(self.user, {
            'current_password': 'a',
            'new_password': 'b',
            'new_password_rpt': 'c',
        })
        self.assertFalse(form.is_valid())
        self.assertTrue('repeated password is not the same as new password' \
            in form.errors['new_password_rpt'])
        self.assertEqual(len(form.errors), 1)

    def test_wrong_current_password(self):
        form = ChangePasswordForm(self.user, {
            'current_password': 'q',
            'new_password': 'b',
            'new_password_rpt': 'b',
        })
        self.assertFalse(form.is_valid())
        self.assertTrue('wrong password' in form.errors['current_password'])
        self.assertEqual(len(form.errors), 1)

    def test_except_in_change_password_if_form_not_valid(self):
        form = ChangePasswordForm(self.user, {
            'current_password': 'q',
            'new_password': 'b',
            'new_password_rpt': 'b',
        })
        self.assertRaises(ValueError, form.change_password)


class ChangeEmailFormTest(TestCase):

    def setUp(self):
        self.user = User()
        self.user.set_password('a')
        self.user.email = 'a@a.com'

    def test_required_fields(self):
        form = ChangeEmailForm(self.user, {})
        self.assertFalse(form.is_valid())
        self.assertTrue(
            'This field is required.' in form.errors['password'])
        self.assertTrue(
            'This field is required.' in form.errors['email'])
        self.assertEqual(len(form.errors), 2)

    def test_wrong_password(self):
        form = ChangeEmailForm(self.user, {
            'email': 'b@b.com',
            'password': 'b',
        })
        self.assertFalse(form.is_valid())
        self.assertTrue('wrong password' in form.errors['password'])
        self.assertEqual(len(form.errors), 1)

    def test_email_is_users_current_email(self):
        form = ChangeEmailForm(self.user, {
            'email': 'a@a.com',
            'password': 'a',
        })
        self.assertFalse(form.is_valid())
        self.assertTrue('a@a.com is your current email address' in form.errors['email'])
        self.assertEqual(len(form.errors), 1)

    def test_except_in_change_password_if_form_not_valid(self):
        form = ChangeEmailForm(self.user, {})
        self.assertRaises(ValueError, form.send_mail)
