from django.test import TestCase, Client
from django.contrib.auth.models import User
from common.models import UserProfile
from django.core.urlresolvers import reverse
from mock import patch


class EmailChangeViewTest(TestCase):

    def setUp(self):
        User.objects.all().delete()
        UserProfile.objects.all().delete()
        self.user = User.objects.create_user('some_user', password='pwd',
            email='a@a.com')

    @patch('common.forms.EmailNotification')
    def test_valid_form_submit_calls_send_email_change_email(self,
        EmailNotification):

        c = Client()
        c.login(username='some_user', password='pwd')
        c.post(reverse('account_change_email_form_handler'), data={
            'password': 'pwd',
            'email': 'b@b.com'
        })
        EmailNotification.send_email_change_email.assert_called_once_with(
            self.user.id, 'b@b.com')
