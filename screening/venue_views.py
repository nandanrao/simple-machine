import os
import json

from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template.loader import render_to_string
from django import http

from .models import Venue, FilmOnVenue
from .forms import VenueForm, VenueFilmForm
from film.profile_image_cropper import ProfileImageCropper
from common.browse_views import _get_item_pagination
from common.browse import Browse
from film.models import Film


def venue_details(request, pk):
    venue = get_object_or_404(Venue, pk=pk)
    slug = slugify(venue.name)
    if not slug in request.get_full_path():
        return redirect(reverse('venue-details', args=[pk]) + slug)
    return render(request, 'venue_details.html', {'venue': venue})


def _needs_cropping(venue, form_image):
    if venue and os.path.basename(venue.profile_image.name) == os.path.basename(form_image):
        return False
    return True


def venue_films(request, pk):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    venue = get_object_or_404(Venue, pk=pk)
    films = [film.film for film in venue.films.order_by('-film__year').all() if film.film.is_public
                and film.film.runtime >= min_value and film.film.runtime <= max_value
                and film.film.year >= min_year and film.film.year <= max_year
                and (country_id == 'all' or film.film.countries.filter(pk=country_id))]
    return render(request, 'partials/_film_details_module.html', {
        'films': _get_item_pagination(films, request)
    })


def add_venue(request, pk=None):
    if pk:
        venue = Venue.objects.get(pk=pk)
    else:
        venue = None

    if venue:
        form = VenueForm(request.POST or None, instance=venue)
    else:
        form = VenueForm(request.POST or None)

    if form.is_valid():
        if venue:
            messages.success(request, 'Venue updated')
        else:
            messages.success(request, 'Venue added')
        form.instance.user = request.user
        venue = form.save()
        return redirect('add-venue-films', venue.pk)

    return render(request, 'add_venue/venue_form.html', {
        'form': form,
        'venue': venue
    })


@login_required
def add_venue_films(request, pk):
    venue = get_object_or_404(Venue, pk=pk)
    if not venue.belongs_to(request.user) or venue.deleted:
        return redirect('home')

    # current_step = FILM_STEPS['FESTIVALS']

    form = VenueFilmForm(request.POST or None)
    if form.is_valid():
        fovs = FilmOnVenue.objects.filter(venue=venue)
        for f in fovs:
            f.make_active()
            f.venue.make_active()
            f.venue.save()
            f.save()
        film.save()
        return redirect('venue-details', pk)

    fovs = venue.films.all()
    available_years = [{'id': str(y[0]), 'text': str(y[0])} for y in FilmOnVenue.VENUE_YEARS[::-1]]

    return render(request, 'add_venue/film_to_venue.html', {
        'form': form,
        'venue': venue,
        'available_years': available_years,
        # 'current_step': current_step,
        'film_on_venue': fovs,
        'next_step_url': reverse('venue-details', args=[pk])
    })


@login_required
def delete_fov(request):
    fov_id = request.POST.get('id', None)

    # Delete from database
    if fov_id:
        if FilmOnVenue.objects.filter(pk=int(fov_id)).exists():
            fov = FilmOnVenue.objects.get(pk=(fov_id))
            venue = fov.venue
            if venue.user == request.user:
                fov.delete()

        html = render_to_string('add_venue/_added_films.html', {
            'film_on_venue': FilmOnVenue.objects.filter(venue=venue)
        })
        success = True
        ret_val = {'success': success, 'html': html}
        return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')

    return redirect('home')


@login_required
def add_venue_films_ajax(request):
    film_id = request.POST.get('film_id', None)
    film = Film.objects.get(pk=film_id)
    section = request.POST.get('section', None)
    awards = request.POST.get('awards', None)
    year = request.POST.get('year', None)
    venue_pk = request.POST.get('venue_pk', None)
    venue = Venue.objects.get(pk=venue_pk)

    if not film:
        # User didn't pick festival, ignore
        resp = HttpResponse("Error: festival missing")
        resp.status_code = 400
        return resp

    pk = request.POST.get('pk', None)
    if pk:
        pk = int(pk)
        f = FilmOnVenue.objects.get(pk=pk)
        f.film = film
        f.section = section
        f.awards = awards
        f.year = year
    else:
        f = FilmOnVenue(film=film, section=section, awards=awards, year=year, venue=venue)

    f.save()

    fovs = FilmOnVenue.objects.filter(venue=venue)
    return render(request, 'add_venue/_added_films.html', {
        'film_on_venue': fovs
    })


def delete_venue(request, pk):
    venue = get_object_or_404(Venue, pk=pk)
    if venue.user == request.user:
        venue.deleted = True
        venue.save()
        messages.success(request, 'Venue deleted!')
    return redirect('home')


def venues_by_letter(request, letter):
    venues = Venue.objects.filter(name__startswith=letter)
    return render(request, 'partials/_browse_venue_results.html', {'venues': venues})


def venues_by_film(request, film_id):
    venues = Venue.have_shown(film_id)
    return render(request, 'partials/_browse_venue_results.html', {'venues': venues})
