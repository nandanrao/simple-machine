from django.conf.urls.defaults import patterns, url

from .views import *

urlpatterns = patterns(
    '',
    url(r'^add/$', create_screening, name='add-screening'),
    url(r'^(?P<pk>\d+)/$', view_screening, name='screening-details'),
    url(r'^(?P<pk>\d+)/public/$', screening_page, name='screening-page'),
    url(r'^(?P<pk>\d+)/accept/$', accept_screening, name='accept-screening'),
    url(r'^(?P<pk>\d+)/decline/$', decline_screening, name='decline-screening'),
    url(r'^(?P<pk>\d+)/cancel/$', cancel_screening, name='cancel-screening'),
    url(r'^(?P<pk>\d+)/pay/$', screening_payment_form, name='screening-payment'),
    url(r'^(?P<pk>\d+)/paid/$', screening_paid, name='screening-paid'),

    url(r'^(?P<pk>\d+)/edit/$', create_screening, name='edit-screening'),
    url(r'^(?P<pk>\d+)/report/add/$', create_screening_report, name='create-screening-report'),
    url(r'^(?P<pk>\d+)/report/add/$', create_screening_report, name='edit-screening-report'),
    url(r'^(?P<pk>\d+)/report/photo/upload/$', upload_screening_report_photo, name='upload-screening-report-photo'),
    url(r'^(?P<pk>\d+)/report/photo/remove/$', remove_screening_report_photo, name='remove-screening-report-photo'),
    url(r'^(?P<pk>\d+)/report/film_owner/$', fill_screening_report, name='fill-screening-report'),
)
