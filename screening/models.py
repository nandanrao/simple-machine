import hashlib
import datetime
import random
import pytz
from django.db import models
from django.db.models import Q, Count
from django.contrib.auth.models import User
from copy import deepcopy
from pytz import timezone

from common.models import Country
from film.models import Film
from notifications.models import NewsItem, NewsItemType


class ActiveVenuesManager(models.Manager):
    def get_query_set(self):
        return super(ActiveVenuesManager, self).get_query_set().filter(deleted=False)


class Venue(models.Model):
    TIMEZONES = map(lambda tz: (tz, tz.replace('_', ' ')), pytz.common_timezones)

    name = models.CharField(max_length=250)
    # street_address = models.CharField(max_length=250, null=True, blank=True)
    city = models.CharField(max_length=250, null=True, blank=True)
    county = models.CharField(max_length=250, null=True, blank=True)
    state = models.CharField(max_length=250, null=True, blank=True)
    country = models.ForeignKey(Country)
    timezone = models.CharField(max_length=100, choices=TIMEZONES)
    phone_number = models.TextField(max_length=30, null=True, blank=True)
    # email = models.TextField(max_length=250, null=True, blank=True)
    # google_or_yelp = models.URLField(max_length=250, null=True, blank=True)
    website = models.URLField(max_length=250, null=True, blank=True)
    description = models.TextField()
    deleted = models.BooleanField(default=False)
    # profile_image = models.ImageField(upload_to='venues/profile_images', null=True, blank=True)
    #is_verified = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='venues')

    objects = models.Manager()
    active = ActiveVenuesManager()

    def __unicode__(self):
        return self.name

    def model_class(self):
        return 'Venue'

    def belongs_to(self, user):
        return True if self.user == user else False

    @property
    def metadesc(self):
        return ','.join([self.name, self.street_address, self.city, self.country.name, self.description])

    @staticmethod
    def by_user(user):
        return Venue.active.filter(user=user)

    def url_service(self):
        if self.google_or_yelp.startswith('http://www.google.com'):
            return 'GOOGLE'
        elif self.google_or_yelp.startswith('http://www.yelp.com'):
            return 'YELP'
        return 'REVIEW'

    @staticmethod
    def get_with_film_count(min_length, max_length, min_year, max_year, country_id):
        if country_id != 'all':
            venues = Venue.active.filter(
                screenings__cancelled=False,
                screenings__decision='APPROVED',
                films__film__runtime__range=(min_length, max_length),
                films__film__year__range=(min_year, max_year),
                films__film__is_public=True,
                films__film__deleted=False,
                films__film__countries__pk=country_id
            )
        else:
            venues = Venue.active.filter(
                screenings__cancelled=False,
                screenings__decision='APPROVED',
                films__film__runtime__range=(min_length, max_length),
                films__film__year__range=(min_year, max_year),
                films__film__is_public=True,
                films__film__deleted=False
            )

        return venues.annotate(screening_count=Count('screenings')).annotate(film_count=Count('screenings__film', distinct=True))

    def get_random_film(self):
        return random.choice(self.films.all()).film if self.films.count() else None

    @property
    def public_screenings(self):
        return [s for s in self.screenings.all() if s.is_published and s.film.is_public]

    def has_in_progress_screening_of(self, film):
        for screening in self.screenings.all():
            if screening.is_accepted and screening.film == film and not screening.is_published:
                return True

        return False

    @staticmethod
    def have_shown(film_id):
        """
        Returns all venues that have a published screening for given film.
        """
        film = Film.active.get(pk=film_id)
        screenings = Screening.objects.filter(film=film)
        screenings = [s for s in screenings if s.is_published]
        return [s.venue for s in screenings]


class FilmOnVenue(models.Model):
    VENUE_START_YEAR = 1932
    VENUE_END_YEAR = 2015
    VENUE_YEARS = zip(
        range(VENUE_START_YEAR, VENUE_END_YEAR + 1),
        range(VENUE_START_YEAR, VENUE_END_YEAR + 1)
    )

    section = models.CharField(max_length=200, null=True, blank=True)
    awards = models.CharField(max_length=200, null=True, blank=True)
    venue = models.ForeignKey(Venue, related_name='films')
    year = models.PositiveIntegerField(choices=VENUE_YEARS, null=True, blank=True)
    film = models.ForeignKey(Film, related_name='venues')

    def __unicode__(self):
        return self.film.title + ' on ' + self.venue.name


class ProfitShare(models.Model):
    TYPES = (
        ('NOPROFIT', 'No profit'),
        ('SHARE', 'Share'),
        ('UPFRONT', 'Up front')
    )
    # These two are percentages, used when type == 'SHARE'
    film_share = models.IntegerField(null=True, blank=True)
    smplmchn_share = models.IntegerField(null=True, blank=True)

    # These two are exact amounts, used when type == 'UPFRONT'
    film_upfront = models.DecimalField(null=True, blank=True, max_digits=12, decimal_places=2)
    smplmchn_upfront = models.DecimalField(null=True, blank=True, max_digits=12, decimal_places=2)
    type = models.CharField(max_length=20, choices=TYPES)

    def __unicode__(self):
        return self.type

    @property
    def venue_share(self):
        if self.film_share and self.smplmchn_share:
            return 100 - self.film_share - self.smplmchn_share
        return None

    @property
    def venue_upfront(self):
        if self.film_upfront and self.smplmchn_upfront:
            return self.film_upfront + self.smplmchn_upfront
        return None


class ScreeningReport(models.Model):
    description = models.TextField()
    total_revenue = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return 'Yes ($' + str(self.total_revenue) + ')' if self.total_revenue else 'Yes'


class ScreeningReportFilmOwner(models.Model):
    PAID_PROMPTLY = (
        ('YES', 'Yes'),
        ('KINDA', 'Kinda'),
        ('NO', 'No')
    )
    RECOMMEND_VENUE = (
        ('YES', 'Yes'),
        ('EH', 'Eh'),
        ('NO', 'No')
    )
    REPORT_FRAUD = (
        ('YES', 'Yes'),
        ('NO', 'No')
    )

    description = models.TextField()
    paid_promptly = models.CharField(max_length=20, choices=PAID_PROMPTLY, blank=True, null=True)
    recommend_venue = models.CharField(max_length=20, choices=RECOMMEND_VENUE)
    report_fraud = models.CharField(max_length=20, choices=REPORT_FRAUD)


class Screening(models.Model):
    DECISIONS = (
        ('APPROVED', 'Approved'),
        ('DECLINED', 'Declined')
    )

    created_on = models.DateTimeField(auto_now_add=True)
    description = models.TextField()
    venue = models.ForeignKey(Venue, related_name='screenings')
    film = models.ForeignKey(Film, related_name='screenings')
    decision = models.CharField(max_length=20, choices=DECISIONS, null=True, blank=True)
    profit = models.OneToOneField(ProfitShare, related_name='screening', null=True, blank=True)
    cancelled = models.BooleanField(default=False)
    report = models.OneToOneField(ScreeningReport, null=True, blank=True)
    film_report = models.OneToOneField(ScreeningReportFilmOwner, null=True, blank=True)
    paid = models.BooleanField(default=False)

    def __unicode__(self):
        return self.film.title + ' @ ' + self.venue.name + '(' + str(self.id) + ')'

    @staticmethod
    def all_published():
        screenings = Screening.objects.filter(cancelled=False).all()
        return [s for s in screenings if s.is_published]

    @staticmethod
    def all_valid():
        screenings = Screening.objects.filter(cancelled=False).all()
        return [s for s in screenings if s.is_accepted]

    @staticmethod
    def for_user(user):
        return Screening.objects.filter(
            (Q(venue__user = user) | Q(film__user = user)) & Q(cancelled = False)
        ).prefetch_related('schedule')

    @property
    def title(self):
        return self.film.title + " @ " + self.venue.name

    @property
    def decision_made(self):
        return True if self.decision else False

    def approve(self):
        self.decision = 'APPROVED'

    def decline(self):
        self.decision = 'DECLINED'

    @property
    def is_accepted(self):
        return self.decision == 'APPROVED'

    @property
    def is_declined(self):
        return self.decision == 'DECLINED'

    @property
    def is_updated(self):
        film_owner_message = NewsItem.get_for_user(
            self.film.user,
            NewsItemType.SCREENING_UPDATED,
            object=self
        )
        return True if film_owner_message else False

    def cancel(self):
        self.cancelled = True

    def add_screening_at(self, t):
        if not self.schedule.filter(time=t).exists():
            self.schedule.add(ScreeningEvent(screening=self, time=t))

    @property
    def film_owners_earning(self):
        if self.profit.type == 'UPFRONT':
            return float(self.profit.film_upfront)
        elif self.profit.type == 'SHARE':
            if not self.report or not self.report.total_revenue:
                return None
            return float(self.report.total_revenue * (float(self.profit.film_share)/100))

    @property
    def simplemachine_earning(self):
        if self.profit.type == 'UPFRONT':
            return float(self.profit.smplmchn_upfront)
        elif self.profit.type == 'SHARE':
            if not self.report or not self.report.total_revenue:
                return None
            return float(self.report.total_revenue * (float(self.profit.smplmchn_share)/100))
        return None

    @property
    def venue_owners_earning(self):
        if not self.report.total_revenue:
            return None

        return self.report.total_revenue - (self.film_owners_earning + self.simplemachine_earning)

    @property
    def total_payment(self):
        return self.film_owners_earning + self.simplemachine_earning

    @property
    def film_owners_cut(self):
        return self.profit.film_share if self.profit.type=='SHARE' else None

    @property
    def venue_owners_cut(self):
        return self.profit.venue_share

    @property
    def is_public(self):
        return True if self.report and self.film_report else False

    def is_organized_by(self, user):
        return True if self.venue.user == user else False

    @property
    def has_published_report(self):
        if self.paid and self.report:
            return True
        elif self.film_report:
            return True
        return False

    @property
    def last_event_passed(self):
        now = datetime.datetime.now(pytz.utc)
        last_event = self.schedule.latest('time').time
        return True if now > last_event else False

    @property
    def days_past_since_last_event(self):
        now = datetime.datetime.now(pytz.utc)
        last_event = self.schedule.latest('time').time
        delta = now - last_event
        return delta.days

    @property
    def requires_payment(self):
        if self.paid or self.profit.type == 'NOPROFIT':
            return False
        return True

    @property
    def requires_payment_now(self):
        if self.is_accepted and not self.paid:
            if self.profit.type=='SHARE' and self.first_event_started:
                return True
            elif self.profit.type=='UPFRONT':
                return True

        return False

    @property
    def payment_token(self):
        m = hashlib.sha1()
        m.update(str(self.created_on) + str(self.id))
        return m.hexdigest()

    @property
    def first_event_started(self):
        now = datetime.datetime.now(pytz.utc)
        first_event = self.schedule.order_by('time')[0].time
        return True if now > first_event else False

    @property
    def date_of_first_event(self):
        return self.schedule.order_by('time')[0].time

    @property
    def is_editable(self):
        return not self.first_event_started

    @property
    def can_be_canceled(self):
        return not self.first_event_started and not self.paid

    @property
    def is_ready_for_report(self):
        if self.first_event_started:
            if self.is_accepted or self.film.user == self.venue.user:
                return True

        return False

    @property
    def is_published(self):
        if self.report and self.film_report:
            return True

        if self.report and self.film.user == self.venue.user:
            return True

        if self.days_past_since_last_event >= 6 and self.film_report:
            return True

        film_owner_message = NewsItem.get_for_user(self.film.user,NewsItemType.SCREENING_REPORT_FILM, object=self)
        if film_owner_message:
            if self.report and film_owner_message.days_passed_since_sent >= 6:
                return True

        return False

    @property
    def time_to_next_event(self):
        now = datetime.datetime.now(pytz.utc)
        future_events = self.schedule.filter(time__gt=now).order_by("time")
        if future_events:
            return future_events[0].time - now
        else:
            # TODO: this is hack!
            return datetime.timedelta(1000)

    @property
    def is_dead(self):
        if self.cancelled or self.is_declined:
            return True
        elif self.first_event_started and not self.is_accepted:
            return True
        else:
            return False

    @staticmethod
    def vo_late_with_report():
        se = ScreeningEvent.older_than(6)
        ids = [s.id for s in se]
        return Screening.objects.filter(cancelled=False).filter(report__isnull=True).filter(id__in=ids)


    @staticmethod
    def fo_late_with_report():
        se = ScreeningEvent.older_than(12)
        ids = [s.id for s in se]
        return Screening.objects.filter(cancelled=False).filter(film_report__isnull=True).filter(id__in=ids)


class ScreeningReportPhoto(models.Model):
    image = models.ImageField(upload_to='screenings/photos')
    screening_report = models.ForeignKey(ScreeningReport, related_name='photos', null=True)


class ScreeningEvent(models.Model):
    screening = models.ForeignKey(Screening, related_name='schedule')
    time = models.DateTimeField()

    class Meta:
        get_latest_by = 'time'

    @staticmethod
    def older_than(days):
        days_ago = datetime.datetime.now(pytz.utc) - datetime.timedelta(days=days)
        return ScreeningEvent.objects.filter(time__lte=days_ago)
