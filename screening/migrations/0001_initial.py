# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Venue'
        db.create_table('screening_venue', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('street_address', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('county', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['common.Country'])),
            ('phone_number', self.gf('django.db.models.fields.TextField')(max_length=30, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.TextField')(max_length=250)),
            ('google_or_yelp', self.gf('django.db.models.fields.URLField')(max_length=250, null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=250, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('profile_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='venues', to=orm['auth.User'])),
        ))
        db.send_create_signal('screening', ['Venue'])

        # Adding model 'ProfitShare'
        db.create_table('screening_profitshare', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('film_share', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('smplmchn_share', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('film_upfront', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=2, blank=True)),
            ('smplmchn_upfront', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=12, decimal_places=2, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('screening', ['ProfitShare'])

        # Adding model 'ScreeningReport'
        db.create_table('screening_screeningreport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('total_revenue', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('screening', ['ScreeningReport'])

        # Adding model 'ScreeningReportFilmOwner'
        db.create_table('screening_screeningreportfilmowner', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('paid_promptly', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('recommend_venue', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('report_fraud', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('screening', ['ScreeningReportFilmOwner'])

        # Adding model 'Screening'
        db.create_table('screening_screening', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('description', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('venue', self.gf('django.db.models.fields.related.ForeignKey')(related_name='screenings', to=orm['screening.Venue'])),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='screenings', to=orm['film.Film'])),
            ('decision', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('profit', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='screening', unique=True, null=True, to=orm['screening.ProfitShare'])),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('report', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['screening.ScreeningReport'], unique=True, null=True, blank=True)),
            ('film_report', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['screening.ScreeningReportFilmOwner'], unique=True, null=True, blank=True)),
            ('paid', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('screening', ['Screening'])

        # Adding model 'ScreeningEvent'
        db.create_table('screening_screeningevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('screening', self.gf('django.db.models.fields.related.ForeignKey')(related_name='schedule', to=orm['screening.Screening'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('screening', ['ScreeningEvent'])


    def backwards(self, orm):
        # Deleting model 'Venue'
        db.delete_table('screening_venue')

        # Deleting model 'ProfitShare'
        db.delete_table('screening_profitshare')

        # Deleting model 'ScreeningReport'
        db.delete_table('screening_screeningreport')

        # Deleting model 'ScreeningReportFilmOwner'
        db.delete_table('screening_screeningreportfilmowner')

        # Deleting model 'Screening'
        db.delete_table('screening_screening')

        # Deleting model 'ScreeningEvent'
        db.delete_table('screening_screeningevent')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'common.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'film.exhibitionfile': {
            'Meta': {'object_name': 'ExhibitionFile'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uploaded': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'film.film': {
            'Meta': {'object_name': 'Film'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'films'", 'symmetrical': 'False', 'to': "orm['common.Country']"}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'exhibition_file': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'film'", 'unique': 'True', 'null': 'True', 'to': "orm['film.ExhibitionFile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'profile_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'runtime': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'synopsis': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'trailer_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'films'", 'to': "orm['auth.User']"}),
            'year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'screening.profitshare': {
            'Meta': {'object_name': 'ProfitShare'},
            'film_share': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'film_upfront': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'smplmchn_share': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'smplmchn_upfront': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '12', 'decimal_places': '2', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'screening.screening': {
            'Meta': {'object_name': 'Screening'},
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'decision': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'screenings'", 'to': "orm['film.Film']"}),
            'film_report': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['screening.ScreeningReportFilmOwner']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paid': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'profit': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'screening'", 'unique': 'True', 'null': 'True', 'to': "orm['screening.ProfitShare']"}),
            'report': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['screening.ScreeningReport']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'venue': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'screenings'", 'to': "orm['screening.Venue']"})
        },
        'screening.screeningevent': {
            'Meta': {'object_name': 'ScreeningEvent'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'screening': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schedule'", 'to': "orm['screening.Screening']"}),
            'time': ('django.db.models.fields.DateTimeField', [], {})
        },
        'screening.screeningreport': {
            'Meta': {'object_name': 'ScreeningReport'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'total_revenue': ('django.db.models.fields.IntegerField', [], {})
        },
        'screening.screeningreportfilmowner': {
            'Meta': {'object_name': 'ScreeningReportFilmOwner'},
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'paid_promptly': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'recommend_venue': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'report_fraud': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'screening.venue': {
            'Meta': {'object_name': 'Venue'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['common.Country']"}),
            'county': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'email': ('django.db.models.fields.TextField', [], {'max_length': '250'}),
            'google_or_yelp': ('django.db.models.fields.URLField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'phone_number': ('django.db.models.fields.TextField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'profile_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'street_address': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'venues'", 'to': "orm['auth.User']"}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['screening']