import decimal
import datetime
import re
import pytz
from dateutil import parser as iso8601_parser

from django import forms
from django_select2.widgets import Select2Widget
from django_select2.fields import ModelSelect2Field, Select2ChoiceField
from django.forms.widgets import HiddenInput
from django.utils.timezone import utc
from pytz import timezone

from common.models import Country
from common.html5_fields import PhoneInput, EmailInput, URLInput, NumberInput
from film.models import Film
from .models import Screening, Venue, ScreeningReport, ScreeningReportFilmOwner, FilmOnVenue


class ScreeningForm(forms.ModelForm):
    film = forms.ModelChoiceField(
        queryset=Film.active.all(),
        empty_label='',
        widget=forms.TextInput(attrs={'data-placeholder': 'Search by film title'})
    )
    venue = forms.ModelChoiceField(
        empty_label='Venue *',
        queryset=Venue.active.all(),

    )
    screening_times = forms.CharField(
        widget=HiddenInput(attrs={'id':'screening-times-id'})
    )

    share_film = forms.IntegerField(widget=NumberInput, required=False, max_value=90, min_value=1)
    share_venue = forms.IntegerField(widget=NumberInput, required=False, max_value=89, min_value=0)

    upfront_film = forms.DecimalField(
        required=False,
        max_digits=12,
        decimal_places=2,
        min_value=decimal.Decimal('0.01')
    )
    upfront_smplmchn = forms.DecimalField(
        required=False,
        max_digits=12,
        decimal_places=2,
        min_value=decimal.Decimal('0.01')
    )
    upfront_venue = forms.DecimalField(
        required=False,
        max_digits=12,
        decimal_places=2,
        min_value=decimal.Decimal('0.01')
    )

    profit_share_type = forms.CharField(
        widget=HiddenInput(attrs={'id':'profit-share-type-id'}),
        required=False
    )

    def _clean_when_shared(self):
        if 'share_film' in self.cleaned_data and 'share_venue' in self.cleaned_data:
            if not self.cleaned_data['share_film']:
                raise forms.ValidationError('Please enter shares for Venue and Film')
            if int(self.cleaned_data['share_film']) + int(self.cleaned_data['share_venue']) + 10 != 100:
                raise forms.ValidationError('Profit shares must sum up to 100%')

    def _clean_when_upfront(self):
        if 'upfront_film' in self.cleaned_data and 'upfront_venue' in self.cleaned_data  and 'upfront_smplmchn' in self.cleaned_data:
            if not self.cleaned_data['upfront_film'] or not self.cleaned_data['upfront_venue'] or not self.cleaned_data['upfront_smplmchn']:
                raise forms.ValidationError('Please enter up-front amount')

            film = self.cleaned_data['upfront_film']
            venue = self.cleaned_data['upfront_venue']
            smplmchn = self.cleaned_data['upfront_smplmchn']

            if film + smplmchn != venue:
                raise forms.ValidationError('Venue amount must equal SMPLMCHN amount plus Film amount')

        else:
            raise forms.ValidationError('Please provide all amounts')


    # This is needed to display film title on form after failed validation
    # of the form (otherwise only film ID is returned and we need an extra
    # request to get film title)
    def clean_film(self, *args, **kwargs):
        self.fields['film'].widget.attrs.update({
            'data-id':self.cleaned_data['film'].id,
            'data-text': self.cleaned_data['film'].title
        })
        return self.cleaned_data['film']


    def clean_share_venue(self):
        if 'share_venue' in self.cleaned_data and self.cleaned_data['share_venue'] is None:
            return 0
        else:
            return self.cleaned_data['share_venue']

    def clean_profit_share_type(self):
        data = self.cleaned_data['profit_share_type']
        if data == 'SHARE':
            self._clean_when_shared()
        elif data == 'UPFRONT':
            self._clean_when_upfront()
        return data

    def clean_screening_times(self):
        data = self.cleaned_data['screening_times']
        self.cleaned_data['screenings'] = []
        for st in self.cleaned_data['screening_times'].split(';'):
            if st != '':
                try:
                    dt = iso8601_parser.parse(st)
                    now = datetime.datetime.now(timezone(self.cleaned_data['venue'].timezone))
                    if dt < now:
                        raise forms.ValidationError('Invalid screening date or time')
                    self.cleaned_data['screenings'].append(dt)
                except:
                    raise forms.ValidationError('Invalid screening date or time')

        return data


    class Meta:
        model = Screening
        exclude = ('user', 'decision', 'cancelled', 'paid', 'profit', 'film_report', 'report')
        widgets = {
            'description': forms.Textarea(attrs={
                'placeholder':  'Description* - This is just for the film-owner. '\
            }),
        }


class VenueForm(forms.ModelForm):
    country = ModelSelect2Field(
        queryset=Country.objects.all(),
        empty_label='',
        widget=Select2Widget(select2_options={'placeholder': 'COUNTRY *'})
    )
    timezone = Select2ChoiceField(
        required=True,
        choices=[('', '')] + Venue.TIMEZONES,
        widget=Select2Widget(select2_options={'placeholder': 'TIMEZONE *'})
    )

    class Meta:
        model = Venue
        exclude = ('user', 'status', 'deleted', 'is_verified')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'span12', 'placeholder': 'NAME *'}),
            'city': forms.TextInput(attrs={'class': 'span12', 'placeholder': 'CITY'}),
            'county': forms.TextInput(attrs={'class': 'span12', 'placeholder': 'COUNTY/MUNICIPALITY'}),
            'state': forms.TextInput(attrs={'class': 'span12', 'placeholder': 'STATE'}),
            'phone_number': PhoneInput(attrs={'class': 'span12', 'placeholder': 'PHONE NUMBER'}),
            'website': URLInput(attrs={'class': 'span12', 'placeholder': 'OFFICIAL WEBSITE URL'}),
            'description': forms.Textarea(attrs={'class': 'span12', 'placeholder': 'TELL US ABOUT YOUR VENUE. *'})
        }


class VenueFilmForm(forms.Form):
    form_name = 'festivals-form'

    film = forms.ModelChoiceField(
        queryset=Film.active.all(),
        empty_label='',
        widget=forms.TextInput(attrs={'data-placeholder': 'Search by film title'})
    )


class ScreeningReportForm(forms.ModelForm):
    class Meta:
        model = ScreeningReport
        widgets = {
            'description': forms.Textarea(attrs={
                'class':'span4',
                'placeholder':  'DESCRIBE THIS SCREENING - HOW WAS THE FILM? HOW DID THE '\
                                'AUDIENCE FIND THE FILM? ANY NOTABLE POSITIVE OR NEGATIVE REACTIONS? *'
            })
        }

    def clean_total_revenue(self):
        data = self.cleaned_data['total_revenue']
        if self.initial['revenue_required']:
            if not data:
                raise forms.ValidationError('Please enter Total revenue')

        return data


class ScreeningReportFilmOwnerForm(forms.ModelForm):
    class Meta:
        model = ScreeningReportFilmOwner
        widgets = {
            'description': forms.Textarea(attrs={
                'class':'span4',
                'placeholder':  'Describe this screening - how were your interactions with '\
                                'the venue? Did you get paid promptly and fairly? Did '\
                                'it seem like a decent event? *',
            }),
            'paid_promptly': forms.HiddenInput(),
            'recommend_venue': forms.HiddenInput(),
            'report_fraud': forms.HiddenInput()
        }
