from django.contrib import admin
from screening.models import Screening, Venue


class LateWithReportScreenings(admin.SimpleListFilter):
    title = 'Reports'
    parameter_name = 'on_time'

    def lookups(self, request, model_admin):
        return (
            ('ok', 'Both reports submitted'),
            ('fo_late', 'Film owner is late'),
            ('vo_late', 'Venue owner is late'),
        )

    def queryset(self, request, queryset):
        val = self.value()
        if val == 'ok':
            return queryset.filter(report__isnull=False, film_report__isnull=False)

        if val == 'vo_late':
            return Screening.vo_late_with_report()

        if val == 'fo_late':
            return Screening.fo_late_with_report()


def vo_report_list_name(obj):
    return obj.report
vo_report_list_name.short_description = 'Venue report'

def fo_report_list_name(obj):
    return 'Yes' if obj.film_report else 'No'
fo_report_list_name.short_description = 'Film report'

def sm_earnings_list_name(obj):
    return '$' + str(obj.simplemachine_earning) if obj.simplemachine_earning else '-'
sm_earnings_list_name.short_description = 'SM earnings'

def screening_title_list_name(obj):
    return str(obj)
screening_title_list_name.short_description = 'Screening'

def screening_event_datetime(obj):
    return obj.schedule.all()[0].time if obj.schedule else '-'
screening_event_datetime.short_description = 'First event'


class ScreeningAdmin(admin.ModelAdmin):
    change_list_template = "admin/change_list_filter_sidebar.html"
    list_filter = ('paid', LateWithReportScreenings)
    list_display = (screening_title_list_name, 'created_on', 'profit', 'decision', 'cancelled', screening_event_datetime, vo_report_list_name, fo_report_list_name, 'paid', sm_earnings_list_name)

# admin.site.register(Screening)
admin.site.register(Screening, ScreeningAdmin)
admin.site.register(Venue)
