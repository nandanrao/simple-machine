from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required

from .venue_views import *

urlpatterns = patterns(
    '',
    # For browse page
    url(r'^by_letter/letter=(?P<letter>\w)/$', venues_by_letter, name='venues-by-letter'),
    url(r'^by_film/film_id=(?P<film_id>\d+)/$', venues_by_film, name='venues-by-film'),

    url(r'^add/$', add_venue, name='add-venue'),
    url(r'^add/film$', add_venue_films_ajax, name='add-venue-films-ajax'),
    url(r'^delete/$', delete_fov,  name='delete-fov'),
    url(r'^(?P<pk>\d+)/films/', venue_films,  name='venue-films'),
    url(r'^(?P<pk>\d+)/add/films/$', add_venue_films, name='add-venue-films'),
    url(r'^(?P<pk>\d+)/edit/$', add_venue, name='edit-venue'),
    url(r'^(?P<pk>\d+)/delete/$', delete_venue, name='delete-venue'),
    url(r'^(?P<pk>\d+)/', venue_details, name='venue-details'),
)
