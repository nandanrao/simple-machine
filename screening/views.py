import json

from django.core.urlresolvers import reverse
from django.contrib import messages
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django import http
from django.template.loader import render_to_string

from common.temp_file_storage import TempFileStorage, UnsupportedFileType
from .models import Screening, ProfitShare, Venue, ScreeningReportPhoto, ScreeningEvent
from .forms import ScreeningForm, ScreeningReportForm, ScreeningReportFilmOwnerForm
from .presenter import ScreeningPresenter
from film.models import Film
from common.paypal import Paypal
from notifications.models import EmailNotification
from notifications.news_item_generator import NewsItemGenerator
from notifications.conversation_creation import ConversationCreation
from notifications.models import NewsItem, NewsItemType


@login_required
def create_screening(request, pk=None):
    model = None
    if pk:
        model = Screening.objects.get(pk=pk)
        form = ScreeningForm(request.POST or None,
                             instance=model,
                             initial={
                                 'screening_times':';'.join([d.time.isoformat() for d in model.schedule.all()]),
                                 'profit_share_type':model.profit.type,
                                 'share_film':model.profit.film_share,
                                 'share_venue': model.profit.venue_share,
                                 'upfront_film':model.profit.film_upfront,
                                 'upfront_smplmchn':model.profit.smplmchn_upfront,
                                 'upfront_venue': model.profit.venue_upfront
                             })
        form.fields['film'].widget.attrs.update({'data-id': model.film.id, 'data-text':model.film.title, 'value': model.film.id})
    else:
        form = ScreeningForm(request.POST or None, initial={'profit_share_type':'SHARE'})

    # user can only organize screening at his own venue
    users_venues = Venue.by_user(request.user)
    form.fields['venue'].queryset = users_venues

    if 'for_film' in request.GET:
        film = Film.objects.get(pk=request.GET['for_film'])
        form.fields['film'].widget.attrs.update({'data-id': film.id, 'data-text':film.title, 'value': film.id})

    if form.is_valid():
        if model:
            messages.success(request, 'Screening updated')
        else:
            messages.success(request, 'Screening added')
        screening = form.save()

        # Delete events that are deleted on update
        if model:
            for event in model.schedule.all():
                if event.time not in form.cleaned_data['screenings']:
                    ScreeningEvent.objects.filter(pk=event.pk).delete()

        for event in form.cleaned_data['screenings']:
            screening.add_screening_at(event)

        # If payment type is UPFRONT and paid don't allow payment type change
        if not model or (model and not model.paid):
            #TODO: spike, refactor this; untested
            profit = None
            if form.cleaned_data['profit_share_type'] != 'NOPROFIT':
                if model:
                    profit = screening.profit
                    profit.type = form.cleaned_data['profit_share_type']
                else:
                    profit = ProfitShare(type=form.cleaned_data['profit_share_type'])

                if form.cleaned_data['profit_share_type'] == 'SHARE':
                    profit.film_share = form.cleaned_data['share_film']
                    profit.smplmchn_share = 10
                elif form.cleaned_data['profit_share_type'] == 'UPFRONT':
                    profit.film_upfront = form.cleaned_data['upfront_film']
                    profit.smplmchn_upfront = form.cleaned_data['upfront_smplmchn']
                profit.save()
                screening.profit=profit
                screening.save()
            else:
                profit = ProfitShare(
                    type='NOPROFIT'
                )
                profit.save()
                screening.profit=profit
                screening.save()

        # Send notfications
        if screening.venue.user != screening.film.user:
            conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
            if model:
                screening.decision = None
                screening.save()
                for_film_owner, for_venue_owner = NewsItemGenerator.screening_updated(screening, conversation)
                for_film_owner.save_or_bump()
                for_venue_owner.save_or_bump()
                NewsItem.mark_undone(NewsItemType.SCREENING_INVITATION, screening, screening.film.user)
                EmailNotification.send_screening_updated(screening)
            else:
                for_film_owner, for_venue_owner = NewsItemGenerator.screening_invitation(screening, conversation)
                for_film_owner.save()
                for_venue_owner.safe_save()
                EmailNotification.send_screening_invitation(screening)

        return redirect('home')

    user_has_venues = bool(users_venues)
    return render(request, 'screening_form.html', {
        'form': form,
        'sm_percentage': 10,
        'has_venues': user_has_venues,
        'is_edit': True if pk else False,
        'is_paid': True if model and model.paid else False
    })


@login_required
def view_screening(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_public and screening.venue.user != request.user and screening.film.user != request.user:
        return redirect('home')

    context = ScreeningPresenter(screening, request.user).context
    context.update({'screening': screening})
    return render(request, 'screening_details.html', context)


@login_required
def accept_screening(request, pk):
    screening = Screening.objects.get(pk=pk)
    if screening.film.user == request.user:
        if screening.is_accepted:
            messages.success(request, 'This screening has already been already accepted')
            return redirect('screening-details', pk=screening.id)

        if not screening.film.user.get_profile().paypal_email:
            messages.warning(request, 'You must provide a PayPal email before you can accept a screening invitation. You can do this in the Account section')
            return redirect(reverse('change-payment-settings')+"?next="+reverse('accept-screening', args=[pk]))

        screening.approve()
        screening.save()

        # Notification handling
        conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
        # If payment is UPFRONT, send PAY? message to Venue owner
        if screening.profit.type == 'UPFRONT':
            upfront_pay = NewsItemGenerator.screening_pay_upfront(screening, conversation)
            upfront_pay.safe_save()

        for_film_owner, for_venue_owner = NewsItemGenerator.screening_accepted(screening, conversation)
        for_film_owner.safe_save()
        for_venue_owner.safe_save()
        NewsItem.mark_done(NewsItemType.SCREENING_INVITATION, screening, request.user)

        if not screening.first_event_started:
            EmailNotification.send_screening_accepted_notification(screening)

        context = ScreeningPresenter(screening, request.user).context
        context.update({'screening':screening})
    return render(request, 'screening_details.html', context)


@login_required
def decline_screening(request, pk):
    screening = Screening.objects.get(pk=pk)
    if screening.film.user == request.user:
        screening.decline()
        screening.save()

        # Notification handling
        conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
        for_film_owner, for_venue_owner = NewsItemGenerator.screening_declined(screening, conversation)
        for_film_owner.safe_save()
        for_venue_owner.safe_save()
        NewsItem.mark_done(NewsItemType.SCREENING_INVITATION, screening, request.user)

        EmailNotification.send_screening_declined_notification(screening)

        context = ScreeningPresenter(screening, request.user).context
        context.update({'screening':screening})
    return render(request, 'screening_details.html', context)


@login_required
def cancel_screening(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    if screening.venue.user == request.user:
        screening.cancel()
        screening.save()

        # Notification handling
        if screening.film.user != screening.venue.user:
            conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
            for_film_owner, for_venue_owner = NewsItemGenerator.screening_cancelled(screening, conversation)
            for_film_owner.safe_save()
            for_venue_owner.safe_save()
            NewsItem.mark_done(NewsItemType.SCREENING_INVITATION, screening, screening.film.user)

            if not screening.first_event_started:
                EmailNotification.send_screening_canceled_notification(screening)

        context = ScreeningPresenter(screening, request.user).context
        context.update({'screening':screening})
    return render(request, 'screening_details.html', context)


# TODO: Similar to upload stills for Film, see if this could be refactored
@login_required
def upload_screening_report_photo(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    image = request.FILES['file']
    try:
        path = TempFileStorage.save(image)
    except UnsupportedFileType:
        return http.HttpResponse(json.dumps({
            "success": False,
            "reason": "Unsupported file type. Please upload .jpg, .png, .bmp or .gif file"
        }), mimetype='application/json')

    photo = TempFileStorage.get(path)

    p = ScreeningReportPhoto.objects.create(image=photo)
    if not 'screening_report_photos' in request.session:
        request.session['screening_report_photos'] = []

    request.session['screening_report_photos'].append(p)
    request.session.modified = True
    return http.HttpResponse(json.dumps({
        "success": True,
        "url": TempFileStorage.url_for(path),
        "id": p.id
    }), mimetype='application/json')


@login_required
def remove_screening_report_photo(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    if 'id' in request.POST:
        id = int(request.POST['id'])
        p = ScreeningReportPhoto.objects.get(pk=id)
        try:
            request.session['screening_report_photos'].remove(p)
            request.session.modified = True
        except:
            pass
        p.delete()

    return http.HttpResponse('')


@login_required
def create_screening_report(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    if request.method == "GET":
        try:
            del request.session['screening_report_photos']
            request.session.modified = True
        except KeyError:
            pass

    instance = screening.report
    revenue_required = bool(screening.profit.type=='SHARE')
    form = ScreeningReportForm(request.POST or None, instance=instance, initial={'revenue_required':revenue_required})
    if form.is_valid():
        if not screening.requires_payment:
            # Show messages only if there will not be redirect to payment
            if instance:
                messages.success(request, 'Report updated!')
            else:
                messages.success(request, 'Report saved!')

        screening.report = form.save()
        screening.save()

        # Add images
        if 'screening_report_photos' in request.session:
            for p in request.session['screening_report_photos']:
                p.screening_report = screening.report
                p.save()
                # photo = TempFileStorage.get(path)
                # ScreeningReportPhoto.objects.create(image=photo, screening_report=screening.report)

        if screening.requires_payment:
            return redirect('screening-payment', pk=screening.id)
        else:
            # Notification handling
            if screening.venue.user != screening.film.user:
                if not instance:
                    conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
                    for_venue_owner, for_film_owner, for_film_owner_report = NewsItemGenerator.screening_report_venue_sent(screening, conversation)
                    for_venue_owner.save()
                    for_film_owner.save()
                    for_film_owner.safe_save()
                    NewsItem.mark_done(NewsItemType.SCREENING_REPORT_VENUE, screening, screening.venue.user)


        return http.HttpResponse(json.dumps({
            'type': 'url',
            'url': reverse('screening-page', args=(screening.id,))
        }), mimetype='application/json')

    context = ScreeningPresenter(screening, request.user).context
    context.update({'screening':screening})
    context.update({'form': form})
    context.update({'revenue_required': revenue_required})
    photos = []
    if 'screening_report_photos' in request.session:
        photos.extend(request.session['screening_report_photos'])
    if screening.report:
        photos.extend(screening.report.photos.all())
    context.update({'photos': photos})
    return http.HttpResponse(json.dumps({
        'type': 'html',
        'html': render_to_string('screening_report_form.html', context)
    }), mimetype='application/json')


@login_required
def fill_screening_report(request, pk):
    # Film owner fills his report
    screening = Screening.objects.get(pk=pk)
    if screening.film.user != request.user:
        return redirect('home')

    instance = screening.film_report
    form = ScreeningReportFilmOwnerForm(request.POST or None, instance=instance)
    earned = screening.film_owners_earning if screening.requires_payment else None
    film_owners_cut = screening.film_owners_cut
    if form.is_valid():
        if instance:
            messages.success(request, 'Report updated!')
        else:
            messages.success(request, 'Report sent!')
        screening.film_report = form.save()
        screening.save()

        # Notification handling
        if not instance:
            conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
            for_film_owner, for_venue_owner = NewsItemGenerator.screening_report_film_sent(screening, conversation)
            for_film_owner.safe_save()
            for_venue_owner.safe_save()
        NewsItem.mark_done(NewsItemType.SCREENING_REPORT_FILM, screening, screening.film.user)

        return http.HttpResponse(json.dumps({
            'type': 'url',
            'url': reverse('screening-page', args=(screening.id,))
        }), mimetype='application/json')

    return http.HttpResponse(json.dumps({
        'type': 'html',
        'html': render_to_string('screening_report_film_owner_form.html', {
            'form':form,
            'screening':screening,
            'earned': earned,
            'film_owners_cut': film_owners_cut
        })
    }), mimetype='application/json')


@login_required
def screening_payment_form(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    if not screening.requires_payment:
        return redirect('home')

    url = Paypal.prepare_for(screening, settings.BASE_URL)
    url += "&token=" + screening.payment_token

    return http.HttpResponse(json.dumps({
        'type': 'html',
        'html': render_to_string('screening_payment_form.html', {
            "screening": screening,
            "paypal_url": url
        })
    }), mimetype='application/json')


@login_required
def screening_paid(request, pk):
    screening = Screening.objects.get(pk=pk)
    if not screening.is_organized_by(request.user):
        return redirect('home')

    token = request.GET.get('token', None)

    if token and screening.payment_token == token:
        screening.paid = True
        screening.save()

        if screening.report:
            NewsItem.mark_done(NewsItemType.SCREENING_REPORT_VENUE, screening, screening.venue.user)
        messages.success(request, 'Screening paid!')

        # Show paid notification
        conversation = ConversationCreation.get_or_start(screening.film.user, screening.venue.user)
        for_film_owner, for_venue_owner = NewsItemGenerator.screening_paid(screening, conversation)
        for_film_owner.safe_save()
        for_venue_owner.safe_save()
        EmailNotification.send_screening_paid(screening)

        if screening.last_event_passed:
            # This means it wasn't an upfront payment
            for_venue_owner, for_film_owner, for_film_owner_report = NewsItemGenerator.screening_report_venue_sent(screening, conversation)
            for_venue_owner.safe_save()
            for_film_owner.safe_save()
            for_film_owner.safe_save()
            return redirect('screening-page', pk=screening.id)
        else:
            return redirect('screening-details', pk=screening.id)

    return redirect('home')


def screening_page(request, pk):
    """
    Screening page is always visible. This is the page that is shared
    on Facebook, Eventbrite, etc... This page also has screening photos
    and reports after the screening is done.
    """
    screening = get_object_or_404(Screening, pk=pk)

    if screening.cancelled:
        return redirect('home')

    return render(request, 'screening_page.html', {
        'screening': screening,
        'screening_full_url': request.build_absolute_uri(
            reverse('screening-page', args=(screening.id,))
        )
    })


def see_events(request):
    screenings = Screening.all_valid()
    past, future = [], []
    for s in screenings:
        if s.last_event_passed:
            past.append(s)
        elif not s.first_event_started:
            future.append(s)

    return render(request, 'events_list.html', {'past':past, 'future': future})
