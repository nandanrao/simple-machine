class ScreeningOrganizer(object):
    @staticmethod
    def organize(screenings):
        """
        Returns screenings ordered by time to next event.
        Removes screenings whose first event has passed but are not
        accepted.
        """
        valid_screenings = [vs for vs in screenings if not (vs.first_event_started and not vs.is_accepted)]
        return sorted(valid_screenings, key=lambda s: s.time_to_next_event)
