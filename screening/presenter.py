class ScreeningPresenter(dict):
    def __init__(self, screening, user):
        self.screening = screening
        self.user = user
        self.context = {}
        self._fill_context()

    def __getitem__(self, key):
        return self.context[key]

    def _fill_context(self):
        self.context['show_decision_menu'] = False
        self.context['show_admin_menu'] = False
        self.context['profit_share'] = False
        self.context['upfront'] = False
        self.context['noprofit'] = False
        self.context['was_updated'] = False

        if self.screening.venue.user == self.user:
            self.context['show_admin_menu'] = True

        if self.screening.profit.type == 'SHARE':
            self.context['profit_share'] = True

        if self.screening.profit.type == 'UPFRONT':
            self.context['upfront'] = True

        if self.screening.profit.type == 'NOPROFIT':
            self.context['noprofit'] = True

        if self.screening.is_updated and not self.screening.decision:
            self.context['was_updated'] = True

        self.context['show_decision_menu'] = self._show_decision_menu()

    def _show_decision_menu(self):
        if self.screening.venue.user == self.screening.film.user:
            return False

        if self.screening.film.user == self.user and not self.screening.first_event_started:
            return True
