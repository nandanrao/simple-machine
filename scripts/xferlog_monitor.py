# This script should be started before starting FTP server
# It monitors xferlog file and when new line is appended
# it checks if transfer is complete; if it is, it copies
# the file to films folder and calls
# Django management command ftp_upload_done with filename
# and FTP user arguments

# xferlog line format:
# Wed Aug 21 14:26:44 2013 49 213.147.102.38 65245469 /srv/ftp/incoming/33459eab/filename.txt b _ i r 33459eab ftp 0 * c

import os
import subprocess
import sys
import watchdog

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from django.conf import settings
from django.core import management

from film.models import FtpUploadInfo


f = open(settings.XFERLOG_FILE_PATH, 'r')
f.seek(0,2)

class Hand(FileSystemEventHandler):
    def on_modified(self, event):
        if event.event_type == 'modified' and event.src_path == settings.XFERLOG_FILE_PATH:
            global f
            position = f.tell()
            f.close()
            f = open(settings.XFERLOG_FILE_PATH, 'r')
            f.seek(position)
            split_line = f.read().strip().split(' ')
            if split_line[-1] == 'c':
                path = split_line[8]
                ftp_user = split_line[13]

                # Get film ID so we can prepend it to filename
                ftp_upload_info = FtpUploadInfo.objects.get(username=ftp_user)
                film = ftp_upload_info.film
                filename = str(film.id) + "_" + os.path.basename(path)
                try:
                    subprocess.check_call(['mv', path, os.path.join(settings.VIDEO_FILES_DIR, filename)])
                    management.call_command('ftp_upload_done', filename, ftp_user, interactive=False)
                except subprocess.CalledProcessError as e:
                    print "Can't copy video file to video files folder. " + str(e)
                except:
                    print sys.exc_info()


event_handler = Hand()
observer = Observer()
# We pass in dir as path argument instead of a file because wathcdog
# can't watch for file changes, only dirs
observer.schedule(event_handler, path=os.path.dirname(settings.XFERLOG_FILE_PATH))

observer.start()
observer.join()
