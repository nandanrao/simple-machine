"""
    Script that gets festivals data from www.withoutabox.com
    eg. https://www.withoutabox.com/03film/03t_fin/03t_fin_fest_01over.php?festival_id=13278
    and stores them to CSV file

    Need to install pyquery and download html files to local disk (path var)
"""

from pyquery import PyQuery as pq
from lxml import etree
import urllib
from datetime import datetime

from os import listdir
from os.path import isfile, join


# Path where are html files
path = '/Users/imustac/Develop/festivals/files/'

files = [ f for f in listdir(path) if isfile(join(path, f)) ]

f = open("Nandan-Festivals.csv", 'w')


for festival_file in files:
    ff_id = festival_file.split("festival_id")[1][1:]
    filepath = "%s%s" % (path, festival_file)
    d = pq(filename=filepath)
    festival_name = d("form[name='cat'] h2").text()
    festival_dates = d("form[name='cat'] p.fest-dates").text()
    email = d("table.fest-general-table a.mailto").text()
    general = d("table.fest-general-table td.formA div").html().split('<br/>')
    phone = ''
    country = ''
    city = ''

    if len(general) > 3:
        laa = len(general)
        indices = [i for i, s in enumerate(general) if "Phone:" in s]
        # If there is no Phone, try Fax
        if not indices:
            phone = '-'
            indices = [i for i, s in enumerate(general) if "Fax:" in s]
        # If there is no Phone or Fax, try Email
        if not indices:
            phone = '-'
            indices = [i for i, s in enumerate(general) if "@" in s]
        # If there is no Phone or Fax or Email, try Http
        if not indices:
            phone = '-'
            indices = [i for i, s in enumerate(general) if "http" in s]
        index = indices[0] if indices else 0
        phone = general[index] if not phone else '-'
        country = general[index - 1]
        city = general[index - 2]

    if (festival_name):
        date1 = festival_dates.split('Annual')
        # annual_count = int(date1[0]) if date1[0].strip().isdigit() else 0
        if len(date1) > 1:
            date2 = date1[1].split(' to ')
            try:
                date_object1 = datetime.strptime(date2[0].strip(), '%B %d, %Y')
                date_object2 = datetime.strptime(date2[1].strip(), '%B %d, %Y')
                if datetime.date(date_object1) == datetime.date(date_object2):
                    days_count = 1
                else:
                    delta = date_object2 - date_object1
                    days_count = abs(delta.days)+1
            except:
                days_count = '-'
                date_object1 = None
                date_object2 = None

        f.write('%s;"%s";"%s";"%s";"%s";"%s";%s;%s;%s\n' % (
            ff_id,
            festival_name.encode('utf8'),
            email.encode('utf8'),
            country.encode('utf8'),
            city.encode('utf8'),
            phone.encode('utf8'),
            date_object1.isoformat()[:10] if date_object1 else '-',
            date_object2.isoformat()[:10] if date_object2 else '-',
            days_count
        ))

f.close()
