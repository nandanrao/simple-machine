bind = "127.0.0.1:8000"
workers = 9
accesslog = "logs/access.txt"
errorlog = "logs/error.txt"
pidfile = "gunicorn_pid.txt"
loglevel = "info"
daemon = False
