import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django import http
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

from common.browse_views import _get_item_pagination
from common.browse import Browse
from .models import Reviewer

# Reviewer is now called critic

@login_required
def add_critic(request):
    reviewer = Reviewer.create(request.POST['name'], request.user)
    return http.HttpResponse(str(reviewer.id))


@login_required
def list_critics(request):
    q = request.GET['q']
    reviewers = Reviewer.for_user(request.user).filter(name__icontains=q)
    reviewers_list = [{'id': r.id, 'text': r.name} for r in reviewers]
    return http.HttpResponse(json.dumps(reviewers_list), mimetype='application/json')


def critic_details(request, pk):
    reviewer = get_object_or_404(Reviewer, pk=pk)
    slug = slugify(reviewer.name)
    if not slug in request.get_full_path():
        return redirect(reverse('reviewer-details', args=[pk]) + slug)
    return render(request, 'reviewer_details.html', {'reviewer': reviewer})


def critic_films(request, pk):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    reviewer = get_object_or_404(Reviewer, pk=pk)
    films = [review.film for review in reviewer.reviews.order_by('-film__year').all() if review.film.is_public
                and review.film.runtime >= min_value and review.film.runtime <= max_value
                and review.film.year >= min_year and review.film.year <= max_year
                and (country_id == 'all' or review.film.countries.filter(pk=country_id))]
    return render(request, 'partials/_film_details_module.html', {
        'films': _get_item_pagination(films, request),
        'reviewer_id': pk
    })

