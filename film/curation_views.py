import os

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from forms import CurationForm
from models import Curation, Film
from film.profile_image_cropper import ProfileImageCropper
from common.browse_views import _get_item_pagination
from common.browse import Browse


def list_curations(request):
    curations = Curation.objects.filter(is_public=True)
    return render(request, 'curations/list.html', {'curations': curations})


def _needs_cropping(venue, form_image):
    if venue and os.path.basename(venue.profile_image.name) == os.path.basename(form_image):
        return False
    return True


def store_anonymous_curation_title(request):
    if request.POST.get('title') == '':
        request.session['curation_title'] = 'My Great Film Festival'
    else:
        request.session['curation_title'] = request.POST.get('title')
    request.session.modified = True
    return redirect('browse')


def store_film_to_anonymous_curation(request, film_pk):
    request.session['curation_film_id'] = film_pk
    request.session.modified = True
    return redirect("add-curation")


@login_required
def add_curation(request, pk=None, film_pk=None):
    title = None
    if pk:
        curation = Curation.objects.get(pk=pk)
    else:
        curation = None
        if request.session.get('curation_title'):
            title = request.session.get('curation_title')
            del request.session['curation_title']
            request.session.modified = True
        if request.session.get('curation_film_id') and not film_pk:
            film_pk = request.session.get('curation_film_id')
            film_pk = int(film_pk) if film_pk else None
            del request.session['curation_film_id']
            request.session.modified = True

    if curation:
        form = CurationForm(request.POST or None, instance=curation)
    else:
        form = CurationForm(request.POST or None, initial={'title': title})

    if form.is_valid():
        form.instance.user = request.user
        new_curation = form.save()
        if curation:
            messages.success(request, 'List updated!')
        else:
            messages.success(request, 'List created!')
        return redirect('curation-details', new_curation.pk)

    edit = bool(curation)

    # Get titles of tagged films
    tagged_films = None
    if request.method == "POST":
        ids = form.data.getlist('films')
        ids = map(int, ids)
        tagged_films = Film.objects.filter(pk__in=ids)
        tagged_films = [{'id': f.id, 'title': f.title} for f in tagged_films]

    if edit:
        tagged_films = [{'id': f.id, 'title': f.title} for f in curation.films.all()]

    if film_pk:
        film = get_object_or_404(Film, pk=film_pk)
        tagged_films = [{'id': film.id, 'title': film.title}]

    return render(request, 'curations/curation_form.html', {
        'form': form,
        'tagged_films': tagged_films,
        'edit': edit,
        'curation_id': curation.id if curation else None
    })


def curation_details(request, pk):
    curation = get_object_or_404(Curation, pk=pk)
    films = curation.films.all()
    return render(request, 'curations/curation_details.html', {
        'curation': curation,
        'films': _get_item_pagination(films, request)
    })


def curation_films(request, pk):
    curation = get_object_or_404(Curation, pk=pk)
    films = [film for film in curation.films.order_by('-year').all() if film.is_public]
    return render(request, 'partials/_film_details_module.html', {
        'films': _get_item_pagination(films, request)
    })


def add_film_to_curation(request, pk, film_pk):
    curation = get_object_or_404(Curation, pk=pk)
    film = get_object_or_404(Film, pk=film_pk)
    curation.films.add(film)
    return redirect('curation-details', pk=pk)


def delete_curation(request, pk):
    curation = get_object_or_404(Curation, pk=pk)
    if curation.user == request.user:
        curation.deleted = True
        curation.save()
        messages.success(request, 'List deleted!')
    return redirect('home')
