import os
import json
import mimetypes
import requests
import logging
from urlparse import urlparse

from django.conf import settings
from django.shortcuts import redirect, render, get_object_or_404
from django.http import HttpResponse, Http404
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.core.servers.basehttp import FileWrapper
from django.template.defaultfilters import slugify
from django.contrib import messages
from django import http
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse

from common.dropbox_api import get_films_for_user, get_client_from_string
from common.paypal import Paypal
from dropbox.session import OAuthToken
from common.vimeo_api import VimeoAPI
from film.tasks import download_from_dropbox
from notifications.models import EmailNotification, NewsItemType, NewsItem
from notifications.conversation_creation import ConversationCreation
from notifications.news_item_generator import NewsItemGenerator

from .models import (Film, Festival, Reviewer, Organization, Review, Cast, Crew, FilmOnFestival,
                     FilmStill, PreviewRequest, ExhibitionFile, FilmPreview, VideoLinkConverter,
                     Curation, DownloadRequest, FilmPayment)
from .forms import (FestivalForm, BasicInfoStep, MediaStep, FestivalsStep, ReviewsStep, CastStep,
                    ReviewForm, FilmPreviewForm, LogisticsForm)
from .download_policy import DownloadPolicy
from .film_creation import FilmCreation
from .ftp_upload_info import FtpUploadInfoCreator
from .dropbox_downloader import DropboxDownloader, DownloadFailed

from common.image_resizer import ImageResizer
from common.models import Progress, Country
from common.temp_file_storage import TempFileStorage, UnsupportedFileType
from common.tasks import mp_track
from .profile_image_cropper import ProfileImageCropper


log = logging.getLogger('simplemachine')

# View decorator, only film owner can perform wrapper view action
def film_owner(fn):
    def wrapped(request, pk):
        film = get_object_or_404(Film, pk=pk)
        if not film.belongs_to(request.user) or film.deleted:
            return redirect('home')
        return fn(request, pk, film)

    return wrapped


def film_details(request, pk):
    film = get_object_or_404(Film, pk=pk)

    slug = slugify(film.title)
    if not slug in request.get_full_path():
        return redirect(reverse('film-details', args=[pk]) + slug)

    if request.user.is_authenticated():
        prev_req_qs = PreviewRequest.objects.filter(user=request.user, film=film)
        down_req_qs = DownloadRequest.objects.filter(user=request.user, film=film)
        user_curations = Curation.for_user(request.user)

        return render(request, 'film_details.html', {
            'film': film,
            'linked_with_dropbox': request.user.get_profile().is_linked_with_dropbox(),
            'preview_request': prev_req_qs[0] if prev_req_qs.exists() else None,
            'download_request': down_req_qs[0] if down_req_qs.exists() else None,
            'can_download': DownloadPolicy.can_download(request.user, film),
            'curations': user_curations
        })
    else:
        return render(request, 'film_details.html', {
            'film': film,
            'anonymous_curation_title': request.session.get('curation_title')
        })


@login_required
def film_payment(request, pk):
    film = Film.objects.get(pk=pk)
    try:
        amount = float(request.POST.get('amount'))
    except:
        amount = 0
    payment_token = film.payment_token(request.user.pk)
    url = Paypal.prepare_film_payment(film, settings.BASE_URL, payment_token, amount)
    url += "&token=" + payment_token
    return redirect(url)


@login_required
def film_paid(request, pk):
    film = Film.objects.get(pk=pk)
    token = request.GET.get('token', None)
    amount = request.GET.get('amount', None)

    if token and film.payment_token(request.user.id) == token:
        film_payment = FilmPayment.objects.create(film=film, user=request.user, amount=amount)
        film_payment.save()

        # Show paid notification
        messages.success(request, 'Money sent!')
        conversation = ConversationCreation.get_or_start(film.user, request.user)
        for_film_owner, for_payer = NewsItemGenerator.film_paid(film_payment, conversation)
        for_film_owner.safe_save()
        for_payer.safe_save()
        EmailNotification.send_film_paid(film_payment)

    return redirect('film-details', pk=film.id)


def film_details_json(request, pk):
    film = get_object_or_404(Film, pk=pk)
    return HttpResponse(json.dumps({
        'id': film.id,
        'text': film.title
    }), mimetype='application/json')


@login_required
def search_films_json(request):
    q = request.GET.get('q')
    films = Film.active.filter(title__icontains=q)
    json_films = json.dumps([{'id': f.id, 'text': f.title} for f in films])
    return HttpResponse(json_films, mimetype='application/json')


@login_required
@film_owner
def edit_film(request, pk, film):
    current_step = FILM_STEPS['BASIC-INFO']

    image = request.POST['image'] if 'image' in request.POST else film.profile_image.name
    form = BasicInfoStep(request.POST or None,
                         instance=film,
                         initial={'coordinates': '0,0,2000,2000', 'image': image})

    if form.is_valid():
        film = form.save(commit=False)
        film.user = request.user

        if form.cleaned_data['image'] != film.profile_image.name:
            film.profile_image = ProfileImageCropper.crop(
                form.cleaned_data['image'],
                form.cleaned_data['coordinates']
            )
            film.profile_image_small = TempFileStorage.get(
                ImageResizer.resize(film.profile_image.name, 560, 560, suffix='_560px', convert=False)
            )

        film.countries = form.cleaned_data['countries']
        film.save()
        form.save_m2m()
        return redirect('add-film-media', film.id)

    return render(request, 'add_film_wizard/basic_info.html', {
        'form': form,
        'film': film,
        'current_step': current_step,
        'next_step_url': reverse('add-film-media', args=[pk])
    })


@login_required
def add_film(request):
    current_step = FILM_STEPS['BASIC-INFO']
    form = BasicInfoStep(request.POST or None)
    if form.is_valid():
        film_data = form.cleaned_data.copy()
        film_data.update({'user': request.user})
        film_id = FilmCreation.create(film_data)
        # form.save_m2m()
        return redirect('add-film-media', film_id)
    return render(request, 'add_film_wizard/basic_info.html', {
        'form': form, 'first_step': True, 'current_step': current_step
    })


FILM_STEPS = {
    'BASIC-INFO': 'basic-info',
    'MEDIA': 'media',
    'FESTIVALS': 'festivals',
    'REVIEWS': 'reviews',
    'CAST': 'cast',
    'LOGISTICS': 'logistics'
}


@login_required
def add_film_media(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    initial_data = {
        'trailer_url': film.trailer_link
    }
    if FilmPreview.objects.filter(film=film).exists():
        initial_data.update({
          'vimeo_url': film.preview.link,
            'vimeo_password': film.preview.password
        })
    form = MediaStep(request.POST or None, initial=initial_data)
    if form.is_valid():
        if FilmPreview.objects.filter(film=film).exists():
            film_preview = FilmPreview.objects.get(film=film)
            film_preview.link = form.cleaned_data['vimeo_url']
            film_preview.password = form.cleaned_data['vimeo_password']
        else:
            film_preview = FilmPreview(
                film=film,
                link=form.cleaned_data['vimeo_url'],
                password=form.cleaned_data['vimeo_password']
            )

        film_preview.save()
        film.trailer_link = form.cleaned_data['trailer_url']
        film.save()
        return redirect('add-film-festivals', pk)

    return render(request, 'add_film_wizard/media.html', {
        'form': form,
        'film': film,
        'current_step': FILM_STEPS['MEDIA'],
        'next_step_url': reverse('add-film-festivals', args=[pk])
    })


@login_required
def add_film_festivals(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    current_step = FILM_STEPS['FESTIVALS']

    form = FestivalsStep(request.POST or None)
    if form.is_valid():
        fofs = FilmOnFestival.objects.filter(film=film)
        for f in fofs:
            f.make_active()
            f.festival.make_active()
            f.festival.save()
            f.save()
        film.save()
        return redirect('add-film-reviews', pk)

    fofs = film.played_on.all()
    available_years = [{'id': str(y[0]), 'text': str(y[0])} for y in FilmOnFestival.FESTIVAL_YEARS[::-1]]

    return render(request, 'add_film_wizard/festivals.html', {
        'form': form,
        'film': film,
        'available_years': available_years,
        'current_step': current_step,
        'film_on_festivals': fofs,
        'next_step_url': reverse('add-film-reviews', args=[pk])
    })


@login_required
def add_film_reviews(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    current_step = FILM_STEPS['REVIEWS']

    form = ReviewsStep(request.POST or None)
    if form.is_valid():
        for r in film.reviews.all():
            r.active = True
            r.save()
            if r.reviewer:
                r.reviewer.make_active()
                r.reviewer.save()
            if r.organization:
                r.organization.make_active()
                r.organization.save()

        Reviewer.delete_inactive_for_user(request.user)
        Organization.delete_inactive_for_user(request.user)

        film.save()
        return redirect('add-film-cast', pk)

    reviews = film.reviews.all()

    return render(request, 'add_film_wizard/reviews.html', {
        'form': form,
        'review_form': ReviewForm(),
        'film': film,
        'current_step': current_step,
        'reviews': reviews,
        'next_step_url': reverse('add-film-cast', args=[pk])
    })


CREW_ROLES = {
    'sound': 'Sound',
    'music_by': 'Music',
    'cinematography_by': 'Cinematography',
    'produced_by': 'Producer',
    'directed_by': 'Director',
    'editing_by': 'Editing'
}


@login_required
def add_film_cast(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    current_step = FILM_STEPS['CAST']

    form = CastStep(request.POST or None)
    if form.is_valid():
        film.cast.all().update(active=True)
        film.crew.all().update(active=True)
        return redirect('add-film-logistics', pk)

    cast = film.cast.all()
    crew = film.crew.all()

    return render(request, 'add_film_wizard/cast.html', {
        'form': form,
        'film': film,
        'current_step': current_step,
        'cast': cast,
        'crew': crew,
        'next_step_url': reverse('add-film-logistics', args=[pk]),
        'allow_delete': True
    })


@login_required
def add_film_logistics(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    current_step = FILM_STEPS['LOGISTICS']

    initial_data = {}
    edit = False
    if FilmPreview.objects.filter(film=film).exists():
        edit = True
        initial_data.update({
            'vimeo_url': film.preview.link,
            'vimeo_password': film.preview.password,
            'allow_verified': film.preview.allow_verified
        })
    initial_data.update({'paypal_email': request.user.get_profile().paypal_email})
    initial_data.update({'is_public': film.is_public})
    logistics_form = LogisticsForm(request.POST or None, initial=initial_data)

    if logistics_form.is_valid():
        if FilmPreview.objects.filter(film=film).exists():
            film_preview = FilmPreview.objects.get(film=film)
            film_preview.allow_verified = logistics_form.cleaned_data['allow_verified']
        else:
            film_preview = FilmPreview(
                film=film,
                allow_verified=logistics_form.cleaned_data['allow_verified']
            )

        film_preview.save()
        film.is_public=logistics_form.cleaned_data['is_public']
        film.save()

        request.user.get_profile().paypal_email = logistics_form.cleaned_data['paypal_email']
        request.user.get_profile().save()

        if edit:
            messages.success(request, 'Film successfully updated!')
        else:
            messages.success(request, 'Film successfully added!')
        return redirect('film-details', film.id)

    return render(request, 'add_film_wizard/upload.html', {
        'form': logistics_form,
        'film': film,
        'last_step': True,
        'current_step': current_step
    })


@login_required
def upload_still_image(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    image = request.FILES['file']
    try:
        path = TempFileStorage.save(image)
    except UnsupportedFileType:
        return http.HttpResponse(json.dumps({
            "success": False,
            "reason": "Unsupported file type. Please upload .jpg, .png, .bmp or .gif file"
        }), mimetype='application/json')

    full_resolution = TempFileStorage.get(path)

    smaller_version_path = ImageResizer.resize(TempFileStorage.path(path), 600, 600,
                                               prefix='small_')
    smaller_version = TempFileStorage.get(os.path.basename(smaller_version_path))
    medium_version_path = ImageResizer.resize(TempFileStorage.path(path), 1170, 2000,
                                              prefix='mid_')
    medium_version = TempFileStorage.get(os.path.basename(medium_version_path))

    f = FilmStill.objects.create(film=film, image=full_resolution, small=smaller_version,
                                 medium=medium_version)

    return http.HttpResponse(json.dumps({
        "success":True,
        "filename":path,
        "url": f.small.url,
        "id": f.id
    }), mimetype='application/json')


@login_required
def remove_still(request):
    still = int(request.POST['still'])
    still = get_object_or_404(FilmStill, pk=still)

    if not still.film.belongs_to(request.user) or still.film.deleted:
        return redirect('home')

    still.delete()

    return http.HttpResponse('')


@login_required
def send_preview_request(request, pk):
    film = get_object_or_404(Film, pk=pk)

    preview_request = PreviewRequest.objects.create(film=film, user=request.user)
    conversation = ConversationCreation.get_or_start(preview_request.user, preview_request.film.user)
    for_film_owner, for_sender = NewsItemGenerator.preview_request_sent(preview_request,
                                                                        conversation)
    for_film_owner.save()
    for_sender.save()
    if preview_request.film.user.get_profile().preview_request_email:
        EmailNotification.send_preview_request_notification(preview_request)

    mp_track.delay(request.user, 'preview request sent')
    resp = {'result': '<p class="menu-text">Preview request sent!</p>',
            'conversation_url': reverse('view-conversation', args=[conversation.id])}
    return http.HttpResponse(json.dumps(resp), mimetype='application/json')


@login_required
def accept_preview_request(request, film_id, prev_req_id):
    film = get_object_or_404(Film, pk=film_id, user=request.user)
    if not film.belongs_to(request.user):
        return redirect('home')

    preview_request = get_object_or_404(PreviewRequest, pk=prev_req_id)
    preview_request.approve()
    preview_request.save()

    conversation = ConversationCreation.get_or_start(preview_request.user, preview_request.film.user)
    film_owner, request_user = NewsItemGenerator.preview_request_accepted(preview_request, conversation)

    if (preview_request.user.get_profile().is_linked_with_vimeo() and
        preview_request.user.get_profile().vimeo_add_to_watch_later):
        vimeo_id = VideoLinkConverter.parse_vimeo_link_id(preview_request.film.preview.link)
        if vimeo_id:
            VimeoAPI.add_video_to_watch_later(
                preview_request.user.get_profile().vimeo_access_token,
                vimeo_id
            )

    request_user.safe_save()
    if ((film_owner.safe_save() or film_owner.save_or_bump()) and
        preview_request.user.get_profile().preview_request_email):
        EmailNotification.send_preview_request_accepted_notification(preview_request)

    NewsItem.mark_done(NewsItemType.PREVIEW_REQUEST_SENT, preview_request, request.user)

    if 'HTTP_REFERER' in request.META:
        return redirect(request.META['HTTP_REFERER'])
    return redirect('home')


@login_required
def decline_preview_request(request, film_id, prev_req_id):
    film = get_object_or_404(Film, pk=film_id, user=request.user)
    if not film.belongs_to(request.user):
        return redirect('home')

    preview_request = get_object_or_404(PreviewRequest, pk=prev_req_id)
    preview_request.decline()
    preview_request.save()

    conversation = ConversationCreation.get_or_start(preview_request.user, preview_request.film.user)
    film_owner, request_user = NewsItemGenerator.preview_request_declined(preview_request, conversation)

    film_owner.safe_save()
    request_user.safe_save()
    NewsItem.mark_done(NewsItemType.PREVIEW_REQUEST_SENT, preview_request, request.user)
    if preview_request.user.get_profile().preview_request_email:
        EmailNotification.send_preview_request_declined_notification(preview_request)

    if 'HTTP_REFERER' in request.META:
        return redirect(request.META['HTTP_REFERER'])
    return redirect('home')


@login_required
def send_download_request(request, pk):
    film = get_object_or_404(Film, pk=pk)

    download_request = DownloadRequest.objects.create(film=film, user=request.user)
    conversation = ConversationCreation.get_or_start(download_request.user, download_request.film.user)
    for_film_owner, for_sender = NewsItemGenerator.download_request_sent(download_request,
                                                                        conversation)
    for_film_owner.save()
    for_sender.save()
    if download_request.user.get_profile().download_request_email:
        EmailNotification.send_download_request_notification(download_request)

    mp_track.delay(request.user, 'download request sent')
    resp = {'result': '<p class="menu-text">Download request sent!</p>',
            'conversation_url': reverse('view-conversation', args=[conversation.id])}
    return http.HttpResponse(json.dumps(resp), mimetype='application/json')


@login_required
def accept_download_request(request, film_id, down_req_id):
    film = get_object_or_404(Film, pk=film_id, user=request.user)
    if not film.belongs_to(request.user):
        return redirect('home')

    download_request = get_object_or_404(DownloadRequest, pk=down_req_id)
    download_request.approve()
    download_request.save()

    conversation = ConversationCreation.get_or_start(download_request.user, download_request.film.user)
    film_owner, request_user = NewsItemGenerator.download_request_accepted(download_request, conversation)

    if film_owner.save_or_bump():
        EmailNotification.send_download_request_accepted_notification(download_request)

    request_user.safe_save()
    if ((film_owner.save_or_bump() or film_owner.safe_save()) and
        download_request.user.get_profile().download_request_email):
        EmailNotification.send_download_request_accepted_notification(download_request)
    NewsItem.mark_done(NewsItemType.DOWNLOAD_REQUEST_SENT, download_request, request.user)

    if 'HTTP_REFERER' in request.META:
        return redirect(request.META['HTTP_REFERER'])
    return redirect('home')


@login_required
def decline_download_request(request, film_id, down_req_id):
    film = get_object_or_404(Film, pk=film_id, user=request.user)
    if not film.belongs_to(request.user):
        return redirect('home')

    download_request = get_object_or_404(DownloadRequest, pk=down_req_id)
    download_request.decline()
    download_request.save()

    conversation = ConversationCreation.get_or_start(download_request.user, download_request.film.user)
    film_owner, request_user = NewsItemGenerator.download_request_declined(download_request, conversation)

    film_owner.safe_save()
    request_user.safe_save()
    NewsItem.mark_done(NewsItemType.DOWNLOAD_REQUEST_SENT, download_request, request.user)
    if download_request.user.get_profile().download_request_email:
        EmailNotification.send_download_request_declined_notification(download_request)

    if 'HTTP_REFERER' in request.META:
        return redirect(request.META['HTTP_REFERER'])
    return redirect('home')


def add_review(request, film_id):
    if request.method == 'GET':
        return render(request, 'create_review.html', {'form': ReviewForm()})

    pk = request.POST.get('pk', None)
    film = Film.objects.get(pk=film_id)
    r = None
    if pk:
        pk = int(pk)
        r = Review.objects.get(pk=pk)

    form = ReviewForm(request.POST, instance=r)
    if form.is_valid():
        review = form.save()
        review.film = film
        if not pk:
            max_sort_order = max([xreview.sort_order for xreview in film.reviews.all()]) if film.reviews.all() else None
            sort_order = max_sort_order + 1 if max_sort_order else None
            review.sort_order = sort_order
        review.save()
        html = render_to_string('add_film_wizard/partials/_added_reviews.html', {
            'reviews': Review.objects.filter(film=film)
        })
        success = True
    else:
        success = False
        html = render_to_string('create_review.html', {'form': form})

    ret_val = {'success': success, 'html': html}

    reviewer_id = request.POST.get('reviewer', None)
    if reviewer_id:
        reviewer = Reviewer.objects.get(pk=reviewer_id)
        ret_val.update({'reviewer': {'id': reviewer_id, 'text': reviewer.name}})

    organization_id = request.POST.get('organization', None)
    if organization_id:
        org = Organization.objects.get(pk=organization_id)
        ret_val.update({'organization': {'id': organization_id, 'text': org.name}})

    return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')


def delete_review(request):
    id = int(request.POST.get('id'))
    # Delete from database
    if Review.objects.filter(pk=id).exists():
        review = Review.objects.get(pk=id)
        film = review.film
        review.delete()

        html = render_to_string('add_film_wizard/partials/_added_reviews.html', {
            'reviews': Review.objects.filter(film=film)
        })
        success = True
        ret_val = {'success': success, 'html': html}
        return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')

    return http.HttpResponse('')


@login_required
def add_cast(request, film_pk):
    film = Film.objects.get(pk=film_pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    character = request.POST['character']
    actor = request.POST['actor']

    if 'edit_pk' in request.POST and Cast.objects.filter(pk=int(request.POST['edit_pk'])).exists():
        cast = Cast.objects.get(pk=int(request.POST['edit_pk']))
        cast.character = character
        cast.actor = actor
        cast.save()
    else:
        Cast.objects.create(character=character, actor=actor, film=film)

    return render(request, 'add_film_wizard/partials/_added_cast.html', {
        'film': film,
        'cast': film.cast.all(),
        'allow_delete': True
    })


@login_required
def add_crew(request, film_pk):
    film = Film.objects.get(pk=film_pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    role=request.POST['role']
    name=request.POST['name']

    if 'edit_pk' in request.POST and Crew.objects.filter(pk=int(request.POST['edit_pk'])).exists():
        crew = Crew.objects.get(pk=int(request.POST['edit_pk']))
        crew.role = role
        crew.name = name
        crew.save()
    else:
        Crew.objects.create(role=request.POST['role'], name=request.POST['name'], film=film)

    return render(request, 'add_film_wizard/partials/_added_crew.html', {
        'film': film,
        'crew': film.crew.all(),
        'allow_delete': True
    })


@login_required
def delete_cast(request, film_pk, cast_pk):
    film = Film.objects.get(pk=film_pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    cast = Cast.objects.get(pk=cast_pk)
    if not cast.film == film:
        return redirect('home')

    cast.delete()
    return render(request, 'add_film_wizard/partials/_added_cast.html', {
        'film': film,
        'cast': film.cast.all(),
        'allow_delete': True
    })


@login_required
def delete_crew(request, film_pk, crew_pk):
    film = Film.objects.get(pk=film_pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    crew = Crew.objects.get(pk=crew_pk)
    if not crew.film == film:
        return redirect('home')

    crew.delete()
    return render(request, 'add_film_wizard/partials/_added_crew.html', {
        'film': film,
        'crew': film.crew.all(),
        'allow_delete': True
    })


@login_required
def add_film_on_festival(request):
    festival_id = request.POST.get('id-festival[]', None)
    festival_id = int(festival_id) if festival_id else None
    festival = Festival.objects.get(pk=festival_id) if festival_id else None
    section = request.POST.get('section', None)
    awards = request.POST.get('awards', None)
    year = request.POST.get('year', None)
    film_pk = request.POST.get('film_pk', None)
    film = Film.objects.get(pk=film_pk)

    if not festival:
        # User didn't pick festival, ignore
        resp = HttpResponse("Error: festival missing")
        resp.status_code = 400
        return resp

    pk = request.POST.get('pk', None)
    if pk:
        pk = int(pk)
        f = FilmOnFestival.objects.get(pk=pk)
        f.festival = festival
        f.section = section
        f.awards = awards
        f.year = year
    else:
        max_sort_order = max([fof.sort_order for fof in film.played_on.all()]) if film.played_on.all() else None
        sort_order = max_sort_order + 1 if max_sort_order else None
        f = FilmOnFestival(festival=festival, section=section, awards=awards,
                           year=year, film=film, sort_order=sort_order)

    festival.active = True
    festival.save()
    f.save()

    fofs = FilmOnFestival.objects.filter(film=film)
    return render(request, 'add_film_wizard/partials/_added_festivals.html', {
        'film_on_festivals': fofs
    })


@login_required
def upload_exhibition_file(request, pk):
    film = Film.objects.get(pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    video = request.FILES['file']
    file_storage = FileSystemStorage(location=os.path.join(
        settings.MEDIA_ROOT, 'films/exhibition_files/'
    ))
    filename = str(film.id) + '_' + video.name
    path = file_storage.save(filename, video)
    ex_file = ExhibitionFile(file=path, uploaded=True)
    ex_file.via = 'HTTP'
    ex_file.save()
    film.exhibition_file = ex_file
    film.save()
    return http.HttpResponse(json.dumps({"success": True, "filename": path}),
                             mimetype='application/json')


@login_required
def upload_from_dropbox(request, pk):
    film = Film.objects.get(pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    path = request.POST['path']
    key, secret = request.user.get_profile().dropbox_access_token.split(':')
    access_token = OAuthToken(key, secret)
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT,
                                                           'films/exhibition_files/'))
    fname = str(pk) + '_' + path.strip('/')
    outfile = file_storage.path(fname)
    ex_file = ExhibitionFile(file=fname)
    ex_file.save()
    film.exhibition_file = ex_file
    film.save()
    download_from_dropbox.delay(access_token, path, outfile, ex_file.id)
    return http.HttpResponse('downloading')


@login_required
def users_dropbox_files(request):
    if request.user.get_profile().is_linked_with_dropbox():
        key, secret = request.user.get_profile().dropbox_access_token.split(':')
        dropbox_files = get_films_for_user(OAuthToken(key, secret))
        dropbox_files = dropbox_files['contents']
    else:
        dropbox_files = None

    return render(request, 'partials/_exhibition_file_upload_dropbox_files.html', {
        'dropbox_files': dropbox_files
    })


@login_required
def dropbox_upload_progress(request, pk):
    film = Film.objects.get(pk=pk)
    if not film.belongs_to(request.user):
        return redirect('home')

    if not film.exhibition_file:
        return redirect('home')

    upload_in_progress = Progress.objects.filter(model_id=film.exhibition_file.id,
                                                 model="ExhibitionFile").order_by('-id')
    status = 'uploading'
    filename = ''
    exhibition_file = film.exhibition_file
    if exhibition_file.uploaded:
        return http.HttpResponse(json.dumps({'status': 'done',
                                             'filename': exhibition_file.file.name}),
                                 mimetype='application/json')

    if len(upload_in_progress) > 0:
        progress = upload_in_progress[0]
        filename = exhibition_file.file.name
        if exhibition_file.uploaded:
            status = 'done'
        return http.HttpResponse(json.dumps({'uploaded': progress.done,
                                             'total': progress.total,
                                             'percent': progress.percentage,
                                             'status': status,
                                             'time_elapsed': progress.time_elapsed,
                                             'filename': filename}), mimetype='application/json')
    else:
        return http.HttpResponse(json.dumps({'uploaded': 0,
                                             'total': 0,
                                             'percent': 0,
                                             'status': status,
                                             'filename': filename}), mimetype='application/json')


@login_required
def cancel_dropbox_upload(request, pk):
    film = Film.objects.get(pk=pk)
    if not film.belongs_to(request.user):
        return redirect('home')

    ex_file = film.exhibition_file
    if ex_file and not ex_file.uploaded:
        film.exhibition_file = None
        film.save()
        progress_notifications = Progress.objects.filter(model_id=ex_file.id,
                                                         model="ExhibitionFile").order_by('-id')
        progress_notifications.delete()
        ex_file.delete()
        messages.success(request, 'Upload cancelled!')

    return redirect('film-details', pk=pk)


@login_required
def preview_form_handler(request, film_id):
    film = get_object_or_404(Film, pk=film_id)
    if not film.belongs_to(request.user):
        return redirect('home')

    try:
        instance = film.preview
    except FilmPreview.DoesNotExist:
        instance = None

    form = FilmPreviewForm(request.POST or None, instance=instance)

    success = False
    if form.is_valid():
        preview_data = form.save(commit=False)
        preview_data.film = film
        preview_data.save()
        instance = preview_data
        success = True

    response = render(request, 'partials/_film_preview_form.html', {
        'form': form,
        'film': film,
        'preview': instance
    })

    return http.HttpResponse(json.dumps({
        'resp': response.content,
        'success': success,
    }), mimetype='application/json')


@login_required
def unlink_preview_file(request, pk):
    preview = get_object_or_404(FilmPreview, pk=pk)
    film = preview.film
    if not film.belongs_to(request.user):
        return redirect('home')

    preview.delete()

    response = render(request, 'partials/_film_preview_form.html', {
        'form': FilmPreviewForm(),
        'film': film
    })

    return http.HttpResponse(json.dumps({
        'resp': response.content,
        'success': True,
    }), mimetype='application/json')



@login_required
def download_exhibition_file(request, pk):
    film = get_object_or_404(Film, pk=pk)

    if not film.exhibition_file or not film.exhibition_file.uploaded:
        return redirect('home')

    if DownloadPolicy.can_download(request.user, film):
        file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT,
                                                               'films/exhibition_files/'))
        path = file_storage.path(film.exhibition_file.file)
        filename = os.path.basename(path)
        response = HttpResponse(FileWrapper(open(path)),
                                content_type=mimetypes.guess_type(path)[0])
        response['Content-Length'] = os.path.getsize(path)
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response

    return redirect('home')


@login_required
def download_exhibition_file_ftp(request, pk):
    """
    Creates FTP credentials and returns them to user.
    """
    film = get_object_or_404(Film, pk=pk)

    if not film.exhibition_file or not film.exhibition_file.uploaded:
        return redirect('home')

    if DownloadPolicy.can_download(request.user, film):
        credentials = FtpUploadInfoCreator.get_for_film(film)
        credentials.update({'download': True})
        return render(request, 'partials/_ftp_file_upload.html', credentials)

    return render(request, 'partials/_ftp_file_upload.html', {
        'error': "Can't generate upload credentials right now. Please try again later"
    })


@login_required
def delete_film(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if film.user == request.user:
        film.deleted = True
        film.save()
        messages.success(request, 'Film deleted!')
    return redirect('home')


@login_required
def ftp_upload_info(request, pk):
    film = get_object_or_404(Film, pk=pk)
    if not film.belongs_to(request.user) or film.deleted:
        return redirect('home')

    fui = FtpUploadInfoCreator.get_for_film(film)
    if not fui:
        return render(request, 'partials/_ftp_file_upload.html', {
            'error': "Can't generate upload credentials right now. Please try again later"})

    return render(request, 'partials/_ftp_file_upload.html', fui)


@login_required
def download_exhibition_file_dropbox(request, pk):
    film = get_object_or_404(Film, pk=pk)

    if not film.exhibition_file or not film.exhibition_file.uploaded:
        return redirect('home')

    from_user = film.user
    to_user = request.user

    if not to_user.get_profile().dropbox_access_token:
        return HttpResponse(json.dumps({
            'status': 'error',
            'msg': 'Your Dropbox account is not linked. Please link your account first'
        }), mimetype='application/json')

    if DownloadPolicy.can_download(to_user, film):
        db_client_to = get_client_from_string(to_user.get_profile().dropbox_access_token)
        from_user_token = from_user.get_profile().dropbox_access_token
        if from_user_token:
            db_client_from = get_client_from_string(from_user_token)
        else:
            db_client_from = None

        ex_file = film.exhibition_file
        try:
            dd = DropboxDownloader(db_client_to, ex_file, db_client_from)
            res = dd.download()
            if res == 'SCHEDULED':
                msg = 'Your download has started. You will see the file in smplmchn folder in your Dropbox once the transfer is done. Depending on the size of the video file, that might take couple of hours.'
            else:
                msg = 'Your file has been transfered.'
            return HttpResponse(json.dumps({
                'status': 'success',
                'msg': msg
            }), mimetype='application/json')

        except DownloadFailed as e:
            log.error('Transfer of file %s to %s Dropbox account failed' % (ex_file.file.name, to_user.email))
            return HttpResponse(json.dumps({
                'status': 'error',
                'msg': "Unfortunately we can't transfer video file at this moment. Please try again later."
            }), mimetype='application/json')


#CHECK: Not in use - no link to view in templates
def films_by_letter(request, letter):
    films = Film.objects.filter(title__startswith=letter)
    return render(request, 'browse/partials/_film_results.html', {
        'films': films,
        'anonymous_curation_title': request.session.get('curation_title')
    })


def films_by_tag(request, tag=None):
    if tag is None:
        tag = request.GET.get('tag')
    films = Film.active.filter(tags__name__in=[tag])
    return render(request, 'films_by_tag.html', {'films': films, 'tag': tag})


def get_trailer_embed_code(request, film_id):
    film = get_object_or_404(Film, pk=film_id)
    return HttpResponse(film.trailer_embed_code_autoplay)


def get_preview_embed_code(request, film_id):
    film = get_object_or_404(Film, pk=film_id)
    return HttpResponse(film.preview_embed_code_autoplay)
