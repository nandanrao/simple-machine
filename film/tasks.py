from celery import task
from common.dropbox_api import download_file, upload_file
from common.models import Progress
from film.models import ExhibitionFile

import logging

log = logging.getLogger('simplemachine')

def film_upload_progress(written, total, time, ex_file_id):
    if ExhibitionFile.objects.filter(id=ex_file_id).exists():
        Progress.objects.create(done=written, total=total, time_elapsed=time, model="ExhibitionFile", model_id=ex_file_id)
    else:
        raise Exception('Dropbox upload cancelled')


@task()
def download_from_dropbox(access_token, path, outfile, ex_file_id):
    download_file(access_token, path, outfile, ex_file_id, callback=film_upload_progress)
    try:
        ex_file = ExhibitionFile.objects.get(pk=ex_file_id)
        ex_file.uploaded = True
        ex_file.via = 'DROPBOX'
        ex_file.save()
    except ExhibitionFile.DoesNotExist:
        pass


@task()
def transfer_to_dropbox(to_user, full_path, name, size):
    log.info("Starting transfer: %s -> Dropbox via chunked upload" % name)
    upload_file(to_user, full_path, name, size)
    log.info("Transfer of file %s to Dropbox done!" % name)
