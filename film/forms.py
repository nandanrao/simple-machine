from django import forms
import json

from .models import Film, Festival, Review, FilmOnFestival, FilmPreview, Curation
from common.models import Country
from common.html5_fields import URLInput, NumberInput, EmailInput

from django_select2.widgets import Select2MultipleWidget
from django_select2.fields import ModelSelect2MultipleField

from django.forms.widgets import HiddenInput
from django.utils import six
from taggit.utils import edit_string_for_tags

class TagSMWidget(forms.Textarea):
    def render(self, name, value, attrs=None):
        if value is not None and not isinstance(value, six.string_types):
            value = edit_string_for_tags([o.tag for o in value.select_related("tag")])
        return super(TagSMWidget, self).render(name, value, attrs)


class BasicInfoStep(forms.ModelForm):
    "Step 1 in adding new Film wizard"

    def __init__(self, *args, **kwargs):
        super(BasicInfoStep, self).__init__(*args, **kwargs)
        self.fields['tags'].help_text = None
        self.fields.keyOrder = [
            'coordinates',
            'image',
            'title',
            'director',
            'year',
            'runtime',
            'aspect_ratio',
            'language',
            'countries',
            'website',
            'imdb',
            'captioned',
            'synopsis',
            'tags',
            'availability',
            'directors_statement'
        ]

    # form_name = 'basic-info-form'

    countries = ModelSelect2MultipleField(
        queryset=Country.objects.all(),
        empty_label=None,
        widget=Select2MultipleWidget(select2_options={'placeholder': 'COUNTRIES OF PRODUCTION *'})
    )
    coordinates = forms.CharField(widget=HiddenInput(attrs={'id': 'coords'}))
    image = forms.CharField(
        widget=HiddenInput(attrs={'id': 'profile_image_field'}),
        error_messages={'required': 'Profile image is required'},
        required=True
    )
    captioned = forms.BooleanField(initial=False, required=False, widget=HiddenInput(attrs={'id': 'captioned'}))

    def clean_tags(self):
        tags = [tag.strip().replace('\n', ' ').replace('\r', '') for tag in self.cleaned_data['tags']]
        return tags

    class Meta:
        model = Film
        exclude = (
            'user', 'trailer_link', 'profile_image', 'profile_image_small', 'festivals', 'exhibition_file', 'deleted', 'is_public', 'thumbnail_link'
        )
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'TITLE *'}),
            'imdb': URLInput(attrs={'class': 'span12', 'placeholder': 'IMDB URL'}),
            'director': forms.TextInput(attrs={'placeholder': 'DIRECTOR(S) *'}),
            'website': URLInput(attrs={'placeholder': 'OFFICIAL WEBSITE'}),
            # 'distribution_by': forms.TextInput(attrs={'placeholder': 'DISTRIBUTOR (leave blank if none)'}),
            'runtime': forms.TextInput(attrs={'placeholder': 'RUNTIME IN MINUTES *'}),
            'aspect_ratio': forms.TextInput(attrs={'placeholder': 'ASPECT RATIO'}),
            'language': forms.TextInput(attrs={'placeholder': 'LANGUAGE(S)'}),
            'year': forms.TextInput(attrs={'placeholder': 'YEAR (world premiere) *'}),
            'synopsis': forms.Textarea(attrs={'placeholder': 'SHORT SYNOPSIS (one paragraph) *'}),
            'directors_statement': forms.Textarea(attrs={'placeholder': "DIRECTOR'S STATEMENT (IF APPLICABLE)"}),
            'availability': forms.Textarea(attrs={
                'placeholder': 'AVAILABILITY (Describe any considerations or limitations for '\
                'showing your film - is it available in every country? At every sort of event? '\
                'What formats is it available in?'
            }),
            'tags': TagSMWidget(attrs={'placeholder': 'TAGS (Comma-separated list of key words. Use this to associate your film with others that share a similar tone, form, or subject matter. Be creative, go beyond genre, putting "drama" does not help anyone)'}),
        }


class MediaStep(forms.Form):
    "Step 2 in adding new Film wizard"

    # form_name = 'media-form'

    vimeo_url = forms.URLField(
        widget=URLInput(attrs={'placeholder': 'VIMEO URL'}),
        required=False
    )
    vimeo_password = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': 'PASSWORD (IF APPLICABLE)'}),
        required=False
    )

    trailer_url = forms.URLField(
        widget=URLInput(attrs={'placeholder': 'TRAILER URL (VIMEO OR YOUTUBE)'}),
        required=False
    )

    def clean_trailer_url(self):
        url = self.cleaned_data['trailer_url'].strip()
        if url != '':
            if not (url.startswith('http://www.vimeo.com/') or
                    url.startswith('http://www.youtube.com') or
                    url.startswith('http://youtube.com') or
                    url.startswith('http://vimeo.com') or
                    url.startswith('https://youtube.com') or
                    url.startswith('https://www.youtube.com') or
                    url.startswith('http://youtu.be') or
                    url.startswith('https://vimeo.com')):
                raise forms.ValidationError('Please provide Vimeo or Youtube URL')
        return url


class FestivalsStep(forms.Form):
    "Step 3 in adding new Film wizard"

    form_name = 'festivals-form'

    festival_ids = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={
            'id': 'id-festival',
            'placeholder': 'NAME OF SHOWCASE*',
        })
    )
    festivals = forms.ModelMultipleChoiceField(
        widget=forms.HiddenInput,
        required=False,
        queryset=FilmOnFestival.objects
    )

    def clean(self):
        if self.cleaned_data['festival_ids'] != '':
            self.cleaned_data['festivals'] = map(int,
                                                 (self.cleaned_data['festival_ids'].split(',')))
        else:
            self.cleaned_data['festivals'] = []
        return self.cleaned_data


class ReviewsStep(forms.Form):
    "Step 4 in adding new Film wizard"

    form_name = 'reviews-form'


class CastStep(forms.Form):
    "Step 5 in adding new Film wizard"


class UploadStep(forms.Form):
    "Step 6 in adding new Film wizard"


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        exclude = ('film', 'active', 'description', 'sort_order')
        widgets = {
            'url': URLInput(attrs={'placeholder': 'URL OF REVIEW *'}),
            'organization': forms.TextInput(attrs={
                'id': 'organization_id', 'placeholder': 'NAME OF ORGANIZATION OR WEBSITE'
            }),
            'reviewer': forms.TextInput(attrs={
                'id': 'reviewer_id', 'placeholder': 'NAME OF REVIEWER'
            }),
            'full_text': forms.Textarea(attrs={
                'placeholder': 'COPY & PASTE FULL TEXT OF THE REVIEW HERE'
            }),
            'pull_quote': forms.Textarea(attrs={
                'placeholder': 'PICK A PULL-QUOTE TO DISPLAY FROM THIS REVIEW (MUST BE FROM '\
                'THE TEXT ABOVE). COPY & PASTE THAT HERE.'
            })
        }

    def clean_pull_quote(self):
        if 'pull_quote' in self.cleaned_data and 'full_text' in self.cleaned_data:
            pull_quote = self.cleaned_data['pull_quote']
            lower_pull_quote = pull_quote.lower()
            quotes = lower_pull_quote.split('...')
            full_text = self.cleaned_data['full_text']
            full_text = full_text.lower()
            for quote in quotes:
                if quote not in full_text:
                    raise forms.ValidationError('pull quote not contained in full text')
            return pull_quote

    def clean(self, *args, **kwargs):
        cleaned_data = super(ReviewForm, self).clean(*args, **kwargs)
        if (('reviewer' not in cleaned_data or not cleaned_data['reviewer']) and
            ('organization' not in cleaned_data or not cleaned_data['organization'])):
            raise forms.ValidationError('Please select reviewer and/or organization')
        return cleaned_data


class FestivalForm(forms.ModelForm):
    class Meta:
        model = Festival
        exclude = ('user', 'about', 'website', 'sort_order')


class LogisticsForm(forms.Form):
    paypal_email = forms.EmailField(
        widget=EmailInput(attrs={
            'placeholder': 'PAYPAL EMAIL*',
        }),
        required=False
    )
    allow_verified = forms.BooleanField(initial=True, required=False)
    is_public = forms.BooleanField(initial=True, required=False)


class FilmPreviewForm(forms.ModelForm):
    class Meta:
        model = FilmPreview
        exclude = ('film', )
        widgets = {
            'link': URLInput(attrs={
                'placeholder': u"Vimeo URL"
            }),
            'password': forms.TextInput(attrs={
                'placeholder': u"Password (if applicable)"
            }),
            'allow_verified': forms.HiddenInput()
        }


class CurationForm(forms.ModelForm):
    is_public = forms.BooleanField(initial=False, required=False)
    class Meta:
        model = Curation
        exclude = ('user', 'profile_image', )

        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'TITLE *', 'autofocus':'autofocus'}),
            'description': forms.Textarea(attrs={'placeholder': 'DESCRIPTION OF THE LIST'}),
        }
