from collections import OrderedDict

from pyquery import PyQuery as pq


class ImdbParser(object):
    def __init__(self, raw_html):
        self.raw = raw_html
        self.pq = pq(self.raw)

    def parse(self):
        parsed = {}
        parsed['title'] = self.pq.find('h1.header')[0].getchildren()[0].text.strip()
        parsed['plot'] = self._parse_plot()
        parsed['language'] = self._parse_language()
        parsed['year'] = self._parse_year()
        parsed['countries'] = self._parse_countries()
        parsed['directors'] = self._parse_directors()
        parsed['runtime'] = self._parse_runtime()
        return parsed

    def _parse_plot(self):
        storyline_p = self.pq.find('div#titleStoryLine > div[itemprop="description"] > p')[0]
        if storyline_p is not None:
            return storyline_p.text.strip()

        return None

    def _parse_language(self):
        language_h3 = self.pq.find('h4.inline:contains("Language:")')[0]
        languages = []
        for l in pq(language_h3).siblings('a'):
            languages.append(l.text.strip())
        return languages

    def _parse_year(self):
        year = self.pq.find('h1.header')[0].getchildren()[1].getchildren()[0].text
        return int(year) if year else None

    def _parse_countries(self):
        countries_h4 = self.pq.find('h4.inline:contains("Country:")')[0]
        countries = []
        for a in pq(countries_h4).siblings('a'):
            countries.append(pq(a).attr('href').split('/')[2].split('?')[0].upper())
        return countries

    def _parse_directors(self):
        return [d.getchildren()[0].text for d in self.pq.find('div[itemprop="director"]').find('a[itemprop="url"]')]

    def _parse_runtime(self):
        return int(self.pq.find('div.infobar').text().strip().split()[0])


class ImdbCrewAndCastParser(object):
    def __init__(self, raw_html):
        self.raw = raw_html
        self.pq = pq(self.raw)

    def parse(self):
        parsed = {}
        parsed['cast'] = self._parse_cast()
        parsed['crew'] = self._parse_crew()
        return parsed

    def _parse_cast(self):
        cast_table = self.pq.find('table.cast')
        cast_dict = OrderedDict({})
        for row in cast_table.find('tr'):
            if len(pq(row).find('td')) == 1:
                continue
            name = pq(pq(row).find('td.nm')).find('a')[0].text
            char = pq(row).find('td.char')[0].text
            if name and char:
                cast_dict.update({name: char})
        return cast_dict

    def _parse_crew(self):
        # Directors
        directors_a = self.pq.find('a[name="directors"]:contains("Directed by")')
        table = pq(directors_a).parents('table')
        all_director_links = table.find('a').not_('.glossary')
        directors = [a.text for a in all_director_links]

        # Produced by
        producers_a = self.pq.find('a[name="producers"]:contains("Produced by")')
        producers = self._extract_crew_for_a_element(producers_a)

        # Original music by
        music_a = self.pq.find('a[name="music_original"]:contains("Original Music")')
        music = self._extract_crew_for_a_element(music_a)

        # Cinematography by
        cinema_a = self.pq.find('a[name="cinematographers"]:contains("Cinematography")')
        cinema = self._extract_crew_for_a_element(cinema_a)

        # Film Editing by
        editors_a = self.pq.find('a[name="editors"]:contains("Film Editing")')
        editors = self._extract_crew_for_a_element(editors_a)

        # Sound
        sound_a = self.pq.find('a[name="sound_department"]')
        sound = self._extract_crew_for_a_element(sound_a)

        ret_val = OrderedDict()
        ret_val['directed_by'] = directors
        ret_val['produced_by'] =  producers
        ret_val['music_by'] = music
        ret_val['cinematography_by'] = cinema
        ret_val['editing_by'] = editors
        ret_val['sound'] = sound
        return ret_val

    def _extract_crew_for_a_element(self, a_elem):
        table = pq(a_elem).parents('table')
        trs = table.find('tr')[1:-1]
        crew = []
        for tr in trs:
            crew.append(pq(tr).children().eq(0).children().eq(0).text().strip())
        return crew
