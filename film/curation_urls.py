from django.conf.urls.defaults import patterns, url

from .curation_views import *


urlpatterns = patterns(
    '',
    url(r'^add/with_film/(?P<film_pk>\d+)/$', add_curation, name='add-curation-with-film'),
    url(r'^add/$', add_curation, name='add-curation'),
    url(r'^anonymous/add/$', store_anonymous_curation_title, name='store-anonymous-curation-title'),
    url(r'^anonymous/add_film/(?P<film_pk>\d+)/$', store_film_to_anonymous_curation, name='store-film-to-anonymous-curation'),
    url(r'^(?P<pk>\d+)/edit/', add_curation, name='edit-curation'),
    url(r'^(?P<pk>\d+)/delete/$', delete_curation, name='delete-curation'),
    url(r'^(?P<pk>\d+)/film/(?P<film_pk>\d+)/add/$', add_film_to_curation, name='add-film-to-curation'),
    url(r'^(?P<pk>\d+)/films/', curation_films,  name='curation-films'),
    url(r'^(?P<pk>\d+)/', curation_details, name='curation-details'),
    url(r'^$', list_curations, name='list-curations'),
)
