import re
import random
import hashlib

from django.db import models
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.db.models import Q, Count
from django.template.defaultfilters import truncatewords

from taggit.managers import TaggableManager
from taggit.models import Tag
from common.models import Country

from urlparse import urlparse, parse_qs


class ActiveManager(models.Manager):
    use_for_related_fields = True

    def get_query_set(self):
        return super(ActiveManager, self).get_query_set().filter(active=True)


class Festival(models.Model):
    name = models.CharField(max_length=200)
    about = models.TextField(null=True, blank=True)
    website = models.URLField(verbose_name='Official website', null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    logo = models.CharField(max_length=250, null=True, blank=True)
    phone = models.CharField(max_length=250, null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['name']

    objects = models.Manager()
    visible = ActiveManager()

    def __unicode__(self):
        return self.name

    @property
    def description(self):
        return self.about

    def model_class(self):
        return 'Festival'

    @staticmethod
    def for_user(user):
        return Festival.objects.filter(Q(active=True) | (Q(active=False) & Q(user=user)))

    @staticmethod
    def delete_inactive_for_user(user):
        Festival.objects.filter(active=False, user=user).delete()

    def make_active(self):
        self.active = True

    def public_film_count(self):
        return self.films.filter(film__is_public=True).count()

    @staticmethod
    def get_with_film_count(min_length, max_length, min_year, max_year, country_id):
        """
        Returns festival and associated film count.
        """
        if country_id != 'all':
            festivals = Festival.visible.filter(
                films__film__runtime__range=(min_length, max_length),
                films__film__year__range=(min_year, max_year),
                films__film__is_public=True,
                films__film__deleted=False,
                films__film__countries__pk=country_id,
            )
        else:
            festivals = Festival.visible.filter(
                films__film__runtime__range=(min_length, max_length),
                films__film__year__range=(min_year, max_year),
                films__film__is_public=True,
                films__film__deleted=False,
            )

        return festivals.annotate(film_count=Count('films'))

    def get_random_film(self):
        return random.choice(self.films.all()).film if self.films.count() else None


class ExhibitionFile(models.Model):
    file = models.FileField(upload_to='films/exhibition_files')
    uploaded = models.BooleanField(default=False)
    via = models.CharField(max_length=20, null=True, blank=True)


class ActiveFilmsManager(models.Manager):
    def get_query_set(self):
        return super(ActiveFilmsManager, self).get_query_set().filter(deleted=False).filter(is_public=True)


class Film(models.Model):
    imdb = models.URLField(null=True, blank=True)
    title = models.CharField(max_length=200)
    director = models.CharField(max_length=200)
    website = models.URLField(null=True, blank=True)
    runtime = models.PositiveSmallIntegerField(null=True)
    language = models.CharField(max_length=50, null=True, blank=True)
    countries = models.ManyToManyField(Country, related_name='films')
    year = models.PositiveSmallIntegerField(null=True)
    # distribution_by = models.CharField(max_length=200, null=True, blank=True)
    synopsis = models.TextField()
    directors_statement = models.TextField(null=True, blank=True)
    availability = models.TextField(null=True, blank=True)
    profile_image = models.ImageField(upload_to='films/profile_images', null=True, blank=True)
    profile_image_small = models.ImageField(upload_to='films/profile_images', null=True, blank=True)
    trailer_link = models.URLField(null=True, blank=True)
    thumbnail_link = models.URLField(null=True, blank=True)
    deleted = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='films')
    exhibition_file = models.OneToOneField(ExhibitionFile, related_name='film', null=True, blank=True)
    is_public = models.BooleanField(default=True)
    aspect_ratio = models.CharField(max_length=25, null=True, blank=True)
    tags = TaggableManager(blank=True)
    captioned = models.BooleanField(default=False)

    objects = models.Manager()
    active = ActiveFilmsManager()

    def __unicode__(self):
        return self.title

    @property
    def metadesc(self):
        countries = ','.join([c.name for c in self.countries.all()])
        return ','.join([self.title, self.director, str(self.year), countries, self.synopsis])

    @classmethod
    def create(cls, data):
        countries = data.pop('countries')
        tags = data.pop('tags')
        obj = cls(**data)
        obj.save()
        obj.countries = countries
        for tag in tags:
            obj.tags.add(tag)
        return obj.id

    @staticmethod
    def by_user(user):
        return Film.objects.filter(user=user, deleted=False)

    @property
    def trailer_embed_code(self):
        return VideoLinkConverter.get_embed_code(self.trailer_link)

    @property
    def trailer_embed_code_autoplay(self):
        return VideoLinkConverter.get_embed_code(self.trailer_link, True)

    @property
    def preview_embed_code_autoplay(self):
        try:
            preview_embed_code = VideoLinkConverter.get_embed_code(self.preview.link)
        except FilmPreview.DoesNotExist:
            preview_embed_code = ""
        return preview_embed_code

    def belongs_to(self, user):
        return True if self.user == user else False

    @property
    def public_screenings(self):
        return [s for s in self.screenings.all() if s.is_published]

    def set_ftp_upload_info(self, username, password):
        fui = FtpUploadInfo(film=self, username=username, password=password)
        fui.save()

    def get_ftp_upload_info(self):
        try:
            return self.ftp_upload_info
        except FtpUploadInfo.DoesNotExist:
            return None

    def ordered_cast(self):
        return self.cast.all().order_by('id')

    def ordered_crew(self):
        return self.crew.all().order_by('id')

    @property
    def tags_with_film_count(self):
        tags = [tag.name for tag in self.tags.all()]
        return Tag.objects.filter(
                name__in=tags, film__is_public=True
            ).annotate(film_count=Count('film'))

    def payment_token(self, user_id):
        m = hashlib.sha1()
        m.update(str(self.user.pk) + str(self.id) + str(user_id))
        return m.hexdigest()


class FilmPayment(models.Model):
    paid_on = models.DateTimeField(null=True)
    film = models.ForeignKey(Film, related_name='payments')
    amount = models.DecimalField(null=True, blank=False, max_digits=12, decimal_places=2)
    user = models.ForeignKey(User, null=True, blank=True)


class FilmPreview(models.Model):
    film = models.OneToOneField(Film, related_name="preview")
    link = models.URLField()
    password = models.CharField(max_length=50, blank=True)
    allow_verified = models.BooleanField(default=False)


class FilmStill(models.Model):
    image = models.ImageField(upload_to='films/stills')
    film = models.ForeignKey(Film, related_name='stills')
    small = models.ImageField(upload_to='films/stills')
    medium = models.ImageField(upload_to='films/stills')
    show_as_background = models.BooleanField(default=False)

    def still_thumbnail(self):
        return '<img src="%s" />' % self.small.url

    still_thumbnail.short_description = 'Thumbnail'
    still_thumbnail.allow_tags = True

    @staticmethod
    def get_random():
        try:
            return FilmStill.objects.filter(show_as_background=True).order_by('?')[0]
        except IndexError:
            return None


class FilmOnFestival(models.Model):
    FESTIVALS_START_YEAR = 1932
    FESTIVALS_END_YEAR = 2015
    FESTIVAL_YEARS = zip(
        range(FESTIVALS_START_YEAR, FESTIVALS_END_YEAR + 1),
        range(FESTIVALS_START_YEAR, FESTIVALS_END_YEAR + 1)
    )

    section = models.CharField(max_length=200, null=True, blank=True)
    awards = models.CharField(max_length=200, null=True, blank=True)
    festival = models.ForeignKey(Festival, related_name='films')
    year = models.PositiveIntegerField(choices=FESTIVAL_YEARS, null=True, blank=True)
    film = models.ForeignKey(Film, related_name='played_on')
    active = models.BooleanField(default=False)
    sort_order = models.IntegerField(null=True, blank=True)

    objects = models.Manager()
    visible = ActiveManager()

    class Meta:
        ordering = ['sort_order']

    def __unicode__(self):
        return self.film.title + ' on ' + self.festival.name

    def make_active(self):
        self.active = True


class Reviewer(models.Model):
    name = models.CharField(max_length=200)
    active = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='created_reviewers', blank=True, null=True)
    description = models.TextField(null=True, blank=True)

    objects = models.Manager()
    visible = ActiveManager()

    def __unicode__(self):
        return self.name

    def model_class(self):
        return 'Reviewer'

    @classmethod
    def create(cls, name, user):
        obj = cls(name=name, created_by=user)
        obj.save()
        return obj

    def make_active(self):
        self.active = True

    @staticmethod
    def for_user(user):
        return Reviewer.objects.filter(Q(active=True) | (Q(active=False) & Q(created_by=user)))

    @staticmethod
    def delete_inactive_for_user(user):
        Reviewer.objects.filter(active=False, created_by=user).delete()

    @staticmethod
    def get_with_film_count(min_length, max_length, min_year, max_year, country_id):
        if country_id != 'all':
            reviewers = Reviewer.visible.filter(
                    reviews__film__runtime__range=(min_length, max_length),
                    reviews__film__year__range=(min_year, max_year),
                    reviews__film__is_public=True,
                    reviews__film__deleted=False,
                    reviews__film__countries__pk=country_id
                )
        else:
            reviewers = Reviewer.visible.filter(
                    reviews__film__runtime__range=(min_length, max_length),
                    reviews__film__year__range=(min_year, max_year),
                    reviews__film__is_public=True,
                    reviews__film__deleted=False)

        return reviewers.annotate(review_count=Count('reviews')).annotate(film_count=Count('reviews__film', distinct=True))


    def get_random_film(self):
        return random.choice(self.reviews.all()).film if self.reviews.count() else None


class Organization(models.Model):
    name = models.CharField(max_length=200)
    active = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='added_organizations', null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    objects = models.Manager()
    visible = ActiveManager()

    def __unicode__(self):
        return self.name

    def model_class(self):
        return 'Organization'

    @classmethod
    def create(cls, name, user):
        obj = cls(name=name, created_by=user)
        obj.save()
        return obj

    @staticmethod
    def for_user(user):
        return Organization.objects.filter(Q(active=True) | (Q(active=False) & Q(created_by=user)))

    @staticmethod
    def delete_inactive_for_user(user):
        Organization.objects.filter(active=False, created_by=user).delete()

    def make_active(self):
        self.active = True

    # @staticmethod
    # def get_with_film_count():
    #     return Organization.objects.annotate(review_count=Count('reviews')).annotate(film_count=Count('reviews__film', distinct=True))

    @staticmethod
    def get_with_film_count(min_length, max_length, min_year, max_year, country_id):
        if country_id != 'all':
            organizations = Organization.visible.filter(
                    reviews__film__runtime__range=(min_length, max_length),
                    reviews__film__year__range=(min_year, max_year),
                    reviews__film__is_public=True,
                    reviews__film__deleted=False,
                    reviews__film__countries__pk=country_id
                )
        else:
            organizations = Organization.visible.filter(
                    reviews__film__runtime__range=(min_length, max_length),
                    reviews__film__year__range=(min_year, max_year),
                    reviews__film__is_public=True,
                    reviews__film__deleted=False)

        return organizations.annotate(review_count=Count('reviews')).annotate(film_count=Count('reviews__film', distinct=True))

    def get_random_film(self):
        return random.choice(self.reviews.all()).film if self.reviews.count() else None


class Review(models.Model):
    url = models.URLField()
    reviewer = models.ForeignKey(Reviewer, related_name='reviews', blank=True, null=True)
    organization = models.ForeignKey(Organization, related_name='reviews', blank=True, null=True)
    film = models.ForeignKey(Film, related_name='reviews', blank=True, null=True)
    full_text = models.TextField()
    pull_quote = models.TextField(blank=True, null=True)
    active = models.BooleanField(default=False)
    sort_order = models.IntegerField(null=True, blank=True)

    objects = models.Manager()
    visible = ActiveManager()

    class Meta:
        ordering = ['sort_order']

    def __unicode__(self):
        return self.film.title + ' review by ' + self.reviewer_pretty_name()

    def model_class(self):
        return 'Review'

    @classmethod
    def create(cls, url, reviewer=None, organization=None):
        obj = cls(url=url)
        obj.reviewer = reviewer
        obj.organization = organization
        obj.save()
        return obj

    def reviewer_pretty_name(self):
        if self.organization and self.reviewer:
            return self.organization.name + " (" + self.reviewer.name + ")"
        elif self.organization:
            return self.organization.name
        elif self.reviewer:
            return self.reviewer.name
        else:
            return "<unknown>"

    def other_reviews_by_reviewer(self):
        if self.organization and self.reviewer:
            reviews = self.reviewer.reviews.all()
            return set([r.film for r in reviews])
        elif self.organization:
            reviews = self.organization.reviews.all()
            return set([r.film for r in reviews])
        elif self.reviewer:
            reviews = self.reviewer.reviews.all()
            return set([r.film for r in reviews])
        else:
            return []

    @property
    def get_pull_quote(self):
        return truncatewords(self.full_text, 90) if not self.pull_quote else self.pull_quote

    @staticmethod
    def get_random(number):
        return Review.visible.order_by('?')[:number]


class Cast(models.Model):
    character = models.CharField(max_length=200)
    actor = models.CharField(max_length=200)
    film = models.ForeignKey(Film, related_name='cast')
    active = models.BooleanField(default=False)

    objects = models.Manager()
    visible = ActiveManager()

    @staticmethod
    def delete_inactive_for_user(user):
        Cast.objects.filter(film__user=user, active=False).delete()


class Crew(models.Model):
    role = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    film = models.ForeignKey(Film, related_name='crew')
    active = models.BooleanField(default=False)

    objects = models.Manager()
    visible = ActiveManager()

    @staticmethod
    def delete_inactive_for_user(user):
        Crew.objects.filter(film__user=user, active=False).delete()


class PreviewRequest(models.Model):

    class Meta:
        unique_together = ('user', 'film')

    DECISION_APPROVED = 1
    DECISION_DECLINED = 2

    DECISIONS = (
        (DECISION_APPROVED, 'Approved'),
        (DECISION_DECLINED, 'Declined')
    )

    film = models.ForeignKey(Film, editable=False)
    user = models.ForeignKey(User, editable=False)
    decision = models.IntegerField(choices=DECISIONS, null=True, blank=True,
        editable=False)
    cancelled = models.BooleanField(default=False, editable=False)
    requester_info = models.CharField(max_length=255, null=True, blank=True)
    request_message = models.TextField(blank=True, null=True)

    def approve(self):
        self.decision = self.DECISION_APPROVED

    def decline(self):
        self.decision = self.DECISION_DECLINED

    @property
    def decision_made(self):
        return True if self.decision else False

    @property
    def is_accepted(self):
        return self.decision == self.DECISION_APPROVED

    @property
    def is_declined(self):
        return self.decision == self.DECISION_DECLINED

    def cancel(self):
        self.cancelled = True


class DownloadRequest(models.Model):

    class Meta:
        unique_together = ('user', 'film')

    DECISION_APPROVED = 1
    DECISION_DECLINED = 2

    DECISIONS = (
        (DECISION_APPROVED, 'Approved'),
        (DECISION_DECLINED, 'Declined')
    )

    film = models.ForeignKey(Film, editable=False)
    user = models.ForeignKey(User, editable=False)
    decision = models.IntegerField(choices=DECISIONS, null=True, blank=True,
        editable=False)
    cancelled = models.BooleanField(default=False, editable=False)
    requester_info = models.CharField(max_length=255, null=True, blank=True)
    request_message = models.TextField(blank=True, null=True)

    def approve(self):
        self.decision = self.DECISION_APPROVED

    def decline(self):
        self.decision = self.DECISION_DECLINED

    @property
    def decision_made(self):
        return True if self.decision else False

    @property
    def is_accepted(self):
        return self.decision == self.DECISION_APPROVED

    @property
    def is_declined(self):
        return self.decision == self.DECISION_DECLINED

    def cancel(self):
        self.cancelled = True

    @staticmethod
    def get_or_none(**kwargs):
        try:
            return DownloadRequest.objects.get(**kwargs)
        except DownloadRequest.DoesNotExist:
            return None


class VideoLinkConverter(object):

    @staticmethod
    def parse_youtube_link_id(link):
        if 'youtube.com' in link:
            try:
                return parse_qs(urlparse(link).query)['v'][0]
            except KeyError:
                return None
        elif 'youtu.be' in link:
            return link.split('/')[-1]
        else:
            return None

    @staticmethod
    def parse_vimeo_link_id(link):
        m = re.match('.*vimeo.com/(?P<id>\d+).*', link)
        if m:
            return m.group('id')

    @classmethod
    def get_embed_code(cls, link, autoplay=False):
        if link:
            if 'youtube.com' in link or 'youtu.be' in link:
                id = cls.parse_youtube_link_id(link)
                if id:
                    return render_to_string(
                        'partials/_youtube_embed_code.html', {
                            'id': id,
                            'autoplay': autoplay
                        })

            elif 'vimeo.com' in link:
                id = cls.parse_vimeo_link_id(link)
                if id:
                    return render_to_string(
                        'partials/_vimeo_embed_code.html', {
                            'id': id,
                            'autoplay': autoplay
                        })

        return ""

    @classmethod
    def get_trailer_data(cls, link):
        if link:
            if 'youtube.com' in link or 'youtu.be' in link:
                id = cls.parse_youtube_link_id(link)
                if id:
                    return id, 'youtube'

            elif 'vimeo.com' in link:
                id = cls.parse_vimeo_link_id(link)
                if id:
                    return id, 'vimeo'

        return None, None


class ActiveCurationsManager(models.Manager):
    def get_query_set(self):
        return super(ActiveCurationsManager, self).get_query_set().filter(deleted=False)


class Curation(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    user = models.ForeignKey(User, null=True, blank=True, related_name="curations")
    films = models.ManyToManyField(Film)
    is_public = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    objects = models.Manager()
    active = ActiveCurationsManager()

    @property
    def name(self):
        return self.title

    def model_class(self):
        return 'Curation'

    @staticmethod
    def for_user(user):
        return Curation.active.filter(user=user)

    # @staticmethod
    # def get_with_film_count():
    #     return Curation.active.filter(is_public=True).annotate(film_count=Count('films'))

    @staticmethod
    def get_with_film_count(min_length, max_length, min_year, max_year, country_id):
        """
        Returns curations and associated film count.
        """
        if country_id != 'all':
            curations = Curation.active.filter(
                is_public=True,
                films__runtime__range=(min_length, max_length),
                films__year__range=(min_year, max_year),
                films__is_public=True,
                films__deleted=False,
                films__countries__pk=country_id
            )
        else:
            curations = Curation.active.filter(
                is_public=True,
                films__runtime__range=(min_length, max_length),
                films__year__range=(min_year, max_year),
                films__is_public=True,
                films__deleted=False
            )

        return curations.annotate(film_count=Count('films'))

    def get_random_film(self):
        return random.choice(self.films.all()) if self.films.count() else None


class FtpUploadInfo(models.Model):
    film = models.OneToOneField(Film, related_name="ftp_upload_info")
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
