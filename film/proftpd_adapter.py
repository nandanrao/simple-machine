import sqlite3
import pwd
from uuid import uuid4
import stat
import os


class ProftpdAdapter(object):
    """
    Dynamic ProFTPD configuration using SQLite.
    """

    sqlite_schema_users = """
        CREATE TABLE users (
            ref INTEGER UNIQUE,
            userid VARCHAR(255) UNIQUE,
            passwd VARCHAR(255),
            uid INTEGER,
            gid INTEGER,
            homedir INTEGER UNIQUE,
            shell VARCHAR(255) DEFAULT ''
        );
    """
    sqlite_schema_groups = """
        CREATE TABLE groups (
            groupname VARCHAR(255) UNIQUE,
            gid INTEGER,
            members VARCHAR(255) DEFAULT ''
        );
    """

    def __init__(self, db_path):
        """
        Create a ProftpdAdapter object using the database at the specified path.
        If the database doesn't exist yet, it will be created and initialised.

        If the database path is not specified, the default one is used.
        """
        if db_path is None:
            db_path = config.PROFTPD_AUTH_FILE

        self.path = db_path
        self.conn = None
        self._reopen()
        self._ensure_schema()
        print self.path

    def close(self):
        if self.conn:
            self.conn.close()
            self.conn = None

    def _reopen(self):
        self.close()
        self.conn = sqlite3.connect(self.path)

    def _select_single(self, query, *args):
        c = self.conn.cursor()
        rows = list(c.execute(query, args))
        if len(rows) != 1:
            raise ValueError("Query '%s' returned %d rows" %
                (query, len(rows)))
        return rows[0]

    def _select_single_field(self, query, *args):
        row = self._select_single(query, *args)
        if len(row) != 1:
            raise ValueError("Row has more than one field: %s", repr(row))
        return row[0]

    def _ensure_schema(self):
        exists = self._select_single_field("SELECT COUNT(*) FROM " +
            "sqlite_master WHERE type='table' AND name='users'")
        if not exists:
            c = self.conn.cursor()
            c.execute(self.sqlite_schema_users)
            c.execute(self.sqlite_schema_groups)
            c.close()
            self._reopen()

    @staticmethod
    def _get_ftp_uid_gid_home():
        try:
            record = pwd.getpwnam('ftp')
        except KeyError:
            record = pwd.getpwnam('nobody')
        return record.pw_uid, record.pw_gid, record.pw_dir

    @staticmethod
    def _recursive_delete(path):
        for dpath, dirs, files in os.walk(path, topdown=False):
            for fname in files:
                os.unlink(os.path.join(dpath, fname))
            os.rmdir(dpath)

    @staticmethod
    def get_incoming_directory():
        """
        Returns the configured FTP incoming directory.
        """
        uid, gid, home = ProftpdAdapter._get_ftp_uid_gid_home()
        return os.path.join(home, 'incoming')

    def create_user(self, reference, username, password):
        """
        Create a new user with the specified username and password. The
        username and reference must be unique, otherwise, ValueError
        will be raised. If the database is locked for a longer period of time,
        RuntimeError will be raised.

        On success, user is created in the database and their home directory
        is created under <ftp_home>/incoming/ directory.

        Returns: (reference, username, password, homedir) tuple for the
        newly-created user
        """

        uid, gid, home = self._get_ftp_uid_gid_home()
        homedir = os.path.join(self.get_incoming_directory(), username)
        c = self.conn.cursor()
        uid = 1001 # Use smplmchn user
        try:
            c.execute('INSERT INTO users VALUES (?, ?, ?, ?, ?, ?, ?)', (
                reference, username, password, uid, gid, homedir, '/bin/false'
            ))
            os.makedirs(homedir)
            os.chmod(homedir, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
            self.conn.commit()
        except sqlite3.IntegrityError as e:
            self.conn.rollback()
            raise ValueError(e)
        except sqlite3.OperationalError as e:
            self.conn.rollback()
            raise RuntimeError(e)
        finally:
            c.close()

        return reference, username, password, homedir

    def delete_user(self, reference):
        """
        Delete the user from the database, and delete their home directory
        (along with everything still in it). Raises ValueError if the
        user corresponding to the passed-in reference doesn't exist.
        """

        try:
            username = self._select_single_field(
                'SELECT userid FROM users WHERE ref = ?', reference)
        except ValueError:
            raise ValueError("no user with reference '%s'" % repr(reference))

        homedir = os.path.join(self.get_incoming_directory(), username)
        c = self.conn.cursor()
        try:
            c.execute('DELETE FROM users WHERE userid = ?', (username,))
            self.conn.commit()
            self._recursive_delete(homedir)
        except Exception:
            self.conn.rollback()
            raise
        finally:
            c.close()

    def create_random_user(self, reference):
        """
        Create a user with random username and password.
        Returns (reference, username, password, homedir) tuple for
        the newly created user. If user can't be created (eg. reference
        isn't unique), ValueError will be raised.
        """
        for i in xrange(100):
            random_uid = uuid4().hex
            user = random_uid[:8]
            pwd = random_uid[8:16]
            try:
                reference, user, pwd, home = self.create_user(reference,
                    user, pwd)
            except (ValueError, RuntimeError):
                raise
            return reference, user, pwd, home
        raise ValueError('unable to create new user')

    def get_file_reference(self, fname):
        """
        Return the reference of the user owning the file (== whose homedir this
        file is in), or None if there's no owner of this file.
        """
        incoming = self.get_incoming_directory()

        if not fname.startswith(incoming):
            return None

        username = os.path.basename(os.path.dirname(fname))
        try:
            ref = self._select_single_field(
                'SELECT ref FROM users WHERE userid = ?', username)
            return ref
        except ValueError:
            return None
