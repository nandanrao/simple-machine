# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Festival'
        db.create_table('film_festival', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('about', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('logo', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=250, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
        ))
        db.send_create_signal('film', ['Festival'])

        # Adding model 'ExhibitionFile'
        db.create_table('film_exhibitionfile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('uploaded', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('film', ['ExhibitionFile'])

        # Adding model 'Film'
        db.create_table('film_film', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('imdb', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('director', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('runtime', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=50, null=True, blank=True)),
            ('year', self.gf('django.db.models.fields.PositiveSmallIntegerField')(null=True, blank=True)),
            ('synopsis', self.gf('django.db.models.fields.TextField')()),
            ('profile_image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('trailer_link', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='films', to=orm['auth.User'])),
            ('exhibition_file', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='film', unique=True, null=True, to=orm['film.ExhibitionFile'])),
        ))
        db.send_create_signal('film', ['Film'])

        # Adding M2M table for field countries on 'Film'
        db.create_table('film_film_countries', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('film', models.ForeignKey(orm['film.film'], null=False)),
            ('country', models.ForeignKey(orm['common.country'], null=False))
        ))
        db.create_unique('film_film_countries', ['film_id', 'country_id'])

        # Adding model 'FilmPreview'
        db.create_table('film_filmpreview', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('film', self.gf('django.db.models.fields.related.OneToOneField')(related_name='preview', unique=True, to=orm['film.Film'])),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal('film', ['FilmPreview'])

        # Adding model 'FilmStill'
        db.create_table('film_filmstill', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='stills', to=orm['film.Film'])),
        ))
        db.send_create_signal('film', ['FilmStill'])

        # Adding model 'FilmOnFestival'
        db.create_table('film_filmonfestival', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('section', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('awards', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('festival', self.gf('django.db.models.fields.related.ForeignKey')(related_name='films', to=orm['film.Festival'])),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='played_on', to=orm['film.Film'])),
        ))
        db.send_create_signal('film', ['FilmOnFestival'])

        # Adding model 'Reviewer'
        db.create_table('film_reviewer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('film', ['Reviewer'])

        # Adding model 'Organization'
        db.create_table('film_organization', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('film', ['Organization'])

        # Adding model 'Review'
        db.create_table('film_review', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('reviewer', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='reviews', null=True, to=orm['film.Reviewer'])),
            ('organization', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='reviews', null=True, to=orm['film.Organization'])),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='reviews', to=orm['film.Film'])),
            ('full_text', self.gf('django.db.models.fields.TextField')()),
            ('pull_quote', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('film', ['Review'])

        # Adding model 'Cast'
        db.create_table('film_cast', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('character', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('actor', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='cast', to=orm['film.Film'])),
        ))
        db.send_create_signal('film', ['Cast'])

        # Adding model 'Crew'
        db.create_table('film_crew', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(related_name='crew', to=orm['film.Film'])),
        ))
        db.send_create_signal('film', ['Crew'])

        # Adding model 'PreviewRequest'
        db.create_table('film_previewrequest', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('film', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['film.Film'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('decision', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('requester_info', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('request_message', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('film', ['PreviewRequest'])

        # Adding unique constraint on 'PreviewRequest', fields ['user', 'film']
        db.create_unique('film_previewrequest', ['user_id', 'film_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'PreviewRequest', fields ['user', 'film']
        db.delete_unique('film_previewrequest', ['user_id', 'film_id'])

        # Deleting model 'Festival'
        db.delete_table('film_festival')

        # Deleting model 'ExhibitionFile'
        db.delete_table('film_exhibitionfile')

        # Deleting model 'Film'
        db.delete_table('film_film')

        # Removing M2M table for field countries on 'Film'
        db.delete_table('film_film_countries')

        # Deleting model 'FilmPreview'
        db.delete_table('film_filmpreview')

        # Deleting model 'FilmStill'
        db.delete_table('film_filmstill')

        # Deleting model 'FilmOnFestival'
        db.delete_table('film_filmonfestival')

        # Deleting model 'Reviewer'
        db.delete_table('film_reviewer')

        # Deleting model 'Organization'
        db.delete_table('film_organization')

        # Deleting model 'Review'
        db.delete_table('film_review')

        # Deleting model 'Cast'
        db.delete_table('film_cast')

        # Deleting model 'Crew'
        db.delete_table('film_crew')

        # Deleting model 'PreviewRequest'
        db.delete_table('film_previewrequest')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'common.country': {
            'Meta': {'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'film.cast': {
            'Meta': {'object_name': 'Cast'},
            'actor': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'character': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'cast'", 'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'film.crew': {
            'Meta': {'object_name': 'Crew'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'crew'", 'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'film.exhibitionfile': {
            'Meta': {'object_name': 'ExhibitionFile'},
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'uploaded': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'film.festival': {
            'Meta': {'object_name': 'Festival'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'film.film': {
            'Meta': {'object_name': 'Film'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'films'", 'symmetrical': 'False', 'to': "orm['common.Country']"}),
            'director': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'exhibition_file': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'film'", 'unique': 'True', 'null': 'True', 'to': "orm['film.ExhibitionFile']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'imdb': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'profile_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'runtime': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'synopsis': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'trailer_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'films'", 'to': "orm['auth.User']"}),
            'year': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'film.filmonfestival': {
            'Meta': {'object_name': 'FilmOnFestival'},
            'awards': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'festival': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'films'", 'to': "orm['film.Festival']"}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'played_on'", 'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'film.filmpreview': {
            'Meta': {'object_name': 'FilmPreview'},
            'film': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'preview'", 'unique': 'True', 'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'})
        },
        'film.filmstill': {
            'Meta': {'object_name': 'FilmStill'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'stills'", 'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'film.organization': {
            'Meta': {'object_name': 'Organization'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'film.previewrequest': {
            'Meta': {'unique_together': "(('user', 'film'),)", 'object_name': 'PreviewRequest'},
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'decision': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'film': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['film.Film']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'request_message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'requester_info': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'film.review': {
            'Meta': {'object_name': 'Review'},
            'film': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reviews'", 'to': "orm['film.Film']"}),
            'full_text': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'organization': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'reviews'", 'null': 'True', 'to': "orm['film.Organization']"}),
            'pull_quote': ('django.db.models.fields.TextField', [], {}),
            'reviewer': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'reviews'", 'null': 'True', 'to': "orm['film.Reviewer']"}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        'film.reviewer': {
            'Meta': {'object_name': 'Reviewer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['film']