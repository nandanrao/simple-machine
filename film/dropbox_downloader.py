import os
import logging

from dropbox.client import DropboxClient
from common.dropbox_api import link_file_on_dropbox, file_exists
from film.tasks import transfer_to_dropbox
from django.conf import settings


log = logging.getLogger('simplemachine')


class DownloadFailed(Exception):
    pass


class DropboxDownloader(object):
    """
    Handles download (from users' perspective) to Dropbox
    """

    def __init__(self, to_user, file, from_user=None):
        """
        from_user can be None if file was not uploaded to SM via Dropbox
        """
        self.to_user = to_user      # dropbox.client.DrobopxClient - the receiver
        self.from_user = from_user  # dropbox.client.DropboxClient - the sender
        self.file = file
        self.filename = self._parse_filename(self.file)
        self.full_path = self._parse_full_path(self.file)

    def download(self):
        if self._requires_transfer():
            self._start_transfer()
            return 'SCHEDULED'
        else:
            self._link_on_dropbox()
            return 'DONE'

    def _parse_filename(self, file):
        return self.file.file.name

    def _parse_full_path(self, file):
        """
        Returns full path to video file on disk. Needed because self.file.path
        returns only settings.MEDIA_ROOT + filename, and it should return
        settings.MEDIA_ROOT + upload_to (from model) + filename
        """
        return os.path.join(settings.MEDIA_ROOT, file.file.field.upload_to, self.filename)


    def _requires_transfer(self):
        if self.file.via == 'FTP' or self.file.via == 'HTTP':
            return True

        if not self._file_exists_on_dropbox(self.filename):
            return True

        return False

    def _start_transfer(self):
        """
        This method start actual file transfer to Dropbox. Since file transfer
        can take some time, it is started as a background task (Celery)
        """
        try:
            file_size = os.stat(self.full_path).st_size
        except OSError as e:
            log.error("Can't determine file size for path: %s. Error: %s" % (
                self.full_path, str(e)
            ))
            raise DownloadFailed()

        transfer_to_dropbox.delay(self.to_user, self.full_path, self.filename, file_size)

    def _link_on_dropbox(self):
        try:
            link_file_on_dropbox(self.from_user, self.to_user, self.filename)
        except:
            raise DownloadFailed()

    def _file_exists_on_dropbox(self, filename):
        return file_exists(self.from_user, filename) if self.from_user else False
