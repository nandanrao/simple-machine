from django.contrib import admin
from film.models import Festival, Film, FilmStill, FilmOnFestival, Review, Reviewer, Organization, FilmPreview, PreviewRequest

admin.site.register(Film)
admin.site.register(FilmOnFestival)
admin.site.register(Organization)

class ReviewInline(admin.StackedInline):
    model = Review

class FilmOnFestivalInline(admin.StackedInline):
    model = FilmOnFestival

class ReviewerAdmin(admin.ModelAdmin):
    inlines = [ReviewInline,]
    search_fields = ['name',]

class FestivalAdmin(admin.ModelAdmin):
    inlines = [FilmOnFestivalInline,]
    ordering = ['name']
    search_fields = ['name',]

class FilmStillAdmin(admin.ModelAdmin):
    list_display = ('still_thumbnail', 'film', 'show_as_background')

class FilmPreviewAdmin(admin.ModelAdmin):
    list_display = ('film', 'link', 'password')

class PreviewRequestAdmin(admin.ModelAdmin):
    actions = None
    list_display = ('film', 'user', 'decision')
    search_fields = ['film__title', 'user__email']
    fieldsets = [
        (None, {'fields':()}),
        ]

    def __init__(self, *args, **kwargs):
        super(PreviewRequestAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

admin.site.register(FilmStill, FilmStillAdmin)
admin.site.register(Reviewer, ReviewerAdmin)
admin.site.register(Festival, FestivalAdmin)
admin.site.register(FilmPreview, FilmPreviewAdmin)
admin.site.register(Review)
admin.site.register(PreviewRequest, PreviewRequestAdmin)
