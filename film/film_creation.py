from .models import Film
from profile_image_cropper import ProfileImageCropper
from common.image_resizer import ImageResizer
from common.temp_file_storage import TempFileStorage


class FilmCreation(object):
    @staticmethod
    def create(film_data):
        film_data['profile_image'] = ProfileImageCropper.crop(film_data.pop('image'), film_data.pop('coordinates'))
        film_data['profile_image_small'] = TempFileStorage.get(
            ImageResizer.resize(str(film_data['profile_image']), 560, 560, suffix='_560px', convert=False)
        )
        return Film.create(film_data)
