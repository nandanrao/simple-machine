import os
import subprocess

from django.conf import settings
from .proftpd_adapter import ProftpdAdapter


class FtpUploadInfoCreator(object):
    @staticmethod
    def get_for_film(film):
        if not film.get_ftp_upload_info():
            pa = ProftpdAdapter(settings.PROFTPD_AUTH_FILE)
            try:
                ref, user, passwd, home = pa.create_random_user(film.id)
            except Exception as e:
                return None

            film.set_ftp_upload_info(user, passwd)

        info = {
            'server': settings.BASE_URL.lstrip('http://').split(':')[0],
            'username': film.ftp_upload_info.username,
            'password': film.ftp_upload_info.password,
        }

        # If film already has exhibition file uploaded, then FTP credentials
        # are being generated for download
        # In that case, we need to make sure there is a hard link from the
        # MEDIA_ROOT folder to the /srv/ftp/incoming folder, so user can
        # actually see and download file
        if film.exhibition_file:
            # Make sure path exists
            subprocess.check_call(['mkdir', '-p', settings.FTP_UPLOAD_DIR + '/' + info['username']])
            hard_link_path = os.path.join(settings.FTP_UPLOAD_DIR, info['username'], film.exhibition_file.file.name)
            if not os.path.exists(hard_link_path):
                orig_path = os.path.join(settings.MEDIA_ROOT, 'films', 'exhibition_files', film.exhibition_file.file.name)
                subprocess.check_call(['ln', orig_path, hard_link_path])

        return info
