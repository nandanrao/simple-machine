import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django import http
from django.template.defaultfilters import slugify
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string

from common.browse_views import _get_item_pagination
from common.browse import Browse
from .models import Organization, Review

# Organization is now called publication

@login_required
def add_organization(request):
    org = Organization.create(request.POST['name'], request.user)
    return http.HttpResponse(str(org.id))


@login_required
def list_organizations(request):
    q = request.GET['q']
    orgs = Organization.for_user(request.user).filter(name__icontains=q)
    orgs_list = [{'id': o.id, 'text': o.name} for o in orgs]
    return http.HttpResponse(json.dumps(orgs_list), mimetype='application/json')


def organization_details(request, pk):
    reviewer = get_object_or_404(Organization, pk=pk)
    slug = slugify(reviewer.name)
    if not slug in request.get_full_path():
        return redirect(reverse('organization-details', args=[pk]) + slug)

    return render(request, 'reviewer_details.html', {'reviewer': reviewer})


def publication_films(request, pk):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    reviewer = get_object_or_404(Organization, pk=pk)
    films = [review.film for review in reviewer.reviews.order_by('-film__year').all() if review.film.is_public
                and review.film.runtime >= min_value and review.film.runtime <= max_value
                and review.film.year >= min_year and review.film.year <= max_year
                and (country_id == 'all' or review.film.countries.filter(pk=country_id))]
    return render(request, 'partials/_film_details_module.html', {
        'films': _get_item_pagination(films, request),
        'organization_id': pk
    })


@login_required
def sort_up_review(request):
    id = request.POST.get('id', None)

    if id:
        if Review.objects.filter(pk=id).exists():
            review = Review.objects.get(pk=id)
            film = review.film

            # reorder reviews
            index = 1
            for xreview in film.reviews.all():
                xreview.sort_order = index
                xreview.save()
                index += 1
            review = Review.objects.get(pk=id)  # update review

            sort_order = review.sort_order
            if sort_order > 1:
                review_current = review.film.reviews.filter(sort_order=sort_order-1).all()[0]
                review_current.sort_order = sort_order
                review.sort_order = sort_order - 1
                review.save()
                review_current.save()

        html = render_to_string('add_film_wizard/partials/_added_reviews.html', {
            'reviews': Review.objects.filter(film=film)
        })
        success = True
        ret_val = {'success': success, 'html': html}
        return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')

    return redirect('home')
