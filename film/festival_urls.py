from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required

from .festival_views import *


urlpatterns = patterns(
    '',
    url(r'^create/$', add_festival, name='add-festival'),
    url(r'^(?P<pk>\d+)/films/', festival_films,  name='festival-films'),
    url(r'^(?P<pk>\d+)/', festival_details,  name='festival-details'),
    url(r'^delete/$', delete_fof,  name='delete-fof'),
    url(r'^sort_up/$', sort_up_fof,  name='sort-up-fof'),
    url(r'^$', list_festivals, name='list-festivals'),
)
