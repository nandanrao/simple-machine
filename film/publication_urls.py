from django.conf.urls.defaults import patterns, url

from .publication_views import *


urlpatterns = patterns(
    '',
    url(r'^add/$', add_organization, name='add-organization'),
    url(r'^sort_up/$', sort_up_review,  name='sort-up-review'),
    url(r'^(?P<pk>\d+)/films/', publication_films,  name='publication-films'),
    url(r'^(?P<pk>\d+)/', organization_details, name='organization-details'),
    url(r'^$', list_organizations, name='list-organizations'),
)
