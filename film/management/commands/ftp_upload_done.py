import os

from django.core.management.base import BaseCommand
from film.models import FtpUploadInfo, ExhibitionFile

# This command is called by script that monitors for xferlog
# for appended lines. When this command is invoked, video file
# is already in films/exhibition_files folder

# Syntax: python manage.py ftp_upload_done filename ftp_user


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        filename, ftp_user = args[0], args[1]

        fui = FtpUploadInfo.objects.get(username=ftp_user)
        film = fui.film
        ef = ExhibitionFile(file=filename, uploaded=True)
        ef.via = 'FTP'
        ef.save()
        film.exhibition_file = ef
        film.save()
