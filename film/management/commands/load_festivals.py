from django.core.management.base import BaseCommand
from film.models import Festival
from _update_festivals import UpdateFestivals


class Command(BaseCommand):
    def handle(self, *args, **options):
        existing_festivals = set(Festival.visible.values_list('name', flat=True))
        wab_festivals = UpdateFestivals().get_data()

        for fest in wab_festivals:
            if fest['name'] not in existing_festivals:
                self._add_festival(fest)

    def _add_festival(self, festival_data):
        festival_data.update({'active': True})
        festival = Festival(**festival_data)
        festival.save()
