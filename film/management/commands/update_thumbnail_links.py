import json
import hashlib
import requests
from django.core.management.base import BaseCommand
from film.models import Film, VideoLinkConverter

class Command(BaseCommand):
    def handle(self, *args, **options):
        films_with_trailers = Film.objects.exclude(trailer_link=None)
        for film in films_with_trailers:
            id, source = VideoLinkConverter.get_trailer_data(film.trailer_link)
            thumbnail_url = None
            if source == 'youtube':
                thumbnail_url = 'http://img.youtube.com/vi/%s/maxresdefault.jpg' % id
                resp = requests.get(thumbnail_url)
                m = hashlib.md5()
                m.update(resp.content)
                if m.digest() == '\xe2\xdd\xfe\xe1\x1a\xe7\xed\xca\xe2W\xdaG\xf3\xa7\x8ap':
                    thumbnail_url = 'http://img.youtube.com/vi/%s/0.jpg' % id
            elif source == 'vimeo':
                resp = requests.get('http://vimeo.com/api/v2/video/%s.json' % id)
                if resp.status_code == 200:
                    thumbnail_url = resp.json()[0]['thumbnail_large']
            if thumbnail_url and film.thumbnail_link != thumbnail_url:
                film.thumbnail_link = thumbnail_url
                film.save()
