import requests
import lxml.html
import re


class Page(object):

    def __init__(self, html=None, base_url=''):
        self.doc = lxml.html.fromstring(html)
        self.base_url = base_url


class SearchResultsPage(Page):

    def get_all_festivals_links(self):
        link_elems = self.doc.cssselect(
            '.results-container table tr:first-of-type td:first-of-type a')
        for elem in link_elems:
            yield self.base_url + elem.attrib['href'] if self.base_url else \
                elem.attrib['href']


class FestivalPage(Page):

    def get_name(self):
        name = self.doc.cssselect('.formA h2')
        return name[0].text.strip() if len(name) else ''

    def _extract_festival_details(self):
        return self.doc.cssselect('span.formQ, span.formA')

    def _get_about_header_element(self, details):
        element = None
        for el in details:
            if 'class' in el.attrib and \
               el.attrib['class'] == 'formQ' and \
               el.find('strong') is not None and \
               el.find('strong').text is not None and \
               'about' in el.find('strong').text.lower():
                element = el
        return element

    def _is_valid_url(self, url):
        regex = re.compile(
            r'^https?://'
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?|'
            r'localhost|'
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
            r'(?::\d+)?'
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        return url is not None and regex.search(url)

    def get_about_text(self):
        details = self._extract_festival_details()
        about_header = self._get_about_header_element(details)
        about = ''
        if about_header in details:
            idx = details.index(about_header)
            try:
                about_desc_elem = details[idx + 1]
                text = about_desc_elem.text_content().strip()
                about = re.sub(r'(\s+)', ' ', text)
            except IndexError:
                pass
        return about

    def _get_general_info_links(self):
        general_info = self.doc.cssselect('.fest-general-table')
        if len(general_info) == 0:
            return []

        info_data = general_info[0].cssselect('.formA')[0]
        if len(info_data) == 0:
            return []

        links = info_data.getchildren()[0].findall('a')
        return links if len(links) > 0 else []

    def get_logo(self):
        images = self.doc.cssselect('form[name=cat] table tr td img')
        if len(images) == 0:
            return ''

        for img in images:
            if 'logo' in img.attrib['src']:
                logo_url = self.base_url + img.attrib['src']
                if self._is_valid_url(logo_url):
                    return logo_url
        return ''

    def get_website(self):
        link = None
        for l in self._get_general_info_links():
            url = l.attrib['href']
            if ('http://' in url or 'https://' in url) and 'new' in l.attrib['target']:
                link = l
        if link is not None and self._is_valid_url(link.attrib['href']):
            return link.attrib['href']
        return ''

    def get_email(self):
        link = None
        for l in self._get_general_info_links():
            if 'mailto:' in l.attrib['href']:
                link = l.attrib['href'].replace('mailto:', '')
        if link is not None:
            return link
        return ''

    def get_phone(self):
        divs = self.doc.cssselect('td .formA div')
        phone = ''
        for d in divs:
            for t in d.itertext():
                if 'phone' in t.lower():
                    phone = t.lower().replace('phone:', '').strip()
                    if len(phone) > 200:
                        return ''
        return phone

    def get_info(self):
        return {
            'name': self.get_name(),
            'about': self.get_about_text(),
            'website': self.get_website(),
            'email': self.get_email(),
            'logo': self.get_logo(),
            'phone': self.get_phone()
        }


class UpdateFestivals(object):

    def __init__(self, base_url='https://www.withoutabox.com',
                 login_url='/05home/login/05_login.php?submit=Login',
                 search_url='/03film/03t_fin/findafestival.php',
                 email='dev@rawjam.co.uk',
                 password='rawjam18'):
        self.email = email
        self.password = password
        self.base_url = base_url
        self.search_url = search_url
        self.login_url = login_url

        self._login()

    def _login(self):
        payload = {'login_name': self.email, 'password': self.password}
        r = requests.post(self.base_url + self.login_url, data=payload)
        self.cookies = r.cookies

    def _search_festivals_data(self):
        payload = {
            'cmd': 'search',
            'cboDeadline': '',
            'search_pool': 'ALL',
            'display': '99999',
        }
        r = requests.post(self.base_url + self.search_url, data=payload, cookies=self.cookies)
        if r.ok:
            return r.content
        return None

    def get_data(self):
        festivals = []
        data = self._search_festivals_data()
        page = SearchResultsPage(data, self.base_url)
        for url in page.get_all_festivals_links():
            r = requests.get(url)
            page = FestivalPage(r.content, self.base_url)
            festivals.append(page.get_info())
        return festivals
