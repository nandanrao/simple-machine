from film.models import DownloadRequest

class DownloadPolicy(object):
    @staticmethod
    def can_download(user, film):
        if film.belongs_to(user):
            return True

        download_request = DownloadRequest.get_or_none(film=film, user=user)
        if download_request and download_request.is_accepted:
            return True

        for venue in user.venues.all():
            if venue.has_in_progress_screening_of(film):
                return True

        return False
