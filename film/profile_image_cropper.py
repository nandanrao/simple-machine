import os

from django.conf import settings

from common.temp_file_storage import TempFileStorage
from common.image_cropper import ImageCropper


class ProfileImageCropper(object):
    @staticmethod
    def crop(image_path, coordinates):
        original_image = TempFileStorage.get(os.path.basename(image_path))
        cropped_filename = 'cropped_' + os.path.basename(image_path)
        cropped_image = TempFileStorage.open(cropped_filename)
        coords = coordinates.split(',')
        ImageCropper.crop(original_image, cropped_image, coords, settings.PROFILE_IMAGE_MAX_WIDTH)
        cropped_image.close()
        cropped_image = TempFileStorage.get(cropped_filename)
        return cropped_image
