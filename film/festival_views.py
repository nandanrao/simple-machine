import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django import http
from django.core.urlresolvers import reverse
from django.template.defaultfilters import slugify

from common.browse_views import _get_item_pagination
from common.browse import Browse
from .models import Festival, FilmOnFestival
from .forms import FestivalForm



@login_required
def add_festival(request):
    form = FestivalForm(request.POST or None)
    if form.is_valid():
        object = form.save(commit=False)
        object.user = request.user
        object.save()
        object = object  # TODO: ?????
        return http.HttpResponse('%s' % object.id)
    return render(request, 'create_festival.html', {'form': form})


def festival_details(request, pk):
    festival = get_object_or_404(Festival, pk=pk)
    slug = slugify(festival.name)
    if not slug in request.get_full_path():
        return redirect(reverse('festival-details', args=[pk]) + slug)
    return render(request, 'festival_details.html', {
        'festival': festival
    })


def festival_films(request, pk):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    festival = get_object_or_404(Festival, pk=pk)
    films = [film.film for film in festival.films.order_by('-film__year').all() if film.film.is_public
                and film.film.runtime >= min_value and film.film.runtime <= max_value
                and film.film.year >= min_year and film.film.year <= max_year
                and (country_id == 'all' or film.film.countries.filter(pk=country_id))]
    return render(request, 'partials/_film_details_module.html', {
        'films': _get_item_pagination(films, request)
    })


@login_required
def delete_fof(request):
    id = request.POST.get('id', None)

    # Delete from database
    if id:
        if FilmOnFestival.objects.filter(pk=int(id)).exists():
            fof = FilmOnFestival.objects.get(pk=(id))
            film = fof.film
            if fof.film.user == request.user:
                fof.delete()

        html = render_to_string('add_film_wizard/partials/_added_festivals.html', {
            'film_on_festivals': FilmOnFestival.objects.filter(film=film)
        })
        success = True
        ret_val = {'success': success, 'html': html}
        return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')

    return redirect('home')


@login_required
def sort_up_fof(request):
    id = request.POST.get('id', None)

    if id:
        if FilmOnFestival.objects.filter(pk=int(id)).exists():
            fof = FilmOnFestival.objects.get(pk=(id))
            film = fof.film

            # Reorder festivals
            index = 1
            for xfof in film.played_on.all():
                xfof.sort_order = index
                xfof.save()
                index += 1
            fof = FilmOnFestival.objects.get(pk=(id))  # update fof

            sort_order = fof.sort_order
            if sort_order > 1:
                fof_current = fof.film.played_on.filter(sort_order=sort_order-1).all()[0]
                fof_current.sort_order = sort_order
                fof.sort_order = sort_order - 1
                fof.save()
                fof_current.save()

        html = render_to_string('add_film_wizard/partials/_added_festivals.html', {
            'film_on_festivals': FilmOnFestival.objects.filter(film=film)
        })
        success = True
        ret_val = {'success': success, 'html': html}
        return http.HttpResponse(json.dumps(ret_val), mimetype='application/json')

    return redirect('home')


@login_required
def list_festivals(request):
    q = request.GET['q']
    festivals = Festival.for_user(request.user).filter(name__icontains=q)
    festival_list = [{'id': f.id, 'text': f.name} for f in festivals]
    return http.HttpResponse(json.dumps(festival_list), mimetype='application/json')
