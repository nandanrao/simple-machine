from django.conf.urls.defaults import patterns, url

from .critic_views import *


urlpatterns = patterns(
    '',
    url(r'^add/$', add_critic, name='add-reviewer'),
    url(r'^(?P<pk>\d+)/films/', critic_films,  name='critic-films'),
    url(r'^(?P<pk>\d+)/', critic_details, name='reviewer-details'),
    url(r'^$', list_critics, name='list-reviewers'),
)

