from django.conf.urls.defaults import patterns, url
from django.contrib.auth.decorators import login_required

from .views import *


urlpatterns = patterns(
    '',
    url(r'^search/$', search_films_json,  name='json-films'),

    # Dropbox
    url(r'^(?P<pk>\d+)/upload/dropbox/$', upload_from_dropbox,  name='upload-from-dropbox'),
    url(r'^(?P<pk>\d+)/upload/dropbox/progress/$', dropbox_upload_progress, name='dropbox-upload-progress'),
    url(r'^(?P<pk>\d+)/upload/dropbox/cancel/$', cancel_dropbox_upload,  name='cancel-dropbox-upload'),

    # Add and edit
    url(r'^add/$', add_film, name='add-film'),
    url(r'^add/(?P<pk>\d+)/media/$', add_film_media, name='add-film-media'),
    url(r'^add/(?P<pk>\d+)/festivals/$', add_film_festivals, name='add-film-festivals'),
    url(r'^add/(?P<pk>\d+)/reviews/$', add_film_reviews, name='add-film-reviews'),
    url(r'^add/(?P<pk>\d+)/cast/$', add_film_cast, name='add-film-cast'),
    url(r'^add/(?P<pk>\d+)/logistics/$', add_film_logistics, name='add-film-logistics'),
    # url(r'^imdb_info/$', get_imdb_info, name='imdb-info'),
    url(r'^(?P<pk>\d+)/edit/$', edit_film, name='edit-film'),
    url(r'^(?P<pk>\d+)/delete/$', delete_film, name='delete-film'),

    # Festivals
    url(r'^add/festivals/add-film/$', add_film_on_festival, name='add-film-on-festival'),
    # url(r'^festival/ajax/create/$', add_festival, name='add-festival'),
    # url(r'^festivals/(?P<pk>\d+)/$', festival_details,  name='festival-details'),
    # url(r'^festivals/delete/$', delete_fof,  name='delete-fof'),
    # url(r'^festivals/$', list_festivals, name='list-festivals'),

    # Stills
    url(r'^(?P<pk>\d+)/stills/upload/$', upload_still_image, name='upload-film-still'),
    url(r'^stills/remove/$', remove_still, name='remove-still'),

    # Files
    url(r'^(?P<pk>\d+)/files/exhibition/$', upload_exhibition_file, name='upload-exhibition-file'),
    url(r'^(?P<pk>\d+)/files/exhibition/download/$', download_exhibition_file, name='download-exhibition-file'),
    url(r'^(?P<pk>\d+)/files/exhibition/ftp-download/$', download_exhibition_file_ftp, name='download-exhibition-file-ftp'),
    url(r'^(?P<pk>\d+)/files/exhibition/dropbox-download/$', download_exhibition_file_dropbox, name='download-exhibition-file-dropbox'),
    url(r'^(?P<pk>\d+)/files/exhibition/ftp_upload_info/$', ftp_upload_info, name='ftp-upload-info'),
    url(r'^users_dropbox_files/$', users_dropbox_files, name='users-dropbox-files'),

    # Reviews
    url(r'^(?P<film_id>\d+)/reviews/add/$', add_review, name='add-review'),
    url(r'^reviews/delete/$', delete_review, name='delete-review'),

    # Cast & Crew
    url(r'^(?P<film_pk>\d+)/cast/add/$', add_cast, name='add-cast'),
    url(r'^(?P<film_pk>\d+)/crew/add/$', add_crew, name='add-crew'),
    url(r'^(?P<film_pk>\d+)/cast/(?P<cast_pk>\d+)/delete/$', delete_cast,  name='delete-cast'),
    url(r'^(?P<film_pk>\d+)/crew/(?P<crew_pk>\d+)/delete/$', delete_crew,  name='delete-crew'),

    # PreviewRequest
    url(r'^(?P<pk>\d+)/preview_request/$', send_preview_request, name='send-preview-request'),
    url(r'^(?P<film_id>\d+)/preview_request/(?P<prev_req_id>\d+)/accept/$', accept_preview_request, name='film-accept-preview-request'),
    url(r'^(?P<film_id>\d+)/preview_request/(?P<prev_req_id>\d+)/decline/$', decline_preview_request, name='film-decline-preview-request'),

    # DownloadRequest
    url(r'^(?P<pk>\d+)/download_request/$', send_download_request, name='send-download-request'),
    url(r'^(?P<film_id>\d+)/download_request/(?P<down_req_id>\d+)/accept/$', accept_download_request, name='film-accept-download-request'),
    url(r'^(?P<film_id>\d+)/download_request/(?P<down_req_id>\d+)/decline/$', decline_download_request, name='film-decline-download-request'),

    # Film payment
    url(r'^(?P<pk>\d+)/payment/$', film_payment, name='film-payment'),
    url(r'^(?P<pk>\d+)/paid/$', film_paid, name='film-paid'),

    # Film preview
    url(r'^(?P<film_id>\d+)/preview/$', preview_form_handler, name='film-preview-form-handler'),
    url(r'^preview/(?P<pk>\d+)/unlink/$', unlink_preview_file, name='unlink-preview-file'),

    # For browse page
    url(r'^by_letter/letter=(?P<letter>\w)$', films_by_letter, name='films-by-letter'),
    url(r'^(?P<film_id>\d+)/trailer/$', get_trailer_embed_code, name='film-get-trailer-embed-code'),
    url(r'^(?P<film_id>\d+)/preview_code/$', get_preview_embed_code, name='film-get-preview-embed-code'),

    # Film details (leave this last because of slug)
    url(r'^(?P<pk>\d+)/', film_details,  name='film-details'),

    # Tags
    url(r'^by_tag/tag=(?P<tag>.+)$', films_by_tag, name='films-by-tag'),
    url(r'^by_tag/$', films_by_tag, name='films-by-tag-search'),

)
