import time

from dropbox import session, client, rest
from django.conf import settings
from oauth.oauth import OAuthToken
from httplib import BadStatusLine

import logging

__all__ = ['get_authorization_url', 'get_access_token', 'get_films_for_user',
    'download_file', 'get_file_size', 'upload_file']

log = logging.getLogger('simplemachine')


def get_session():
    """Prepares Dropbox API access and returns the created session object.

    This is a low-level method useful for creating a Dropbox API client
    or user authentication. You probably don't need to use it directly.
    """

    return session.DropboxSession(
        settings.DROPBOX_APP_KEY,
        settings.DROPBOX_APP_SECRET,
        settings.DROPBOX_ACCESS_TYPE
    )

def get_client_from_string(access_token):
    key, secret = access_token.split(':')
    return get_client(OAuthToken(key, secret))

def get_client(access_token):
    """Prepare authorized Dropbox API client using the provided access_token.

    Returns the client.
    """

    sess = get_session()
    sess.set_token(access_token.key, access_token.secret)
    return client.DropboxClient(sess)


def get_authorization_url(callback=None):
    """Returns the OAuth authorization url to redirect the user to."""

    sess = get_session()
    request_token = sess.obtain_request_token()
    return sess.build_authorize_url(request_token, callback), request_token


def get_access_token(request_token):
    """Exchanges OAuth successful request token for users' access_token.

    Returns the users' access token."""

    return get_session().obtain_access_token(request_token)


def get_films_for_user(access_token):
    """Returns a list of files in the root directory of the app's folder
    in users' dropbox.

    See https://www.dropbox.com/developers/reference/api#metadata for detailed
    info about the data returned.
    """

    folder_metadata = get_client(access_token).metadata('/')
    return folder_metadata


def download_file(access_token, file_path, outfile, film_id, callback=None):
    """Download a file from users' app folder and store it to 'outfile' on
    the worker. If callback is passed in, it will be called with
    arguments (bytes_written, bytes_total, elapsed_seconds).

    Returns outfile if file was successfully downloaded, or None in case
    of error.
    """

    try:
        fd, meta = get_client(access_token).get_file_and_metadata(file_path)
    except rest.ErrorResponse:
        log.error("dbox.download_file(): can't get file '%s' from dropbox" %
            file_path, exc_info=True)
        return None

    t0 = time.time()
    t = 0
    total = meta.get('bytes', None)

    try:
        with open(outfile, 'wb') as outfd:
            written = 0
            callback(written, total, 0, film_id)
            while True:
                buf = fd.read(8192)
                if len(buf) > 0:
                    outfd.write(buf)
                    written += len(buf)
                else:
                    break

                if callback and (time.time() - t) > 5:
                    t = time.time()
                    callback(written, total, t - t0, film_id)

    except IOError as e:
        log.error("dbox.download_file(): I/O error downloading '%s' -> '%s'" %
            (file_path, outfile),  exc_info=True)
        return None

    return outfile


def get_file_size(access_token, path):
    """Returns size of the file (must exist) in users' app folder."""

    metadata = get_client(access_token).metadata(path)
    return metadata['bytes']


def upload_file(to_user, full_path, name, size):
    """
    Uploads file to Dropbox using chunked uploads
    """
    f = open(full_path, 'rb')
    uploader = to_user.get_chunked_uploader(f, size)
    while uploader.offset < size:
        try:
            upload = uploader.upload_chunked()
        except rest.ErrorResponse as e:
            log.error("There was error while uploading file %s to Dropbox. Trying again in 30 seconds. Error msg: %s" % (name, str(e)))
            time.sleep(30)
        except BadStatusLine as e:
            log.error("There was error while uploading file %s to Dropbox. Trying again in 30 seconds. Error msg: %s" % (name, str(e)))
            time.sleep(30)

    uploader.finish(name, overwrite=True)


def link_file_on_dropbox(client_from, client_to, path):
    try:
        resp = client_from.create_copy_ref(path)
        copy_ref = resp['copy_ref']
    except rest.ErrorResponse as e:
        log.error("Failed to make a copy_ref on Dropbox for file %s. Error msg: %s" % (
            path, str(e)
        ))
        raise e

    try:
        resp = client_to.add_copy_ref(copy_ref, path)
    except rest.ErrorResponse as e:
        log.error("Failed to copy file %s on Dropbox. Error msg: %s" % (path, str(e)))
        raise e


def file_exists(client, path):
    try:
        resp = client.get_file(path)
        resp.close()
        return True
    except rest.ErrorResponse as e:
        return False
