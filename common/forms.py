from django import forms
from django.contrib.auth.models import User

from notifications.models import EmailNotification
from common.html5_fields import EmailInput


class RegistrationForm(forms.Form):
    FORBIDDEN_NAMES = ('simple machine', 'simplemachine', 'smplmchn', 'smpl mchn')

    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Full Name *', 'autofocus':'autofocus'}))
    email = forms.EmailField(widget=EmailInput(attrs={'placeholder': 'Email *'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password *'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password (again) *'}))

    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 != password2:
            raise forms.ValidationError("Passwords do not match")

        return password2

    def clean_name(self):
        n = self.cleaned_data['name']
        if n.lower() in RegistrationForm.FORBIDDEN_NAMES:
            raise forms.ValidationError("Please choose another name")

        return n


class LoginForm(forms.Form):
    email = forms.EmailField(widget=EmailInput(attrs={'placeholder':'Email *', 'autofocus':'autofocus'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password *'}))


class ChangePasswordForm(forms.Form):
    current_password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Current Password *'}))
    new_password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'New Password *'}))
    new_password_rpt = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Confirm New Password *'}))

    def __init__(self, user, data=None):
        super(ChangePasswordForm, self).__init__(data)
        self.user = user

    def clean_new_password_rpt(self):
        new_password = self.cleaned_data['new_password']
        new_password_rpt = self.cleaned_data['new_password_rpt']
        if new_password != new_password_rpt:
            raise forms.ValidationError(
                'repeated password is not the same as new password')
        return new_password_rpt

    def clean_current_password(self):
        current_password = self.cleaned_data['current_password']
        if not self.user.check_password(current_password):
            raise forms.ValidationError(
                'wrong password')
        return current_password

    def change_password(self):
        if not self.is_valid():
            raise ValueError(
                'change_password called on non-valid form')

        new_password = self.cleaned_data['new_password']

        self.user.set_password(new_password)
        self.user.save()


class PasswordRecoveryEmailForm(forms.Form):
    email = forms.EmailField(widget=EmailInput(
        attrs={'placeholder': 'Email'}))

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError(
                'Email %s does not exist in our database' % email)
        return email

    def send_mail(self):
        if not self.is_valid():
            raise ValueError('send_mail called on non-valid form')

        EmailNotification.send_password_recovery_email(
            self.cleaned_data['email'].lower())


class PasswordRecoveryForm(forms.Form):
    new_password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'New Password *'}))
    new_password_rpt = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Confirm New Password *'}))

    def __init__(self, user, data=None):
        super(PasswordRecoveryForm, self).__init__(data)
        self.user = user

    def clean_new_password_rpt(self):
        new_password = self.cleaned_data['new_password']
        new_password_rpt = self.cleaned_data['new_password_rpt']
        if new_password != new_password_rpt:
            raise forms.ValidationError(
                'repeated password is not the same as new password')
        return new_password_rpt

    def change_password(self):
        if not self.is_valid():
            raise ValueError(
                'change_password called on non-valid form')

        new_password = self.cleaned_data['new_password']

        self.user.set_password(new_password)
        self.user.save()


class ChangeEmailForm(forms.Form):
    email = forms.EmailField(widget=EmailInput(
        attrs={'placeholder': 'New Email *'}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Password *'}))

    def __init__(self, user, data=None):
        super(ChangeEmailForm, self).__init__(data)
        self.user = user

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if email == self.user.email:
            raise forms.ValidationError('%s is your current email address' % email)
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('%s email address is already in used' % email)
        return email

    def clean_password(self):
        password = self.cleaned_data['password']
        if not self.user.check_password(password):
            raise forms.ValidationError(
                'wrong password')
        return password

    def send_mail(self):
        if not self.is_valid():
            raise ValueError('send_mail called on non-valid form')

        EmailNotification.send_email_change_email(self.user.id,
            self.cleaned_data['email'].lower())


class ChangeNameForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'New Name *'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password *'}))

    def __init__(self, user, data=None):
        super(ChangeNameForm, self).__init__(data)
        self.user = user

    def clean_name(self):
        name = self.cleaned_data['name']
        if name == self.user.get_profile().name:
            raise forms.ValidationError('%s is your current name' % name)
        return name

    def clean_password(self):
        password = self.cleaned_data['password']
        if not self.user.check_password(password):
            raise forms.ValidationError('wrong password')
        return password


class PaypalInfoForm(forms.Form):
    email = forms.EmailField(widget=EmailInput())
    next = forms.CharField(required=False, widget=forms.HiddenInput)
