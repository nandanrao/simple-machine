from django.conf import settings
from requests_oauthlib import OAuth1Session

class VimeoAPI(object):
    @staticmethod
    def _get_oauth_session(user_token):
        vimeo_access_token, vimeo_token_secret = user_token.split(':')
        vimeo = OAuth1Session(
            settings.VIMEO_CLIENT_ID,
            client_secret=settings.VIMEO_CLIENT_SECRET,
            resource_owner_key=vimeo_access_token,
            resource_owner_secret=vimeo_token_secret
        )
        return vimeo

    @staticmethod
    def get_display_name(user_token):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.oauth.checkAccessToken',
            'format': 'json'
        })
        oauth = resp.json().get('oauth')
        if oauth:
            return oauth.get('user').get('display_name')
        else:
            return None

    @staticmethod
    def add_video_to_watch_later(user_token, video_id):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.albums.addToWatchLater',
            'format': 'json',
            'video_id': video_id
        })
        if resp.json().get('err') is None:
            return True
        else:
            return False

    @staticmethod
    def get_user_videos(user_token):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.videos.getAll',
            'format': 'json',
            'user_id': user_token,
            'page': 1,
            'per_page': 50,
            'summary_response': True
        })
        if resp:
            return resp.json().get('videos').get('video')
        else:
            return None


    @staticmethod
    def get_video_info(user_token, video_id):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.videos.getInfo',
            'format': 'json',
            'video_id': video_id
        })
        if resp:
            return resp.json().get('video')
        else:
            return None


    @staticmethod
    def set_privacy(user_token, video_id, privacy, password=None):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.videos.setPrivacy',
            'format': 'json',
            'video_id': video_id,
            'privacy': privacy,
            'password': password
        })
        if resp:
            return resp.json()
        else:
            return None


    @staticmethod
    def set_embed_privacy(user_token, video_id, privacy):
        vimeo = VimeoAPI._get_oauth_session(user_token)
        resp = vimeo.get('http://vimeo.com/api/rest/v2', params={
            'method': 'vimeo.videos.embed.setPrivacy',
            'format': 'json',
            'video_id': video_id,
            'privacy': privacy
        })
        if resp:
            return resp.json()
        else:
            return None
