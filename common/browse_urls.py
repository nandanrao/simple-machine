from django.conf.urls.defaults import patterns, url

from .browse_views import *

urlpatterns = patterns('',
    url(r'^curators_by_type/type=(?P<curator_type>\w+)$', curators_by_type, name='curators-by-type'),
    url(r'^films_by_curator/type=(?P<curator_type>\w+)&(?P<curator_id>\d+)$', films_by_curator, name='films-by-curator'),
    url(r'^browse_filter/$', browse_films_filter, name='browse-films-filter'),
    url(r'^all-films/$', browse_all_films, name='browse-all-films'),
    url(r'^discover-ajax/$', browse_discover_ajax, name='browse-discover-ajax'),
    url(r'^$', browse_discover, name='browse'),
)
