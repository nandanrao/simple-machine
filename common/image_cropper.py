from PIL import Image


class ImageCropper(object):
    @staticmethod
    def crop(original, cropped, coords, max_width):
        orig_image = Image.open(original)
        img_size = orig_image.size
        coords = map(float, coords)
        coords = map(int, coords)
        orig_image.crop(coords).save(cropped)
