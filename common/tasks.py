from django.conf import settings
from celery import task

from screening.models import Venue
from .mixpanel_tracking import track


@task()
def mp_track(user, event, properties={}):
    """
    Sends an event to Mixpanel. Performed as a Celery task because it
    needs to be async (we don't want to delay HTTP response because of this)
    If we are running locally on dev environment, do nothing.
    """
    if user.is_authenticated():
        # This is a hack.
        # This is here because user object we received as an argument
        # to this method is not the same as user.get_profile().user
        # The latter doesn't have ID so we get an exception in
        # has_venue() method.
        user.get_profile().user = user
    	user_id = user.id
    else:
    	user_id = 0

    if user.is_authenticated() and user.get_profile().has_venue():
        has_venue = "true"
    else:
        has_venue = "false"

    if user.is_authenticated() and user.get_profile().has_film():
        has_film = "true"
    else:
        has_film = "false"

    properties['Venue owner'] = has_venue
    properties['Film owner'] = has_film
    properties['user_id'] = user_id

    if not settings.MIXPANEL_TOKEN == 'dummy_token':
        track(user_id, event, properties)
