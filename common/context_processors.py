def mixpanel_token(request):
    from django.conf import settings
    return {'MIXPANEL_TOKEN': settings.MIXPANEL_TOKEN}
