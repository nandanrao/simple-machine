import os

from PIL import Image


class ImageResizer(object):
    @staticmethod
    def resize(image, max_width, max_height, prefix='', suffix='', convert=True):
        if prefix == '' and suffix == '':
            prefix = 'v2_'
        dirname, filename = os.path.split(image)
        fname, ext = os.path.splitext(filename)
        im = Image.open(image)
        im2 = im.copy()
        im2.thumbnail((max_width, max_width), Image.ANTIALIAS)
        if convert:
            new_ext = '.jpg'    # All resized images are saved as JPG to reduce file sizes
        else:
            new_ext = ext
        new_path = os.path.join(dirname, prefix+fname+suffix+new_ext)
        im2.save(new_path)
        return new_path
