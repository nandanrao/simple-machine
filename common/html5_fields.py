from django import forms


class URLInput(forms.TextInput):

    input_type = "url"


class NumberInput(forms.TextInput):

    input_type = "number"


class EmailInput(forms.TextInput):

    input_type = "email"


class PhoneInput(forms.TextInput):

    input_type = "tel"
