import json

from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as django_login
from django.core.urlresolvers import reverse
from django import http
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.template.defaultfilters import slugify
from django.db.models import Max, Min
from django.template.loader import render_to_string

from film.models import Film, FilmStill, Curation, Festival, Reviewer, Organization
from screening.models import Venue, Screening
from screening.screening_organizer import ScreeningOrganizer
from .forms import RegistrationForm, LoginForm
from .user_creation import UserCreation, DuplicateEmail
from .models import UserProfile, Country
from .browse import Browse
from .curators_organizer import CuratorsOrganizer
from dropbox_api import get_authorization_url, get_access_token
from dropbox.session import OAuthToken
from .search import Search
from notifications.conversations import Conversations
from notifications.models import MessageDb, ConversationDb, EmailNotification
from notifications.news_item_generator import NewsItemGenerator
from notifications.conversation_creation import ConversationCreation
from django.shortcuts import get_object_or_404
from common.temp_file_storage import TempFileStorage, UnsupportedFileType
from common.tasks import mp_track
from common.image_tools import ImageTools


def home(request):
    if request.user.is_authenticated():
        films = Film.by_user(request.user)
        venues = Venue.by_user(request.user)
        screenings = ScreeningOrganizer.organize(Screening.for_user(request.user))
        conversations = Conversations.for_user(request.user)
        curations = Curation.for_user(request.user)
        return render(request, 'home.html', {
            'films': films,
            'venues': venues,
            'screenings': screenings,
            'conversations': conversations,
            'curations': curations,
            'home_page': True
        })
    else:
        bg_image = FilmStill.get_random()
        image_url = None
        film_title = None
        film_director = None
        film_url = "#"
        if bg_image:
            image_url = bg_image.medium.url
            film_title = bg_image.film.title
            film_director = bg_image.film.director
            film_url = reverse('film-details', args=[bg_image.film.id]) + slugify(bg_image.film.title)

        context = {
            'home_page': True,
            'image_url': image_url,
            'film_title': film_title,
            'film_director': film_director,
            'film_url': film_url,
            'home_anon': True
        }

        return render(request, 'home_anon.html', context)


def search(request):
    q = request.GET.get('q', None)
    # q = q or ''     # Search all for /search/
    results = Search(q).search()
    results = results[:200]
    mp_track.delay(request.user, 'user searched')
    return render(request, 'search_results.html', {'results': results, 'term':q, 'search_results_page':True})


def register(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid() and request.POST.get('phonenumber') == '616':
        try:
            user = UserCreation.create(name=form.cleaned_data.get('name'),
                                       email=form.cleaned_data.get('email').lower(),
                                       password=form.cleaned_data.get('password1'))
        except DuplicateEmail:
            form._errors['email'] = ['Please choose another email address']
            return render(request, 'registration/registration_form.html', {'form':form})

        return render(request, 'registration/registration_complete.html')
    return render(request, 'registration/registration_form.html', {'form':form, 'registration_page': True})


def login(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        user = authenticate(username=form.cleaned_data.get('email').lower(), password=form.cleaned_data.get('password'))
        if user is not None:
            if user.is_active:
                django_login(request, user)
                if 'next' in request.POST and request.POST['next'] != '':
                    return redirect(request.POST['next'])
                return redirect('home')
            else:
                messages.warning(request, 'Your account has not yet been activated. Please follow the link in the activation email.')
        else:
            messages.warning(request, 'Incorrect username or password. Please try again')

    next = ""
    if 'next' in request.GET:
        next = request.GET['next']
    return render(request, 'registration/login.html', {'form':form, 'next':next, 'login_page': True})


def signup_login(request):
    registration_form = RegistrationForm(request.POST or None)
    login_form = LoginForm(request.POST or None)
    return render(request, 'registration/signup_login.html', {
        'registration_form':registration_form,
        'login_form': login_form
    })


def activate(request, key):
    user_profile = UserProfile.objects.get(activation_key=key)
    if user_profile.user.is_active:
        messages.success(request, 'Your account is already activated, you can log in')
        return redirect('login')

    user_profile.user.is_active = True
    user_profile.user.save()

    # Create initial conversation
    sm_user = User.objects.get(email=settings.SM_USER)
    conversation = ConversationCreation.start_new(user_profile.user, sm_user)
    conversation.save()
    message = MessageDb.objects.create(from_user=sm_user, to_user=user_profile.user, content=settings.WELCOME_MESSAGE_TEXT)
    for_sender, for_receiver = NewsItemGenerator.send_message(conversation, from_user=sm_user, to_user=user_profile.user, message=message)
    for_sender.save()
    for_receiver.save()

    user = user_profile.user
    user.backend='django.contrib.auth.backends.ModelBackend'
    django_login(request, user)

    # If anonymous user "created cycle" (entered title and chose film) show him Add curation page
    if request.session.get('curation_title') and request.session.get('curation_film_id'):
        messages.success(request, 'Activation successful.')
        return redirect('add-curation')
    else:
        messages.success(request, 'Activation successful.')
        return redirect('home')


@login_required
def link_dropbox_account_url(request, current_film=None):
    if current_film:
        request.session['current_film'] = current_film
    if not request.user.get_profile().is_linked_with_dropbox():
        callback_url = request.build_absolute_uri(reverse('store-dropbox-token'))
        authorization_url, request_token = get_authorization_url(callback_url)
        request.session['dropbox_request_token_'+request_token.key] = request_token.key + ":" + request_token.secret
        return http.HttpResponse(authorization_url)
    else:
        return http.HttpResponse('#')


@login_required
def store_dropbox_token(request):
    request_token_key = request.GET.get('oauth_token')
    token = request.session['dropbox_request_token_'+request_token_key]
    key, secret = request.session['dropbox_request_token_'+request_token_key].split(':')
    request_token = OAuthToken(key, secret)
    access_token = get_access_token(request_token)
    request.user.get_profile().dropbox_access_token = access_token.key + ':' + access_token.secret
    request.user.get_profile().save()
    current_film_id = request.session.pop('current_film')
    messages.success(request, 'You have linked your account with Dropbox')
    return redirect('film-details', pk=current_film_id)


@login_required
def send_message(request, pk):
    mp_track.delay(request.user, 'message sent')
    receiver = get_object_or_404(User, pk=pk)
    message_text = request.POST.get('message_text', None)
    if message_text:
        message = MessageDb.objects.create(from_user=request.user, to_user=receiver, content=message_text)
        conversation = ConversationDb.for_participants(request.user, receiver)
        if not conversation:
            conversation = ConversationCreation.start_new(request.user, receiver)
            conversation.save()
        for_sender, for_receiver = NewsItemGenerator.send_message(conversation, from_user=request.user, to_user=receiver, message=message)
        for_sender.save()
        for_receiver.save()
        if for_receiver.for_user.get_profile().message_received_email:
            EmailNotification.send_message_received_email(request.user, for_receiver)
        return http.HttpResponse(json.dumps({'status':'success'}))
    return http.HttpResponse(json.dumps({'status':'error'}))


# Receives image via AJAX POST request, stores it to tmp folder
# and returns full path to that image. Used with for images that
# need to be cropped or shown right after upload for some reason
@login_required
def upload_tmp_image(request):
    image = request.FILES['file']
    try:
        name = TempFileStorage.save(image)
        img_size = ImageTools.get_size(TempFileStorage.get(name))
        url = TempFileStorage.url_for(name)
        return http.HttpResponse(json.dumps({
            "success": True,
            "filename": name,
            "url": url,
            "width": img_size[0],
            "height": img_size[1]
        }), mimetype='application/json')
    except UnsupportedFileType:
        return http.HttpResponse(json.dumps({
            "success": False,
            "reason": "Unsupported file type. Please upload .gif, .jpeg, .png or .bmp file"
        }), mimetype='application/json')


def user_page(request, pk):
    user = get_object_or_404(User, pk=pk)
    return render(request, 'user_details.html', {
        'user':user,
        'films': Film.active.filter(user=user).all(),
        'venues': Venue.active.filter(user=user).all()
    })


def what_it_is(request):
    return render(request, 'what_it_is.html')


def contact(request):
    return render(request, 'contact.html')


def developer(request):
    return render(request, 'developer.html')

def grants(request):
    return render(request, 'grants.html')


# def load_countries(request):
#     countries = Country.get_with_film_count()
#     return render(request, 'partials/_load_countries.html', {
#         'countries': countries
#     })


# def load_min_max_year(request):
#     years_range = Film.active.all().aggregate(Min('year'), Max('year'))
#     return http.HttpResponse(json.dumps({
#         'min_year': years_range['year__min'],
#         'max_year': years_range['year__max']
#     }), mimetype='application/json')


def load_filter_data(request):
    years_range = Film.active.all().aggregate(Min('year'), Max('year'))
    countries = Country.get_with_film_count()
    return http.HttpResponse(json.dumps({
        'min_year': years_range['year__min'],
        'max_year': years_range['year__max'],
        'countries': render_to_string('partials/_load_countries.html', {
            'countries': countries
        })
    }), mimetype='application/json')


def load_discover_data(request):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    festivals_with_film_count = Festival.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    critics_with_film_count = Reviewer.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    pubs_with_film_count = Organization.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    venues_with_film_count = Venue.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    curators = CuratorsOrganizer.sort_random(festivals_with_film_count,
                                            critics_with_film_count,
                                            pubs_with_film_count,
                                            venues_with_film_count)

    return render(request, 'browse/partials/_collection_list_discover.html', {
        'curators': curators,
    })
