import logging

from django.core.mail import send_mail
from django.conf import settings


log = logging.getLogger('simplemachine')

class Mailer(object):
    @staticmethod
    def send_to_badcop(subject, message):
        Mailer.send(subject, message, settings.BADCOP_EMAIL)

    @staticmethod
    def send(subject, message, to):
        log.info("Sending email %s to %s" % (subject, str(to)))
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [to])
