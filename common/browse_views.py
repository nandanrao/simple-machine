import requests
import string
import json

from itertools import chain

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.db.models import Max, Min

from film.models import Festival, Reviewer, Organization, Curation, Film
from screening.models import Screening, Venue
from .models import Country
from .browse import Browse
from .curators_organizer import CuratorsOrganizer
from .venues_organizer import VenuesOrganizer


def browse_discover(request):
    return render(request, 'browse/discover.html')


def browse_discover_ajax(request):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    festivals_with_film_count = Festival.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    critics_with_film_count = Reviewer.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    pubs_with_film_count = Organization.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    venues_with_film_count = Venue.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=3)
    curators = CuratorsOrganizer.sort_random(festivals_with_film_count,
                                            critics_with_film_count,
                                            pubs_with_film_count,
                                            venues_with_film_count)

    festivals_count = Festival.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0).count()
    critics_count = Reviewer.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0).count()
    pubs_count = Organization.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0).count()
    venues_count = Venue.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0).count()

    html = render_to_string('browse/partials/_collection_list_discover.html', {
        'curators': curators,
    })

    return HttpResponse(json.dumps({
        'html': html,
        'showcase_count': festivals_count + venues_count,
        'press_count': critics_count + pubs_count,
    }), mimetype='application/json')


def browse_all_films(request):
    countries = Country.get_with_film_count()
    years_range = Film.active.all().aggregate(Min('year'), Max('year'))
    return render(request, 'browse/all_films.html', {
        'countries': countries,
        'min_year': years_range['year__min'],
        'max_year': years_range['year__max']
    })


def curators_by_type(request, curator_type):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)

    if curator_type == 'showcase':
        festivals = Festival.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0)
        venues = Venue.get_with_film_count(min_value, max_value, min_year, max_year, country_id).filter(film_count__gt=0)
        curators = chain(festivals, venues)
    elif curator_type == 'press':
        publications = Organization.get_with_film_count(min_value, max_value, min_year, max_year, country_id)
        reviewers = Reviewer.get_with_film_count(min_value, max_value, min_year, max_year, country_id)
        curators = chain(publications, reviewers)
    else:
        return redirect(reverse('browse'))

    filtered_curator = CuratorsOrganizer.sort_by_film_number(curators)

    return render(request, 'browse/partials/_collection_list_all_collection.html', {
        'curators': _get_item_pagination(filtered_curator, request),
    })


def films_by_curator(request, curator_type, curator_id):
    films = None
    if curator_type == 'publication':
        curator_url = reverse('organization-details', args=(curator_id,))
        curator = get_object_or_404(Organization, pk=curator_id)
        reviews = curator.reviews.all()
        films = [r.film for r in reviews]
    elif curator_type == 'festival':
        curator_url = reverse('festival-details', args=(curator_id,))
        curator = get_object_or_404(Festival, pk=curator_id)
        fofs = curator.films.all()
        films = [f.film for f in fofs]
    elif curator_type == 'critic':
        curator_url = reverse('reviewer-details', args=(curator_id,))
        curator = get_object_or_404(Reviewer, pk=curator_id)
        reviews = curator.reviews.all()
        films = [r.film for r in reviews]
    elif curator_type == 'curations':
        curator_url = reverse('curation-details', args=(curator_id,))
        curator = get_object_or_404(Curation, pk=curator_id)
        films = curator.films.all()
    else:
        return redirect(reverse('browse'))

    # Remove duplicated films
    films = list(set(films))
    # Sort films, first by year then by title
    films = sorted(films, key=lambda f: f.title)
    films = sorted(films, key=lambda f: f.year, reverse=True)
    return render(request, 'browse/partials/_film_results.html', {
        'films': _get_item_pagination(films, request),
        'curator': curator,
        'curator_type': curator_type,
        'curator_url': curator_url
    })


def browse_films_filter(request):
    min_value, max_value, min_year, max_year, country_id = Browse.get_values_from_request(request)
    if min_value == 1 and max_value == 180:
        films = Film.objects.filter(deleted=False, is_public=True)
    else:
        films = Film.objects.filter(
            runtime__range=(min_value, max_value), deleted=False, is_public=True
        )
    if country_id != 'all':
        films = films.filter(countries__pk=country_id)
    films = films.filter(
        year__range=(min_year, max_year)
    )
    films = films.order_by('title')
    return render(request, 'browse/partials/_film_results.html', {
        'films': _get_item_pagination(films, request)
    })


# TODO: move this to some other .py
def _get_item_pagination(objects, request):
    """ Extract page and per_page values from request and return items for current page """
    try:
        page = int(request.GET.get('page')) if request.GET.get('page') else 1
    except:
        page = 1
    try:
        per_page = int(request.GET.get('per_page')) if request.GET.get('per_page') else 10
    except:
        per_page = 10
    pages = Paginator(objects, per_page)
    return pages.page(page)
