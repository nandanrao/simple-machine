from django.conf import settings

from mixpanel import Mixpanel


# mp = Mixpanel('6141bc16199fb389162cddbc0fd39274')
# mp = Mixpanel('65dae2a4c5d4dc56c74144af7fa09a05')
mp = Mixpanel(settings.MIXPANEL_TOKEN)

def track(user_id, event, properties):
    user_id = user_id or '0'
    mp.track(user_id, event, properties)
