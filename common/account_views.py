import requests

from django.shortcuts import render, redirect, get_object_or_404
from .forms import (ChangePasswordForm, PasswordRecoveryEmailForm,
    PasswordRecoveryForm, ChangeEmailForm, ChangeNameForm)
from django.views.decorators.http import require_POST
from django.contrib import messages
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django import forms, http
from requests_oauthlib import OAuth1Session

from common.forms import PaypalInfoForm
from common.models import UserProfile
from film.models import Film
from screening.models import Venue
from vimeo_api import VimeoAPI
from notifications.models import EmailList


@login_required
def default_page(request):
    return redirect('account_login_info')


@login_required
def login_info(request):
    change_pwd_form = ChangePasswordForm(request.user)
    change_email_form = ChangeEmailForm(request.user)
    change_name_form = ChangeNameForm(request.user)
    return render(request, 'account/login_info.html', {
        'change_name_form': change_name_form,
        'change_pwd_form': change_pwd_form,
        'change_email_form': change_email_form,
    })


@login_required
@require_POST
def change_password(request):
    form = ChangePasswordForm(request.user, request.POST)
    success_msg = ''
    if form.is_valid():
        form.change_password()
        success_msg = 'Password successfully changed'
        form = ChangePasswordForm(request.user)
    return render(request, 'account/partials/_change_pwd_form.html', {
        'form': form,
        'success_msg': success_msg
    })


@login_required
@require_POST
def change_email_form_handler(request):
    form = ChangeEmailForm(request.user, request.POST)
    success_msg = ''
    if form.is_valid():
        form.send_mail()
        success_msg = 'Visit your inbox and follow link to change email'
        form = ChangeEmailForm(request.user)
    return render(request, 'account/partials/_change_email_form.html', {
        'form': form,
        'success_msg': success_msg
    })


@login_required
def change_email_end(request, code):
    user_data = UserProfile.decode_signature(code)
    user = get_object_or_404(User, pk=user_data['user_id'])
    user.email = user_data['email']
    user.username = user_data['email']
    user.save()
    messages.info(request, 'Your email changed to %s' % user_data['email'])
    return redirect('account')


@login_required
def report_fraud(request):
    return render(request, 'account/fraud.html')


@login_required
def help(request):
    return render(request, 'account/help.html')


@login_required
def my_films(request):
    my_active_films = Film.objects.filter(user=request.user, deleted=False)
    return render(request, 'account/my_films.html', {'my_active_films': my_active_films})


@login_required
def my_venues(request):
    my_active_venues = Venue.active.filter(user=request.user)
    return render(request, 'account/my_venues.html', {'my_active_venues': my_active_venues})


@login_required
def show_payment_settings(request):
    current_email = request.user.get_profile().paypal_email
    return render(request, 'account/show_payment_settings.html', {'email': current_email})


@login_required
def change_payment_settings(request):
    current_email = request.user.get_profile().paypal_email
    form = PaypalInfoForm(request.POST or None, initial={'email':current_email, 'next':request.GET.get('next', None)})

    if form.is_valid():
        request.user.get_profile().paypal_email = form.cleaned_data['email']
        request.user.get_profile().save()
        messages.success(request, 'Email saved!')
        if 'next' in form.cleaned_data and form.cleaned_data['next']:
            return redirect(form.cleaned_data['next'])
        return redirect('show-payment-settings')

    return render(request, 'account/payment_settings.html', { 'form': form })


@login_required
def transaction_history(request):
    return render(request, 'account/text_content.html', {
        'content': ['TRANSACTION HISTORY'],
    })


def password_recovery_start(request):
    form = PasswordRecoveryEmailForm(request.POST or None)
    if form.is_valid():
        form.send_mail()
        messages.info(request,
            'Plese check your mail and click the link to recover your password.')
        form = PasswordRecoveryEmailForm()
    return render(request, 'account/password_recovery_start.html', {
        'form': form
    })


def password_recovery_end(request, code):
    user_dict = UserProfile.decode_signature(code)
    user = get_object_or_404(User, pk=user_dict['user_id'])
    form = PasswordRecoveryForm(user, request.POST or None)
    if form.is_valid():
        form.change_password()
        messages.info(request, 'Password successfully changed. You may log in.')
        return redirect('login')
    return render(request, 'account/password_recovery_end.html', {
        'form': form,
        'code': code
    })

@login_required
def change_name_form_handler(request):
    form = ChangeNameForm(request.user, request.POST)
    success_msg = ''
    if form.is_valid():
        success_msg = 'Your name has been changed'
        request.user.get_profile().name = form.cleaned_data['name']
        request.user.get_profile().save()
    return render(request, 'account/partials/_change_name_form.html', {
        'form': form,
        'success_msg': success_msg
    })


@login_required
def eventbrite(request):
    url = settings.EVENTBRITE_USER_AUTH_URL % (settings.EVENTBRITE_API_KEY, request.user.id)
    return render(request, 'account/partials/_eventbrite.html', {'url': url})


@login_required
def auth_eventbrite_access_code(request):
    """
    This view is used as a Redirect URI for Eventbrite OAuth2 procedure
    """

    access_code = request.GET.get('code', None)
    url = settings.EVENTBRITE_USER_AUTH_URL % (settings.EVENTBRITE_API_KEY, request.user.id)
    if access_code is None:
        return render(request, 'account/partials/_eventbrite.html', {'url': url})

    data = {
        'code': access_code,
        'client_secret': settings.EVENTBRITE_CLIENT_SECRET,
        'client_id': settings.EVENTBRITE_API_KEY,
        'grant_type': 'authorization_code'
    }
    resp = requests.post(
        settings.EVENTBRITE_ACCESS_TOKEN_URL,
        data=data,
        headers={'Content-type': 'application/x-www-form-urlencoded'}
    )

    access_token = resp.json().get('access_token', None)
    if access_token:
        request.user.get_profile().eventbrite_access_token = access_token
        request.user.get_profile().save()
        messages.success(request, 'You have successfully linked your Eventbrite account!')
        return redirect('account_eventbrite')
    else:
        messages.warning(request, 'There has been an error while trying to connect your Eventbrite account. Please try again.')

    return render(request, 'account/partials/_eventbrite.html', {'url': url})


@login_required
def vimeo(request):
    next = request.GET.get('next', None)
    vimeo = OAuth1Session(settings.VIMEO_CLIENT_ID, client_secret=settings.VIMEO_CLIENT_SECRET)
    if next:
        vimeo.params = {'oauth_callback': '%s/account/vimeo/auth/access_code?next=%s' % (settings.BASE_URL, next)}
    else:
        vimeo.params = {'oauth_callback': '%s/account/vimeo/auth/access_code' % settings.BASE_URL}
    fetch_response = vimeo.fetch_request_token(settings.VIMEO_REQUEST_TOKEN_URL)
    request.session['vimeo_token'] = fetch_response.get('oauth_token')
    request.session['vimeo_secret'] = fetch_response.get('oauth_token_secret')
    authorization_url = vimeo.authorization_url(settings.VIMEO_USER_AUTH_URL)
    url = '%s&permission=%s' % (authorization_url, settings.VIMEO_PERMISSION)
    user = request.user.get_profile()
    vimeo_name = VimeoAPI.get_display_name(user.vimeo_access_token) if user.is_linked_with_vimeo() else None
    return render(request, 'account/partials/_vimeo.html', {
        'url': url,
        'vimeo_name': vimeo_name
    })


@login_required
def auth_vimeo_access_code(request):
    """
    This view is used as a Callback URI for Vimeo OAuth procedure
    """

    next = request.GET.get('next', None)
    verifier = request.GET.get('oauth_verifier', None)
    oauth_token = request.session['vimeo_token']
    oauth_token_secret = request.session['vimeo_secret']
    del request.session['vimeo_token']
    del request.session['vimeo_secret']

    try:
        vimeo = OAuth1Session(
            settings.VIMEO_CLIENT_ID,
            client_secret=settings.VIMEO_CLIENT_SECRET,
            resource_owner_key=oauth_token,
            resource_owner_secret=oauth_token_secret,
            verifier=verifier
        )
        oauth_tokens = vimeo.fetch_access_token(settings.VIMEO_ACCESS_TOKEN_URL)
        request.user.get_profile().vimeo_access_token = "%s:%s" % (
            oauth_tokens.get('oauth_token'),
            oauth_tokens.get('oauth_token_secret')
        )
        request.user.get_profile().save()
        messages.success(request, 'You have successfully linked your Vimeo account!')
    except:
        messages.warning(
            request,
            'There has been an error while trying to connect your Vimeo account. Please try again.'
        )

    if next:
        return redirect(next)
    else:
        return redirect('account-vimeo')


@login_required
def unlink_vimeo(request):
    request.user.get_profile().vimeo_access_token = None
    request.user.get_profile().save()
    messages.success(request, 'You have successfully unlinked your Vimeo account!')
    return redirect('account-vimeo')


@login_required
def set_vimeo_watch_later(request):
    request.user.get_profile().vimeo_add_to_watch_later = True
    request.user.get_profile().save()
    messages.success(request, 'You have successfully update your Vimeo settings!')
    return redirect('account-vimeo')


@login_required
def unset_vimeo_watch_later(request):
    request.user.get_profile().vimeo_add_to_watch_later = False
    request.user.get_profile().save()
    messages.success(request, 'You have successfully update your Vimeo settings!')
    return redirect('account-vimeo')


@login_required
def emails(request):
    email_lists = EmailList.objects.all()
    return render(request, 'account/emails.html', {"email_lists": email_lists})


@login_required
def change_account_email_settings(request, settings_type):
    user = request.user.get_profile()
    if settings_type == 'message-received-yes':
        user.message_received_email = True
    elif settings_type == 'message-received-no':
        user.message_received_email = False
    elif settings_type == 'preview-request-yes':
        user.preview_request_email = True
    elif settings_type == 'preview-request-no':
        user.preview_request_email = False
    elif settings_type == 'download-request-yes':
        user.download_request_email = True
    elif settings_type == 'download-request-no':
        user.download_request_email = False
    user.save()
    return http.HttpResponse('')


@login_required
def change_account_email_list_settings(request, pk, settings_type):
    user = request.user
    email_list = EmailList.objects.get(pk=pk)
    if settings_type == 'unsubscribe':
        email_list.users.remove(user)
    elif settings_type == 'subscribe':
        email_list.users.add(user)
    email_list.save()
    return http.HttpResponse('')
