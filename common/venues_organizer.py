from collections import Counter


class VenuesOrganizer(object):

    @staticmethod
    def by_film(screenings):
        """
        Takes a list of screenings and returns a list of tuples,
        first value is film and second is a number of venues that had a screening
        of that film, ordered by number of venues, descending.
        """
        return Counter([s.film for s in screenings]).most_common()
