import random

class CuratorsOrganizer(object):

    @staticmethod
    def sort_alphabetical(curators_obj_list):
        """
        Returns list of dicts, filtered by curation type
        """
        curators = []
        curators.extend(CuratorsOrganizer._extract_dict(curators_obj_list))
        return sorted(curators, key=lambda c:c['name'].lower())


    @staticmethod
    def sort_by_film_number(curators_obj_list):
        """
        Returns list of dicts, filtered by curation type and sorted by film number
        """
        curators = []
        curators.extend(CuratorsOrganizer._extract_dict(curators_obj_list))
        return sorted(curators, key=lambda c:c['film_count'], reverse=True)


    @staticmethod
    def sort_random(festivals, critics, publications, venues):
        """
        Returns list of dicts, ordered randomly
        [
            {
                'id': 34,
                'type': 'festival',
                'name': 'SXSW',
                'film_count': 23,
                'random_profile_image': '/media/films/profile_images/cropped_f9a68ec9417ac1dc6e3c20403d9cc3a96e845d01_560px.png'
            },
            ...
        ]
        """
        curators = []
        curators.extend(CuratorsOrganizer._extract_dict(festivals))
        curators.extend(CuratorsOrganizer._extract_dict(critics))
        curators.extend(CuratorsOrganizer._extract_dict(publications))
        curators.extend(CuratorsOrganizer._extract_dict(venues))
        random.shuffle(curators)

        # Curations need to be 2x often than other collections + eliminate duplications
        random_curators = []
        for c in curators:
            if c not in random_curators:
                random_curators.append(c)

        return random_curators

    @staticmethod
    def _extract_dict(models):
        retval = []
        for m in models:
            if m.film_count == 0:
                continue
            retval.append({
                'id': m.id,
                'type': m.model_class(),
                'name': m.name,
                'film_count': m.film_count,
                'description': m.description
            })
        return retval
