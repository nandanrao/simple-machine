from django.db.models import Q, Count
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from film.models import Film, Festival, Reviewer, Review, FilmOnFestival
from screening.models import Venue, Screening


class Search(object):

    def __init__(self, term=''):
        self.term = term

    def _result_dict(self, name, link, type, image=None, films=None, director=None):
        vals = {'name': name, 'link': link, 'type': type}

        if image:
            vals.update({'image': image})
        if films:
            vals.update({'films': films})
        if director:
            vals.update({'director': director})

        return vals

    def _search_reviewer_by_name(self):
        return [self._result_dict(item.name, '', 'Reviewer') \
            for item in Reviewer.visible.filter(name__icontains=self.term)]

    def _search_review_by_text(self):
        return [self._result_dict(item.film.title,
            reverse('film-details', args=(item.film.pk,)), 'Review text') \
            for item in Review.objects.filter(full_text__icontains=self.term).select_related('film')]

    def _search_screening_by_description(self):
        return [self._result_dict(item.film.title + ' - ' + item.venue.name,
            '', 'Screening description') \
            for item in Screening.objects.filter(
                description__icontains=self.term).select_related('film', 'venue')]

    def _search_films(self):
        films = Film.active.filter(Q(synopsis__icontains=self.term) |
                                   Q(title__icontains=self.term) |
                                   Q(played_on__festival__name__icontains=self.term) |
                                   Q(director__icontains=self.term) |
                                   Q(user__userprofile__name__icontains=self.term) |
                                   Q(reviews__reviewer__name__icontains=self.term) |
                                   Q(reviews__organization__name__icontains=self.term) |
                                   Q(reviews__full_text__icontains=self.term)
                                  ).select_related('user', 'user__userprofile').prefetch_related('reviews', 'reviews__reviewer', 'reviews__organization', 'played_on', 'played_on__festival')

        films = set(films)
        by_title = []
        by_synopsis = []
        by_festivals = []
        by_director = []
        by_distribution = []
        by_user = []
        by_reviewer = []
        by_organization = []
        by_review_text = []

        # TODO: following 3 methods are duplication, refactor it
        def _matched_by_reviewer(term, reviewer_names):
            for name in reviewer_names:
                if term in name.lower():
                    return True

            return False

        def _matched_by_organization(term, org_names):
            for name in org_names:
                if term in name.lower():
                    return True

            return False

        def _matched_by_review_text(term, texts):
            for text in texts:
                if term in text.lower():
                    return True

        def _reviewer_names(reviews):
            names = []
            for r in reviews:
                if r.reviewer:
                    names.append(r.reviewer.name.lower())
            return names

        def _org_names(reviews):
            names = []
            for r in reviews:
                if r.organization and not r.reviewer:
                    names.append(r.organization.name.lower())
            return names

        def _review_texts(reviews):
            return [r.full_text for r in reviews]

        for film in films:
            reviewer_names = _reviewer_names(film.reviews.all())
            org_names = _org_names(film.reviews.all())
            review_texts = _review_texts(film.reviews.all())

            if self.term.lower() in film.title.lower():
                by_title.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif self.term.lower() in film.synopsis.lower():
                by_synopsis.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif self.term.lower() in film.director.lower():
                by_director.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif self.term.lower() in film.user.get_profile().name:
                by_user.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif _matched_by_reviewer(self.term.lower(), reviewer_names):
                by_reviewer.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif _matched_by_organization(self.term.lower(), org_names):
                by_organization.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            elif _matched_by_review_text(self.term.lower(), review_texts):
                by_review_text.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))
            else:
                by_festivals.append(self._result_dict(
                    film.title,
                    reverse('film-details', args=(film.pk,)),
                    'Film', image=film.profile_image_small.url,
                    director=film.director
                ))


        return (by_title, by_synopsis, by_festivals, by_director, by_distribution, by_user, by_reviewer, by_organization, by_review_text)

    def _search_venues(self):
        venues = Venue.active.filter(Q(name__icontains=self.term) |
            (Q(city__icontains=self.term) | Q(state__icontains=self.term) |
            Q(country__name__icontains=self.term))).select_related('country').prefetch_related('films', 'films__film')

        by_name = []
        by_tags = []

        def in_tags():
            city = venue.city is not None and \
                self.term.lower() in venue.city.lower()
            state = venue.state is not None and \
                self.term.lower() in venue.state.lower()
            country = venue.country is not None and \
                self.term.lower() in venue.country.name.lower()
            return city | state | country

        for venue in venues:
            films = [(fov.film.id, fov.film.title) for fov in venue.films.all() if fov.film.is_public]
            if self.term.lower() in venue.name.lower():
                by_name.append(self._result_dict(venue.name,
                    reverse('venue-details', args=(venue.pk,)), 'Venue', films=films))
            elif in_tags():
                by_tags.append(self._result_dict(venue.name,
                    reverse('venue-details', args=(venue.pk,)), 'Venue', films=films))

        return (by_name, by_tags)


    def _search_festivals(self):
        festivals = Festival.objects.filter(Q(name__icontains=self.term)).select_related('films', 'films__played_on').annotate(film_count=Count('films')).filter(film_count__gt=0)

        by_name = []
        for fest in festivals:
            screened_films = []
            fofs = fest.films.all()
            for f in fofs:
                if not f.film.deleted and f.film.is_public:
                    screened_films.append({'title': f.film.title, 'url':reverse('film-details', args=[f.film.id])})

            if self.term.lower() in fest.name.lower():
                by_name.append(self._result_dict(fest.name, reverse('festival-details', args=(fest.pk,)), 'Festival', films=screened_films))

        return by_name

    def _search_reviewers(self):
        reviewers = Reviewer.visible.filter(Q(name__icontains=self.term)).prefetch_related('reviews', 'reviews__film').annotate(review_count=Count('reviews')).annotate(film_count=Count('reviews__film', distinct=True)).filter(film_count__gt=0)
        films = Film.active.filter(Q(title__icontains=self.term)).filter(is_public=True).prefetch_related('reviews', 'reviews__reviewer', 'reviews__reviewer__reviews', 'reviews__reviewer__reviews__film')
        reviewers_by_films = []
        for f in films:
            for r in f.reviews.all():
                if r.reviewer:
                    reviewers_by_films.append(r.reviewer)

        def _matched_by_film_title(term, film_titles):
            for title in film_titles:
                if term.lower() in title.lower():
                    return True

            return False

        reviewers = list(reviewers)
        reviewers.extend(reviewers_by_films)

        by_name = []
        by_film_title = []
        for reviewer in reviewers:
            film_titles = [r.film.title for r in reviewer.reviews.all()]

            if _matched_by_film_title(self.term, film_titles):
                append_to = by_film_title
            else:
                append_to = by_name

            append_to.append(self._result_dict(
                reviewer.name,
                reverse('reviewer-details', args=(reviewer.pk,)),
                'Reviewer',
                films=[{'title': r.film.title, 'url': reverse('film-details', args=[r.film.id])} for r in reviewer.reviews.all() if r.film.is_public]
            ))

        return by_name, by_film_title

    def _search_users(self):
        users = User.objects.filter(Q(is_active=True) & Q(userprofile__name__icontains=self.term)).select_related('userprofile').prefetch_related('films')

        by_name = []

        for user in users:
            by_name.append(self._result_dict(
                user.userprofile.name,
                reverse('user-page', args=(user.pk,)),
                'User',
                films=[{'title': f.title, 'url': reverse('film-details', args=[f.id])} for f in user.films.all() if f.is_public]
            ))

        return by_name

    def search(self):
        results = []

        if not self.term:
            return []

        films_by_title, films_by_synopsis, films_by_festivals, films_by_director, films_by_distribution, films_by_user, films_by_reviewer, films_by_organization, films_by_review_text = self._search_films()
        venues_by_name, venues_by_tags = self._search_venues()
        fest_by_name = self._search_festivals()
        reviewers_by_name, reviewers_by_film_title = self._search_reviewers()
        users_by_name = self._search_users()

        results = (films_by_title + films_by_director + films_by_distribution + films_by_reviewer + films_by_organization + venues_by_name + films_by_festivals + fest_by_name + films_by_synopsis + films_by_review_text + venues_by_tags + reviewers_by_name + users_by_name + reviewers_by_film_title)

        return results
