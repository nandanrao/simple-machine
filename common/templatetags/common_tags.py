import datetime
import pytz

from django import template
from django.template.defaultfilters import timesince, date


register = template.Library()

@register.inclusion_tag('helpers/form.html')
def render_form(form):
    return {'form':form}

@register.filter('field_type')
def field_type(ob):
    return ob.__class__.__name__

@register.filter
def sm_date(value):
    timediff = datetime.datetime.now(pytz.utc) - value
    if datetime.timedelta(hours=23) > timediff:
        return timesince(value) + ' ago'
    return date(value, 'd.NY').upper()

@register.filter
def sort_film_reviewer(film, reviewer_pk):
    pk = int(reviewer_pk)
    reviews = film.reviews.filter(reviewer__isnull=False).all()
    return sorted(reviews, key=lambda k: (-pk if k.reviewer.id == pk else k.reviewer.id))


@register.filter
def sort_film_organization(film, organization_pk):
    pk = int(organization_pk)
    reviews = film.reviews.filter(organization__isnull=False).all()
    return sorted(reviews, key=lambda k: (-pk if k.organization.id == pk else k.organization.id))
