import os
import hashlib
import datetime

from django.conf import settings
from django.core.files.storage import FileSystemStorage

class UnsupportedFileType(Exception):
    pass

class TempFileStorage(object):
    LOCATION = os.path.join(settings.MEDIA_ROOT, 'tmp')
    BASE_URL = '/media/tmp/'
    FILE_STORAGE = FileSystemStorage(location=LOCATION, base_url=BASE_URL)
    ALLOWED_FILE_TYPES = (
        '.jpg', '.jpeg', '.png', '.bmp', '.gif', '.mp4', '.avi', '.mov'
    )

    @staticmethod
    def save(file):
        m = hashlib.sha1()
        m.update(str(datetime.datetime.now()))
        fname, ext = os.path.splitext(file.name)
        if ext.lower() in TempFileStorage.ALLOWED_FILE_TYPES:
            return TempFileStorage.FILE_STORAGE.save(m.hexdigest() + ext, file)
        else:
            raise UnsupportedFileType

    @staticmethod
    def get(name):
        return TempFileStorage.FILE_STORAGE.open(name)

    @staticmethod
    def url_for(name):
       return TempFileStorage.FILE_STORAGE.url(name)

    @staticmethod
    def open(name):
        return TempFileStorage.FILE_STORAGE.open(name, 'wb')

    @staticmethod
    def path(name):
        return TempFileStorage.FILE_STORAGE.path(name)
