from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core import signing
from django.db.models import Count


class Country(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_with_film_count():
        return Country.objects.filter(
            films__deleted=False, films__is_public=True
        ).annotate(film_count=Count('films')).filter(film_count__gt=0).order_by('-film_count')


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=100)
    activation_key = models.CharField(max_length=200)
    dropbox_access_token = models.CharField(max_length=100)
    paypal_email = models.EmailField(max_length=150)
    eventbrite_access_token = models.CharField(max_length=100, null=True, blank=True)
    vimeo_access_token = models.CharField(max_length=100, null=True, blank=True)
    vimeo_add_to_watch_later = models.BooleanField(default=True)
    message_received_email = models.BooleanField(default=True)
    preview_request_email = models.BooleanField(default=True)
    download_request_email = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)

    def is_linked_with_dropbox(self):
        return self.dropbox_access_token

    def is_linked_with_vimeo(self):
        return self.vimeo_access_token

    def is_linked_with_paypal(self):
        return self.paypal_email

    def active_curations(self):
        return [c for c in self.user.curations.all() if not c.deleted]

    @staticmethod
    def encode_signature_with_email(email):
        code = signing.dumps({
            'email': email,
            'user_id': User.objects.get(email=email).id
        })
        return code

    @staticmethod
    def decode_signature(code):
        return signing.loads(code)

    @staticmethod
    def encode_signature_with_user_id_and_email(user_id, email):
        code = signing.dumps({
            'email': email,
            'user_id': user_id
        })
        return code

    def has_verified_venue(self):
        venues = self.user.venues.all()
        for v in venues:
            if v.is_verified and not v.deleted:
                return True

        return False

    def has_venue(self):
        if self.user.venues.count():
            return True

        return False

    def has_film(self):
        if self.user.films.count():
            return True

        return False


class Progress(models.Model):
    model = models.CharField(max_length=50)
    model_id = models.IntegerField()
    time_elapsed = models.PositiveIntegerField()
    done = models.BigIntegerField()
    total = models.BigIntegerField()
    finished = models.BooleanField()

    @property
    def percentage(self):
        return int((float(self.done)/self.total) * 100)


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)
