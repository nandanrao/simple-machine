import requests
import json

from django.conf import settings
from common.paypal_fee_amortizer import PaypalFeeAmortizer


class Paypal(object):
    @staticmethod
    def prepare_for(screening, base_url):
        headers = {
            'X-PAYPAL-SECURITY-USERID': settings.PAYPAL_API_USERNAME,
            'X-PAYPAL-SECURITY-PASSWORD': settings.PAYPAL_API_PASSWORD,
            'X-PAYPAL-SECURITY-SIGNATURE': settings.PAYPAL_API_SIGNATURE,
            'X-PAYPAL-REQUEST-DATA-FORMAT':'JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT':'JSON',
            'X-PAYPAL-APPLICATION-ID': settings.PAYPAL_APPLICATION_ID
        }

        success_url = '%s/screenings/%d/paid/?token=%s' % (
            base_url, screening.id, screening.payment_token
        )
        cancel_url = '%s/screenings/%d/pay/' % (
            base_url, screening.id
        )

        fo_amount, sm_amount = PaypalFeeAmortizer.calculate(screening.film_owners_earning, screening.simplemachine_earning)
        params = {
            'actionType': 'PAY',
            'receiverList':{
                'receiver':[
                    {
                        'email': settings.SIMPLEMACHINE_PAYPAL_EMAIL,
                        'amount':sm_amount
                    },
                    {
                        'email':screening.film.user.get_profile().paypal_email,
                        'amount':fo_amount
                    }
                ]
            },
            'currencyCode':'USD',
            'cancelUrl': cancel_url,
            'returnUrl': success_url,
            'requestEnvelope':{
                'errorLanguage':'en_US',
                'detailLevel':'ReturnAll'
            }
        }

        resp = requests.post(settings.PAYPAL_SVCS_ENDPOINT+"/AdaptivePayments/Pay", data=json.dumps(params), headers=headers)

        resp_json = json.loads(resp.content)
        pay_key = resp_json['payKey']
        options = {
            'displayOptions':{
                'businessName': 'Simple Machine'
            }
        }

        options.update({
            'payKey':pay_key,
            'requestEnvelope':{
                'errorLanguage':'en_US',
                'detailLevel':'ReturnAll'
            }
        })

        resp2 = requests.post(settings.PAYPAL_SVCS_ENDPOINT+"/AdaptivePayments/SetPaymentOptions", data=json.dumps(options), headers=headers)
        return settings.PAYPAL_POSTBACK_ENDPOINT+"?cmd=_ap-payment&paykey=%s" % pay_key


    @staticmethod
    def prepare_film_payment(film, base_url, payment_token, amount):
        headers = {
            'X-PAYPAL-SECURITY-USERID': settings.PAYPAL_API_USERNAME,
            'X-PAYPAL-SECURITY-PASSWORD': settings.PAYPAL_API_PASSWORD,
            'X-PAYPAL-SECURITY-SIGNATURE': settings.PAYPAL_API_SIGNATURE,
            'X-PAYPAL-REQUEST-DATA-FORMAT':'JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT':'JSON',
            'X-PAYPAL-APPLICATION-ID': settings.PAYPAL_APPLICATION_ID
        }

        success_url = '%s/films/%d/paid/?token=%s&amount=%s' % (
            base_url, film.id, payment_token, amount
        )
        cancel_url = '%s/films/%d/' % (
            base_url, film.id
        )

        params = {
            'actionType': 'PAY',
            'receiverList':{
                'receiver':[
                    {
                        'email': film.user.get_profile().paypal_email,
                        'amount': amount
                    }
                ]
            },
            'currencyCode': 'USD',
            'cancelUrl': cancel_url,
            'returnUrl': success_url,
            'requestEnvelope': {
                'errorLanguage': 'en_US',
                'detailLevel': 'ReturnAll'
            }
        }

        resp = requests.post(settings.PAYPAL_SVCS_ENDPOINT+"/AdaptivePayments/Pay", data=json.dumps(params), headers=headers)

        resp_json = json.loads(resp.content)
        pay_key = resp_json['payKey']
        options = {
            'displayOptions':{
                'businessName': 'Simple Machine'
            }
        }

        options.update({
            'payKey': pay_key,
            'requestEnvelope':{
                'errorLanguage': 'en_US',
                'detailLevel': 'ReturnAll'
            }
        })

        resp2 = requests.post(settings.PAYPAL_SVCS_ENDPOINT+"/AdaptivePayments/SetPaymentOptions", data=json.dumps(options), headers=headers)
        return settings.PAYPAL_POSTBACK_ENDPOINT+"?cmd=_ap-payment&paykey=%s" % pay_key
