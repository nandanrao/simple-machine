import hashlib
import random

from django.contrib.auth.models import User
# from django.core.mail import send_mail
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string


ACTIVATION_EMAIL_TEXT = """
    Thank you for registering on Simple Machine site.
    Please visit this link to activate your account: %s/activate/%s

    Thank you!
    """


class DuplicateEmail(Exception):
    pass


class UserCreation(object):
    @staticmethod
    def create(name, email, password):
        if User.objects.filter(email=email).exists():
            raise DuplicateEmail

        user = User.objects.create(username=email, email=email)
        user.set_password(password)
        user.is_active = False
        user.save()
        user.get_profile().name = name
        user.get_profile().activation_key = UserCreation._generate_activation_key(user.email)
        user.get_profile().save()

        link = '%s/activate/%s/' % (settings.BASE_URL, user.get_profile().activation_key)
        context = {'activation_link': link}
        body = render_to_string('emails/registration_email.html', context)
        subject = 'Simple Machine activation'
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[user.email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def _generate_activation_key(email):
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        return hashlib.sha1(salt+email).hexdigest()
