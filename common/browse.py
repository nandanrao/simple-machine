import random

from film.models import Review, Festival, Reviewer, Organization, Film
from django.db.models import Max, Min


class Browse(object):
    NUM_SLOTS = 30

    @staticmethod
    def get_results():
        results = []
        buckets = {
            'reviews': list(Review.get_random(Browse.NUM_SLOTS)),
            'festivals': list(reversed(Festival.get_top(Browse.NUM_SLOTS))),
            'critics': list(reversed(Reviewer.get_top(Browse.NUM_SLOTS))),
            'publications': list(reversed(Organization.get_top(Browse.NUM_SLOTS)))
        }

        bucket_names = buckets.keys()
        # We need to show reviews twice as often as everything else
        bucket_names.extend(['reviews'] * len(bucket_names))
        def _get_next_result():
            if not bucket_names:
                return None

            random_bucket = bucket_names[random.randint(0, len(bucket_names) - 1)]
            try:
                return buckets[random_bucket].pop()
            except IndexError:
                bucket_names.remove(random_bucket)
                return _get_next_result()

        for i in range(Browse.NUM_SLOTS):
            result = _get_next_result()
            if result is None:
                break
            results.append(result)

        return results

    @staticmethod
    def get_values_from_request(request):
        years_range = Film.active.all().aggregate(Min('year'), Max('year'))
        return (
            Browse._parse_int_value(request.GET.get('min'), 0),
            Browse._parse_int_value(request.GET.get('max'), 180),
            Browse._parse_int_value(request.GET.get('minYear'), years_range['year__min']),
            Browse._parse_int_value(request.GET.get('maxYear'), years_range['year__max']),
            Browse._parse_int_value(request.GET.get('country_id'), 'all'),
        )

    @staticmethod
    def _parse_int_value(value, default_value=0):
        try:
            value = int(value)
        except:
            value = default_value
        return value
