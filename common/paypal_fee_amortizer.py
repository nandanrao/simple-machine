class PaypalFeeAmortizer(object):
    " Calculates fees so that SM pays all (FO and SM) fees in Paypal Parallel payments "

    @staticmethod
    def calculate(fo_amount, sm_amount):
        fo_amount_fee = fo_amount * 0.03 + 0.3
        new_fo_amount = round(fo_amount + fo_amount_fee, 2)
        new_sm_amount = round(sm_amount - fo_amount_fee, 2)
        if new_sm_amount < 0:
            raise ValueError
        return new_fo_amount, new_sm_amount
