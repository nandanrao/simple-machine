from PIL import Image


class ImageTools(object):
    @staticmethod
    def get_size(image):
        orig_image = Image.open(image)
        return orig_image.size
