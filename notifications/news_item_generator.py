from notifications.models import NewsItem, NewsItemType


class NewsItemGenerator(object):
    @staticmethod
    def preview_request_sent(preview_request, conversation):
        for_sender = NewsItem(for_user=preview_request.user,
                              type=NewsItemType.PREVIEW_REQUEST_SENT,
                              sent=True,
                              read=True,
                              conversation=conversation,
                              content_object=preview_request)
        for_film_owner = NewsItem(for_user=preview_request.film.user,
                                  type=NewsItemType.PREVIEW_REQUEST_SENT,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=preview_request)
        return for_film_owner, for_sender


    @staticmethod
    def preview_request_accepted(preview_request, conversation):
        # Delete old NewsItems if exist
        NewsItem.objects.filter(for_user=preview_request.film.user,
                                type=NewsItemType.PREVIEW_REQUEST_ACCEPTED,
                                conversation=conversation,
                                object_id=preview_request.id).delete()
        NewsItem.objects.filter(for_user=preview_request.user,
                                type=NewsItemType.PREVIEW_REQUEST_ACCEPTED,
                                conversation=conversation,
                                object_id=preview_request.id).delete()
        for_film_owner = NewsItem(for_user=preview_request.film.user,
                                  type=NewsItemType.PREVIEW_REQUEST_ACCEPTED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=preview_request)
        for_requested_user = NewsItem(for_user=preview_request.user,
                                  type=NewsItemType.PREVIEW_REQUEST_ACCEPTED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=preview_request)
        return for_film_owner, for_requested_user


    @staticmethod
    def preview_request_declined(preview_request, conversation):
        # Delete old NewsItems if exist
        NewsItem.objects.filter(for_user=preview_request.film.user,
                                type=NewsItemType.PREVIEW_REQUEST_DECLINED,
                                conversation=conversation,
                                object_id=preview_request.id).delete()
        NewsItem.objects.filter(for_user=preview_request.user,
                                type=NewsItemType.PREVIEW_REQUEST_DECLINED,
                                conversation=conversation,
                                object_id=preview_request.id).delete()
        for_film_owner = NewsItem(for_user=preview_request.film.user,
                                  type=NewsItemType.PREVIEW_REQUEST_DECLINED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=preview_request)
        for_requested_user = NewsItem(for_user=preview_request.user,
                                  type=NewsItemType.PREVIEW_REQUEST_DECLINED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=preview_request)
        return for_film_owner, for_requested_user


    @staticmethod
    def download_request_sent(download_request, conversation):
        for_sender = NewsItem(for_user=download_request.user,
                              type=NewsItemType.DOWNLOAD_REQUEST_SENT,
                              sent=True,
                              read=True,
                              conversation=conversation,
                              content_object=download_request)
        for_film_owner = NewsItem(for_user=download_request.film.user,
                                  type=NewsItemType.DOWNLOAD_REQUEST_SENT,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=download_request)
        return for_film_owner, for_sender


    @staticmethod
    def download_request_accepted(download_request, conversation):
        # Delete old NewsItems if exist
        NewsItem.objects.filter(for_user=download_request.film.user,
                                type=NewsItemType.DOWNLOAD_REQUEST_ACCEPTED,
                                conversation=conversation,
                                object_id=download_request.id).delete()
        NewsItem.objects.filter(for_user=download_request.user,
                                type=NewsItemType.DOWNLOAD_REQUEST_ACCEPTED,
                                conversation=conversation,
                                object_id=download_request.id).delete()
        for_film_owner = NewsItem(for_user=download_request.film.user,
                                  type=NewsItemType.DOWNLOAD_REQUEST_ACCEPTED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=download_request)
        for_requested_user = NewsItem(for_user=download_request.user,
                                  type=NewsItemType.DOWNLOAD_REQUEST_ACCEPTED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=download_request)
        return for_film_owner, for_requested_user


    @staticmethod
    def download_request_declined(download_request, conversation):
        # Delete old NewsItems if exist
        NewsItem.objects.filter(for_user=download_request.film.user,
                                type=NewsItemType.DOWNLOAD_REQUEST_DECLINED,
                                conversation=conversation,
                                object_id=download_request.id).delete()
        NewsItem.objects.filter(for_user=download_request.user,
                                type=NewsItemType.DOWNLOAD_REQUEST_DECLINED,
                                conversation=conversation,
                                object_id=download_request.id).delete()
        for_film_owner = NewsItem(for_user=download_request.film.user,
                                  type=NewsItemType.DOWNLOAD_REQUEST_DECLINED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=download_request)
        for_requested_user = NewsItem(for_user=download_request.user,
                                  type=NewsItemType.DOWNLOAD_REQUEST_DECLINED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=download_request)
        return for_film_owner, for_requested_user


    @staticmethod
    def screening_invitation(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_INVITATION,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_INVITATION,
                                   sent=True,
                                   read=True,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def screening_accepted(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_ACCEPTED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_ACCEPTED,
                                   sent=False,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def screening_declined(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_DECLINED,
                                  sent=True,
                                  read=True,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_DECLINED,
                                   sent=False,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def screening_cancelled(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_CANCELLED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_CANCELLED,
                                   sent=True,
                                   read=True,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def screening_updated(screening, conversation):
        # Delete old UPDATE NewsItems if exist
        NewsItem.objects.filter(for_user=screening.film.user,
                                type=NewsItemType.SCREENING_UPDATED,
                                conversation=conversation,
                                object_id=screening.id).delete()
        NewsItem.objects.filter(for_user=screening.venue.user,
                                type=NewsItemType.SCREENING_UPDATED,
                                conversation=conversation,
                                object_id=screening.id).delete()
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_UPDATED,
                                  sent=False,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_UPDATED,
                                   sent=True,
                                   read=True,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def send_message(conversation, from_user, to_user, message):
        for_sender = NewsItem(for_user=from_user,
                              type=NewsItemType.MESSAGE,
                              sent=True,
                              read=True,
                              conversation=conversation,
                              content_object=message)
        for_receiver = NewsItem(for_user=to_user,
                                   type=NewsItemType.MESSAGE,
                                   sent=False,
                                   conversation=conversation,
                                   content_object=message)
        return for_sender, for_receiver


    @staticmethod
    def screening_report_venue(screening, conversation):
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_REPORT_VENUE,
                                   conversation=conversation,
                                   content_object=screening,
                                   actionable=True)
        return for_venue_owner


    @staticmethod
    def screening_report_reminder_venue(screening, conversation):
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_REPORT_REMINDER_VENUE,
                                   conversation=conversation,
                                   content_object=screening)
        return for_venue_owner


    @staticmethod
    def screening_report_reminder_film(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                   type=NewsItemType.SCREENING_REPORT_REMINDER_FILM,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner


    @staticmethod
    def screening_report_film(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_REPORT_FILM,
                                  conversation=conversation,
                                  content_object=screening,
                                  actionable=True)
        return for_film_owner


    @staticmethod
    def screening_report_venue_sent(screening, conversation):
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_SHOWN,
                                   conversation=conversation,
                                   sent=True,
                                   read=True,
                                   content_object=screening)
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_SHOWN,
                                  conversation=conversation,
                                  content_object=screening)
        for_film_owner_report = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_REPORT_FILM,
                                  conversation=conversation,
                                  content_object=screening,
                                  actionable=True)
        return for_venue_owner, for_film_owner, for_film_owner_report


    @staticmethod
    def screening_pay_upfront(screening, conversation):
        upfront_pay = NewsItem(for_user=screening.venue.user,
                               type=NewsItemType.SCREENING_PAY_UPFRONT,
                               conversation=conversation,
                               content_object=screening)
        return upfront_pay


    @staticmethod
    def screening_report_film_sent(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_REPORT_FILM_SENT,
                                  conversation=conversation,
                                  sent=True,
                                  read=True,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_REPORT_FILM_SENT,
                                   conversation=conversation,
                                   content_object=screening)
        return for_film_owner, for_venue_owner


    @staticmethod
    def screening_paid(screening, conversation):
        for_film_owner = NewsItem(for_user=screening.film.user,
                                  type=NewsItemType.SCREENING_PAID,
                                  conversation=conversation,
                                  content_object=screening)
        for_venue_owner = NewsItem(for_user=screening.venue.user,
                                   type=NewsItemType.SCREENING_PAID,
                                   conversation=conversation,
                                   sent=True,
                                   content_object=screening)
        return for_film_owner, for_venue_owner

    @staticmethod
    def film_paid(film_payment, conversation):
        for_film_owner = NewsItem(for_user=film_payment.film.user,
                                  type=NewsItemType.FILM_PAID,
                                  conversation=conversation,
                                  content_object=film_payment)
        for_payer = NewsItem(for_user=film_payment.user,
                                   type=NewsItemType.FILM_PAID,
                                   conversation=conversation,
                                   sent=True,
                                   content_object=film_payment)
        return for_film_owner, for_payer
