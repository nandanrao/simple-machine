from django import forms
from django.contrib import admin
from django.contrib.auth.models import User
from notifications.models import MessageDb, EmailList

class EmailListForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmailListForm, self).__init__(*args, **kwargs)
        wtf = User.objects.filter(is_active=True)
        w = self.fields['users'].widget
        choices = []
        for choice in wtf:
            choices.append((choice.id, choice.email))
        w.choices = choices


class EmailListAdmin(admin.ModelAdmin):
    filter_horizontal = ('users',)
    form = EmailListForm

admin.site.register(MessageDb)
admin.site.register(EmailList, EmailListAdmin)
