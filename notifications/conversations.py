from notifications.models import ConversationDb, NewsItemType


class Message(object):
    def __init__(self, db):
        self.db = db

    @property
    def role(self):
        return 'SENDER' if self.db.sent else 'RECEIVER'

    @property
    def type(self):
        return self.db.type

    @property
    def model(self):
        return self.db.content_object

    @property
    def date(self):
        return self.db.created_on


class Conversation(object):
    def __init__(self, db, user, messages=None):
        self.db = db
        self.user = user
        if messages:
            self.messages = [Message(m) for m in messages]
        else:
            self.messages = [Message(m) for m in self.db.items.all()]

    @property
    def id(self):
        return self.db.pk

    @property
    def is_with(self):
        if self.user == self.db.user1:
            return self.db.user2
        elif self.user == self.db.user2:
            return self.db.user1
        return 'UNKNOWN'

    @property
    def topic(self):
        item = self._get_last_non_message_item()
        if item:
            if item.type.startswith('PREVIEW_REQUEST_'):
                return item.model.film.title
            elif item.type.startswith('SCREENING_'):
                if self.user == item.model.film.user:
                    return item.model.venue.name
                elif self.user == item.model.venue.user:
                    return item.model.film.title

        return None

    def _get_last_non_message_item(self):
        for m in reversed(self.messages):
            if m.type != 'MESSAGE':
                return m

        return None

    @property
    def is_unread(self):
        return self.db.is_unread(self.user)

    @property
    def last_updated(self):
        return self.db.last_updated


class Conversations(object):
    @staticmethod
    def for_user(user):
        conversations = ConversationDb.for_user(user)
        conversations = [Conversation(c, user) for c in conversations if len(c.items.all())]
        sorted_conversations = sorted(conversations,
                      cmp=lambda x,y: cmp(x.db.mitem, y.db.mitem),
                      reverse=True
                     )
        unread = [c for c in sorted_conversations if c.is_unread]
        read = [c for c in sorted_conversations if not c.is_unread]
        return unread + read

    @staticmethod
    def get_by_id_for_user(pk, user):
        conversation = ConversationDb.get(pk)
        if conversation.is_participant(user):
            messages = [m for m in conversation.items.filter(for_user=user).order_by('-created_on')]
            conversation.mark_as_viewed(user)
            return Conversation(conversation, user, messages)

        return None
