import datetime

from django.core.management.base import BaseCommand

from notifications.news_item_generator import NewsItemGenerator
from notifications.models import ConversationDb, NewsItem, NewsItemType
from notifications.models import EmailNotification
from screening.models import Screening
from django.db import IntegrityError
from common.mailer import Mailer


class Command(BaseCommand):
    def handle(self, *args, **options):
        print 'Sending screening reminders: %s' % str(datetime.datetime.now())
        screenings = Screening.objects.all()
        eligible_for_reminders = []
        for screening in screenings:
            if not screening.has_published_report and screening.is_accepted and screening.last_event_passed and not screening.cancelled:
                eligible_for_reminders.append(screening)

        reminders = [ReminderGenerator.generate_for(screening) for screening in eligible_for_reminders]

        for reminder in reminders:
            for r in reminder:
                try:
                    r.save()
                    print "Sent reminder to %s of type %s" % (r.for_user.get_profile().name, r.type)

                    if r.type == NewsItemType.SCREENING_REPORT_VENUE:
                        # REPORT? for Venue owner (sent right after last screening event)
                        EmailNotification.send_screening_report_venue(r.content_object)
                    elif r.type == NewsItemType.SCREENING_REPORT_REMINDER_VENUE:
                        # REPORT? REMINDER for Venue owner (sent 3 days after last screening event)
                        EmailNotification.send_screening_report_reminder_venue(r.content_object)
                    elif r.type == NewsItemType.SCREENING_REPORT_FILM:
                        # REPORT? for Film owner (sent 6 days after last event if Venue owner didn't submit)
                        EmailNotification.send_screening_report_film(r.content_object)
                    elif r.type == NewsItemType.SCREENING_REPORT_REMINDER_FILM:
                        # REPORT? REMINDER for Film Owner (sent 3 days after he received REPORT? message)
                        EmailNotification.send_screening_report_reminder_film(r.content_object)

                except IntegrityError:
                    # If that reminder already exists, don't insert again
                    pass


class ReminderGenerator(object):
    @staticmethod
    def generate_for(screening):
        conversation = ConversationDb.for_participants(screening.venue.user, screening.film.user)
        reminders = []

        # REPORT? for Venue owner (sent right after last screening event)
        reminders.append(NewsItemGenerator.screening_report_venue(screening, conversation))

        # REPORT? REMINDER for Venue owner (sent 3 days after last screening event)
        if not screening.report and screening.days_past_since_last_event >= 3:
            reminders.append(NewsItemGenerator.screening_report_reminder_venue(screening, conversation))

        # Send email to BAD_COP if 6 days passed and venue owner didn't submit
        # Send only if FO hasn't received notification - if he did, it means we already
        # sent this email
        report_message = NewsItem.get_for_user(user=screening.film.user, type=NewsItemType.SCREENING_REPORT_FILM, object=screening)
        if not screening.report and screening.is_accepted and screening.days_past_since_last_event >= 6 and not report_message:
            email_content = 'Venue %s, owned by %s, hosted a screening of %s. It\'s been' +\
                    ' 6 days since last event, but still no sign of a report.' +\
                    ' Bad cop is gonna get him now!!1'
            email_content = email_content % (
                screening.venue.name,
                screening.venue.user.get_profile().name,
                screening.film.title
            )
            Mailer.send_to_badcop('%s is late with report' % screening.venue.name, email_content)

        if screening.film.user != screening.venue.user:
            # REPORT? for Film owner (sent 6 days after last event if Venue owner didn't submit)
            if not screening.film_report and screening.days_past_since_last_event >= 6:
                reminders.append(NewsItemGenerator.screening_report_film(screening, conversation))

            # REPORT? REMINDER for Film Owner (sent 3 days after he received REPORT? message)
            if not screening.film_report:
                report_message = NewsItem.get_for_user(user=screening.film.user, type=NewsItemType.SCREENING_REPORT_FILM, object=screening)
                if report_message:
                    if report_message.days_passed_since_sent >= 3:
                        reminders.append(NewsItemGenerator.screening_report_reminder_film(screening, conversation))

        return reminders
