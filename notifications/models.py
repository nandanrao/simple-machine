import pytz
import datetime

from django.db import models
from django.db import IntegrityError
from django.core.mail import EmailMessage
from django.db.models.signals import post_save, m2m_changed
from django.template.loader import render_to_string
from django.core.urlresolvers import reverse
from django.conf import settings
from django.db.models import Q, Max
from django.template.defaultfilters import linebreaks, slugify

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic

from django.contrib.auth.models import User
# from screening.models import Screening

from film.models import PreviewRequest, FilmPreview
from common.models import UserProfile

from urlparse import urljoin
from createsend import CreateSend, List, Subscriber


class EmailList(models.Model):
    name = models.CharField(max_length=250)
    users = models.ManyToManyField(User, related_name='email_lists')
    campaign_monitor_id = models.CharField(max_length=250, null=True, blank=True)
    webhook_id = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.name


def create_campaign_monitor_list(sender, instance, created, **kwargs):
    if created:
        cm_list = List()
        cm_list.auth({'api_key': settings.CM_API_KEY})
        list_id = cm_list.create(settings.CM_CLIENT_ID, instance.name, "", False, "")
        instance.campaign_monitor_id = list_id
        webhook_id = cm_list.create_webhook(["Deactivate", "Subscribe"], "%s/notifications/campaign_monitor_webhook/" % settings.BASE_URL, "json")
        instance.webhook_id = webhook_id
        instance.save()

post_save.connect(create_campaign_monitor_list, sender=EmailList)


tmp_old_subscribers = []

def update_subscribers(sender, instance, action, reverse, *args, **kwargs):
    if (action == 'pre_clear' or action == 'pre_remove') and not reverse:
        global tmp_old_subscribers
        tmp_old_subscribers = [user.id for user in instance.users.all()]

    if (action == 'post_add' or action == 'post_remove') and not reverse:
        subscribers = []
        list_id = instance.campaign_monitor_id
        for user in instance.users.all():
            subscribers.append({"EmailAddress": user.email, "Name": user.get_profile().name})
        subscriber = Subscriber({'api_key': settings.CM_API_KEY})
        import_result = subscriber.import_subscribers(list_id, subscribers, True)

        print 'AAAA'

        current_subscribers = [user.id for user in instance.users.all()]
        unsubscribe_list = [x for x in tmp_old_subscribers if x not in current_subscribers]

        print unsubscribe_list
        if unsubscribe_list:
            for user_id in unsubscribe_list:
                user = User.objects.get(pk=user_id)
                subscriber = Subscriber({'api_key': settings.CM_API_KEY}, list_id, user.email)
                subscriber.unsubscribe()


m2m_changed.connect(update_subscribers, sender=EmailList.users.through)


class MessageDb(models.Model):
    from_user = models.ForeignKey(User, related_name='sent_messages')
    to_user = models.ForeignKey(User, related_name='received_messages')
    content = models.TextField()

    def __unicode__(self):
        return 'From ' + self.from_user.get_profile().name + ' to ' + self.to_user.get_profile().name + ' (' + str(self.id) + ')'


class ConversationDb(models.Model):
    content_type_1 = models.ForeignKey(ContentType, related_name='participant1')
    object_id_1 = models.PositiveIntegerField()
    participant_1 = generic.GenericForeignKey('content_type_1', 'object_id_1')
    content_type_2 = models.ForeignKey(ContentType, related_name='participant2')
    object_id_2 = models.PositiveIntegerField()
    participant_2 = generic.GenericForeignKey('content_type_2', 'object_id_2')
    participant_1_topic = models.CharField(max_length=250)
    participant_2_topic = models.CharField(max_length=250)
    participant_1_name = models.CharField(max_length=250)
    participant_2_name = models.CharField(max_length=250)
    user1 = models.ForeignKey(User, related_name='conversations1')
    user2 = models.ForeignKey(User, related_name='conversations2')
    conversation_hash = models.CharField(max_length=50, null=True)

    def __unicode__(self):
        return 'Conversation between %s and %s' % (self.participant_1, self.participant_2)

    @staticmethod
    def for_user(user):
        film_ids = [f.id for f in user.films.all()]
        venue_ids = [v.id for v in user.venues.all()]
        return ConversationDb.objects\
                .select_related('user1', 'user2', 'user1__userprofile', 'user2__userprofile')\
                .prefetch_related('items')\
                .filter(Q(Q(user1=user) | Q(user2=user)))\
                .annotate(mitem=Max('items__created_on'))\
                .extra(select={"unread_count":"select count(*) from notifications_newsitem where notifications_newsitem.conversation_id=notifications_conversationdb.id and notifications_newsitem.read = %s and notifications_newsitem.for_user_id = %s"}, select_params=(False, user.id,))

    @property
    def last_updated(self):
        if self.items.exists():
            latest_item_updated = self.items.latest('created_on').created_on
        else:
            latest_item_updated = datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=pytz.utc)
        return latest_item_updated

    def is_unread(self, user):
        return self.unread_count > 0

    @staticmethod
    def for_participants(first, second):
        try:
            return ConversationDb.objects.get(
                Q(Q(user1=first) & Q(user2=second)) |
                Q(Q(user2=first) & Q(user1=second))
            )
        except ConversationDb.DoesNotExist:
            return None


    @staticmethod
    def get(pk):
        return ConversationDb.objects.get(pk=pk)

    def is_participant(self, user):
        if self.user1 == user or self.user2 == user:
            return True
        return False

    def mark_as_viewed(self, user):
        self.items.filter(actionable=False, conversation=self, for_user=user).update(read=True)

    @staticmethod
    def get_by_hash(conversation_hash):
        return ConversationDb.objects.get(conversation_hash=conversation_hash)

    def get_email(self):
        # TODO: Move to settings file
        return "Simple Machine <%s@%s>" % (self.conversation_hash, settings.INBOUND_EMAIL_DOMAIN)

    def get_other_user(self, first_user):
        if self.user1 == first_user:
            return self.user2
        elif self.user2 == first_user:
            return self.user1
        else:
            return None


class NewsItemType(object):
    PREVIEW_REQUEST_SENT = 'PREVIEW_REQUEST_SENT'
    PREVIEW_REQUEST_ACCEPTED = 'PREVIEW_REQUEST_ACCEPTED'
    PREVIEW_REQUEST_DECLINED = 'PREVIEW_REQUEST_DECLINED'
    DOWNLOAD_REQUEST_SENT = 'DOWNLOAD_REQUEST_SENT'
    DOWNLOAD_REQUEST_ACCEPTED = 'DOWNLOAD_REQUEST_ACCEPTED'
    DOWNLOAD_REQUEST_DECLINED = 'DOWNLOAD_REQUEST_DECLINED'
    SCREENING_INVITATION = 'SCREENING_INVITATION'
    SCREENING_ACCEPTED = 'SCREENING_ACCEPTED'
    SCREENING_DECLINED = 'SCREENING_DECLINED'
    SCREENING_CANCELLED = 'SCREENING_CANCELLED'
    SCREENING_UPDATED = 'SCREENING_UPDATED'
    SCREENING_REPORT_VENUE = 'SCREENING_REPORT_VENUE'
    SCREENING_REPORT_REMINDER_VENUE = 'SCREENING_REPORT_REMINDER_VENUE'
    SCREENING_REPORT_REMINDER_FILM = 'SCREENING_REPORT_REMINDER_FILM'
    SCREENING_REPORT_FILM = 'SCREENING_REPORT_FILM'
    SCREENING_SHOWN = 'SCREENING_SHOWN'
    SCREENING_PAY_UPFRONT = 'SCREENING_PAY_UPFRONT'
    SCREENING_REPORT_FILM_SENT = 'SCREENING_REPORT_FILM_SENT'
    SCREENING_PAID = 'SCREENING_PAID'
    FILM_PAID = 'FILM_PAID'
    MESSAGE = 'MESSAGE'


class NewsItem(models.Model):
    for_user = models.ForeignKey(User)
    created_on = models.DateTimeField(auto_now_add=True)
    read = models.BooleanField(default=False)
    type = models.CharField(max_length=100)
    sent = models.BooleanField(default=False)
    conversation = models.ForeignKey(ConversationDb, null=True, blank=True, related_name='items')
    actionable = models.BooleanField(default=False)

    # Generic foreign key
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = (('for_user', 'type', 'content_type', 'object_id'), )

    @staticmethod
    def latest_for_user(user):
        return NewsItem.objects.filter(for_user=user).order_by('-created_on')

    @staticmethod
    def mark_done(type, model, user):
        model_type = ContentType.objects.get_for_model(model)
        try:
            item = NewsItem.objects.get(
                type=type,
                content_type__pk=model_type.pk,
                object_id=model.id,
                for_user=user
            )
            item.read = True
            item.save()
        except NewsItem.DoesNotExist:
            pass

    @staticmethod
    def mark_undone(type, model, user):
        model_type = ContentType.objects.get_for_model(model)
        item = NewsItem.objects.get(
            type=type,
            content_type__pk=model_type.pk,
            object_id=model.id,
            for_user=user
        )
        item.read = False
        item.save()

    @staticmethod
    def get_for_user(user, type, object):
        try:
            model_type = ContentType.objects.get_for_model(object)
            return NewsItem.objects.get(for_user=user, type=type, content_type__pk=model_type.pk, object_id=object.id)
        except NewsItem.DoesNotExist:
            return None

    @property
    def days_passed_since_sent(self):
        now = datetime.datetime.now(pytz.utc)
        message_sent = self.created_on
        delta = now - message_sent
        return delta.days

    def safe_save(self):
        " Save model to DB but ignores if it already exists "
        try:
            self.save()
            return True
        except IntegrityError:
            pass
        return False

    def save_or_bump(self):
        try:
            self.save()
            return True
        except IntegrityError:
            self.read = False
            self.save()
        return False


class NotificationRole(object):
    SENDER = 'SENDER'
    RECEIVER = 'RECEIVER'


class EmailNotification(object):

    @staticmethod
    def get_screening_invitation_context(screening):
        film = screening.film
        venue = screening.venue
        profit = screening.profit
        conversation = ConversationDb.for_participants(film.user, venue.user)

        if screening.profit.type == 'NOPROFIT':
            profit_info = []
        elif screening.profit.type == 'SHARE':
            profit_info = [
                "Film: %d%%" % screening.profit.film_share,
                "Simple Machine: %d%%" % screening.profit.smplmchn_share,
                "Venue: %d%%" % screening.profit.venue_share,
            ]
        elif screening.profit.type == 'UPFRONT':
            profit_info = [
                "Film: %d$" % screening.profit.film_upfront,
            ]

        return {
            'film_owner': film.user.get_profile().name,
            'film': film.title,
            'film_url': urljoin(settings.BASE_URL, reverse('film-details',
                kwargs={'pk': film.id})) + slugify(film.title),
            'venue': venue.name,
            'venue_owner': venue.user.get_profile().name,
            'venue_timezone': venue.timezone,
            'screening_date_of_first_event': screening.date_of_first_event,
            'venue_url': urljoin(settings.BASE_URL, reverse('venue-details',
                kwargs={'pk': venue.id})) + slugify(venue.name),
            'conversation_url': urljoin(settings.BASE_URL, reverse('view-conversation', kwargs={'pk': conversation.id})),
            'conversation_hash': conversation.conversation_hash,
            'conversation_email': conversation.get_email(),
            'profit_info': profit_info,
            'profit_type': profit.get_type_display()
        }

    @staticmethod
    def send_screening_invitation(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_invitation.html', context)
        subject = "%(film)s invited to %(venue)s" % {
            'film': screening.film.title,
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_updated(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_updated.html', context)
        subject = "%(venue)s updated" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_paid(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_paid.html', context)
        subject = "%(venue)s paid" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_film_paid(film_payment):
        body = render_to_string('emails/film_paid.html', {
            'film_owner': film_payment.film.user.get_profile().name,
            'payer': film_payment.user.get_profile().name,
            'amount': film_payment.amount,
            'film': film_payment.film.title,
            'film_url': urljoin(
                    settings.BASE_URL,
                    reverse('film-details', kwargs={'pk': film_payment.film.id})
                ) + slugify(film_payment.film.title),
        })
        subject = "%(payer)s paid" % {
            'payer': film_payment.user.get_profile().name
        }
        conversation = ConversationDb.for_participants(film_payment.user, film_payment.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[film_payment.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_accepted_notification(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_accepted.html', context)
        subject = "%(film)s accepted invitation to %(venue)s" % {
            'film': screening.film.title,
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.venue.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_declined_notification(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_declined.html', context)
        subject = "%(film)s declined invitation to %(venue)s" % {
            'film': screening.film.title,
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.venue.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_canceled_notification(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_canceled.html', context)
        subject = "%(venue)s canceled" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_report_venue(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_report_venue.html', context)
        subject = "Report for %(venue)s" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.venue.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_report_reminder_venue(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_report_reminder_venue.html', context)
        subject = "Report reminder for %(venue)s" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.venue.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_report_film(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_report_film.html', context)
        subject = "Report for %(venue)s" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_screening_report_reminder_film(screening):
        context = EmailNotification.get_screening_invitation_context(screening)
        body = render_to_string('emails/screening_report_reminder_film.html', context)
        subject = "Report reminder for %(venue)s" % {
            'venue': screening.venue.name
        }
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=context['conversation_email'],
            to=[screening.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def _get_preview_request_notification_context(preview_request):
        film = preview_request.film

        return {
            'film': film.title,
            'film_owner': film.user.get_profile().name,
            'film_url': urljoin(settings.BASE_URL, reverse('film-details',
                kwargs={'pk': film.id})) + slugify(film.title),
            'requester_info': preview_request.user.get_profile().name,
            'accept_prev_req_url': urljoin(settings.BASE_URL,
                reverse('film-accept-preview-request', kwargs={
                    'film_id': preview_request.film.id,
                    'prev_req_id': preview_request.id
                })),
            'decline_prev_req_url': urljoin(settings.BASE_URL,
                reverse('film-decline-preview-request', kwargs={
                    'film_id': preview_request.film.id,
                    'prev_req_id': preview_request.id
                }))
        }

    @staticmethod
    def _get_download_request_notification_context(download_request):
        film = download_request.film

        return {
            'film': film.title,
            'film_owner': film.user.get_profile().name,
            'film_url': urljoin(settings.BASE_URL, reverse('film-details',
                kwargs={'pk': film.id})) + slugify(film.title),
            'requester_info': download_request.user.get_profile().name,
            'accept_down_req_url': urljoin(settings.BASE_URL,
                reverse('film-accept-download-request', kwargs={
                    'film_id': download_request.film.id,
                    'down_req_id': download_request.id
                })),
            'decline_prev_req_url': urljoin(settings.BASE_URL,
                reverse('film-decline-download-request', kwargs={
                    'film_id': download_request.film.id,
                    'down_req_id': download_request.id
                }))
        }

    @staticmethod
    def send_preview_request_notification(preview_request):
        context = EmailNotification._get_preview_request_notification_context(
            preview_request)
        body = render_to_string('emails/preview_request_notification.html',
            context)
        subject = "%(req_info)s asks for %(film)s preview" % {
            'req_info': context['requester_info'],
            'film': context['film'],
        }
        conversation = ConversationDb.for_participants(preview_request.user, preview_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[preview_request.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_download_request_notification(download_request):
        context = EmailNotification._get_download_request_notification_context(
            download_request)
        body = render_to_string('emails/download_request_notification.html',
            context)
        subject = "%(req_info)s asks for %(film)s download" % {
            'req_info': context['requester_info'],
            'film': context['film'],
        }
        conversation = ConversationDb.for_participants(download_request.user, download_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[download_request.film.user.email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_password_recovery_email(email):
        code = UserProfile.encode_signature_with_email(email)
        body = render_to_string('emails/password_recovery.html', {
            'code': code,
            'base_url': settings.BASE_URL
        })
        subject = "Password recovery"
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL_ACCOUNTS,
            to=[email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_email_change_email(user_id, email):
        code = UserProfile.encode_signature_with_user_id_and_email(
            user_id, email)
        body = render_to_string('emails/change_email.html', {
            'code': code,
            'base_url': settings.BASE_URL,
            'email': email
        })
        subject = "Change email address"
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL_ACCOUNTS,
            to=[email]
        )
        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_message_received_email(from_user, news_item):
        body = render_to_string('emails/message_received.html', {
            'from_user': from_user.get_profile().name,
            'receiver_name': news_item.for_user.get_profile().name,
            'base_url': settings.BASE_URL,
            'conversation_id': news_item.conversation.id,
            'message_text': linebreaks(news_item.content_object.content)
        })

        subject = "Simple Machine - Message received"


        email = news_item.for_user.email
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=news_item.conversation.get_email(),
            to=[email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_message_from_unknown_email(to_user, message_text):
        body = render_to_string('emails/message_from_unknown_email.html', {
            'to_email': to_user,
            'base_url': settings.BASE_URL,
            'message_text': linebreaks(message_text)
        })

        subject = "Simple Machine - Message from unknown email"

        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[to_user]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_preview_request_accepted_notification(preview_request):

        context = {
            'requester': preview_request.user.get_profile().name,
            'film_owner': preview_request.film.user.get_profile().name,
            'film': preview_request.film.title,
            'film_page_url': urljoin(settings.BASE_URL, reverse('film-details', args=[preview_request.film.id])) + slugify(preview_request.film.title)
        }

        body = render_to_string('emails/preview_request_accepted_notification.html', context)

        subject = "Preview request accepted!"
        conversation = ConversationDb.for_participants(preview_request.user, preview_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[preview_request.user.email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_preview_request_declined_notification(preview_request):

        context = {
            'requester': preview_request.user.get_profile().name,
            'film_owner': preview_request.film.user.get_profile().name,
            'film': preview_request.film.title,
            'film_page_url': urljoin(settings.BASE_URL, reverse('film-details', args=[preview_request.film.id])) + slugify(preview_request.film.title)
        }

        body = render_to_string('emails/preview_request_declined_notification.html', context)

        subject = "Preview request declined!"
        conversation = ConversationDb.for_participants(preview_request.user, preview_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[preview_request.user.email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_download_request_accepted_notification(download_request):

        context = {
            'requester': download_request.user.get_profile().name,
            'film_owner': download_request.film.user.get_profile().name,
            'film': download_request.film.title,
            'film_page_url': urljoin(settings.BASE_URL, reverse('film-details', args=[download_request.film.id])) + slugify(download_request.film.title)
        }

        body = render_to_string('emails/download_request_accepted_notification.html', context)

        subject = "Download request accepted!"
        conversation = ConversationDb.for_participants(download_request.user, download_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[download_request.user.email]
        )

        msg.content_subtype = "html"
        msg.send()

    @staticmethod
    def send_download_request_declined_notification(download_request):
        context = {
            'requester': download_request.user.get_profile().name,
            'film_owner': download_request.film.user.get_profile().name,
            'film': download_request.film.title,
            'film_page_url': urljoin(settings.BASE_URL, reverse('film-details', args=[download_request.film.id])) + slugify(download_request.film.title)
        }

        body = render_to_string('emails/download_request_declined_notification.html', context)

        subject = "Download request declined!"
        conversation = ConversationDb.for_participants(download_request.user, download_request.film.user)
        msg = EmailMessage(
            subject=subject,
            body=body,
            from_email=conversation.get_email(),
            to=[download_request.user.email]
        )

        msg.content_subtype = "html"
        msg.send()
