from django.conf.urls.defaults import patterns, url

from .views import *

urlpatterns = patterns(
    '',
    url(r'^mark_as_read/$', mark_as_read, name='mark-notification-as-read'),
    url(r'^mailgun_feedback/$', read_mailgun_feedback, name='read-mailgun-feedback'),
    url(r'^campaign_monitor_webhook/$', campaign_monitor_webhook, name='campaign-monitor-webhook'),
    url(r'^conversation/(?P<pk>\d+)/vos/$', view_or_start_conversation, name='view-or-start-conversation'),
    url(r'^conversation/(?P<pk>\d+)/$', view_conversation, name='view-conversation'),
)
