import hashlib

from notifications.models import ConversationDb
from film.models import Film
from screening.models import Venue
from django.contrib.auth.models import User


#TODO: not tested, see comment in test_conversation_creation.py
class ConversationCreation(object):
    @staticmethod
    def start_new(participant1, participant2):
        topic1 = ConversationCreation._resolve_topic(participant2)
        topic2 = ConversationCreation._resolve_topic(participant1)
        name1 = ConversationCreation._resolve_name(participant2)
        name2 = ConversationCreation._resolve_name(participant1)
        user1 = ConversationCreation._resolve_user(participant1)
        user2 = ConversationCreation._resolve_user(participant2)
        conversation_hash = ConversationCreation._calculate_hash(user1, user2)
        return ConversationDb(participant_1=participant1,
                              participant_2=participant2,
                              user1=user1,
                              user2=user2,
                              participant_1_topic=topic1,
                              participant_2_topic=topic2,
                              participant_1_name=name1,
                              participant_2_name=name2,
                              conversation_hash=conversation_hash)

    @staticmethod
    def get_or_start(participant1, participant2):
        conversation = ConversationDb.for_participants(participant1, participant2)
        if not conversation:
            conversation = ConversationCreation.start_new(participant1, participant2)
            conversation.save()
        return conversation

    @staticmethod
    def _resolve_topic(other_participant):
        if isinstance(other_participant, Film):
            return other_participant.title
        elif isinstance(other_participant, Venue):
            return other_participant.name
        elif isinstance(other_participant, User):
            return ''

    @staticmethod
    def _resolve_name(other_participant):
        if isinstance(other_participant, Film):
            return other_participant.user.get_profile().name
        elif isinstance(other_participant, Venue):
            return other_participant.user.get_profile().name
        elif isinstance(other_participant, User):
            return other_participant.get_profile().name

    @staticmethod
    def _resolve_user(participant):
        if isinstance(participant, Film):
            return participant.user
        elif isinstance(participant, Venue):
            return participant.user
        elif isinstance(participant, User):
            return participant

    @staticmethod
    def _calculate_hash(user1, user2):
        m = hashlib.md5()
        m.update("%s%s" % (user1.email, user2.email))
        return m.hexdigest()
