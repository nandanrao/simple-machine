import json
import logging

from django.shortcuts import render, redirect

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from notifications.conversations import Conversations
from notifications.conversation_creation import ConversationCreation
from notifications.models import NewsItem, ConversationDb, MessageDb, EmailNotification, EmailList
from notifications.news_item_generator import NewsItemGenerator
from common.tasks import mp_track


@login_required
def mark_as_read(request):
    pk = request.POST.get('pk')
    notification = get_object_or_404(NewsItem, pk=pk)
    if notification.for_user != request.user:
        return redirect('home')

    if not notification.actionable:
        notification.read = True
        notification.save()
    return HttpResponse('')


@login_required
def view_conversation(request, pk):
    conversation = Conversations.get_by_id_for_user(pk, request.user)
    if conversation is None:
        return redirect('home')
    return render(request, 'conversation.html', {'conversation':conversation})


@login_required
def view_or_start_conversation(request, pk):
    with_user = get_object_or_404(User, pk=pk)
    conversation = ConversationDb.for_participants(request.user, with_user)
    if not conversation:
        conversation = ConversationCreation.start_new(request.user, with_user)
        conversation.save()

    return redirect('view-conversation', conversation.id)


@csrf_exempt
def read_mailgun_feedback(request):
    if request.method == 'POST':
        conversation_hash = request.POST.get('recipient').split('@')[0] \
                            if request.POST.get('recipient') else None
        participant_from = User.objects.filter(email=request.POST.get('sender'))
        msg_text = request.POST.get('stripped-text')

        # For testing
        log = logging.getLogger('sm_messages')
        log.info("\n%s\n%s\n%s\n---===---===---===---===---===---===---===---===---===---" % (
            request.POST.get('sender'),
            request.POST.get('recipient'),
            msg_text
        ))

        if participant_from:
            participant_from = participant_from[0]
            conversation = get_object_or_404(ConversationDb, conversation_hash=conversation_hash)
            if conversation:
                participant_to = conversation.get_other_user(participant_from)
                if participant_to:
                    mp_track.delay(participant_from, 'message sent')
                    message = MessageDb.objects.create(
                        from_user=participant_from, to_user=participant_to, content=msg_text
                    )
                    for_sender, for_receiver = NewsItemGenerator.send_message(
                        conversation, participant_from, participant_to, message
                    )
                    for_sender.save()
                    for_receiver.save()
                    log.info("SAVED\n---===---===---===---===---===---===---===---===---===---")
                    if for_receiver.for_user.get_profile().message_received_email:
                        EmailNotification.send_message_received_email(participant_from, for_receiver)
        else:
            EmailNotification.send_message_from_unknown_email(request.POST.get('sender'), msg_text)
    return HttpResponse('')


@csrf_exempt
def campaign_monitor_webhook(request):
    json_data = json.loads(request.raw_post_data)
    if request.method == 'POST' and json_data.get('ListID'):
        email_list = EmailList.objects.get(campaign_monitor_id=json_data.get('ListID'))
        if email_list:
            for event in json_data.get('Events'):
                user = User.objects.get(email=event.get('EmailAddress'))
                action = event.get('Type')
                if user:
                    if action == 'Subscribe':
                        user.email_lists.add(email_list)
                    elif action == 'Deactivate':
                        user.email_lists.remove(email_list)
    return HttpResponse('')
